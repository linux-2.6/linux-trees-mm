/*
 * IPWireless 3G PCMCIA Network Driver
 *
 * Original code
 *   by Stephen Blackheath <stephen@blacksapphire.com>,
 *      Ben Martel <benm@symmetric.co.nz>
 *
 * Copyrighted as follows:
 *   Copyright (C) 2004 by Symmetric Systems Ltd (NZ)
 *
 * Various driver changes and rewrites, port to new kernels
 *   Copyright (C) 2006-2007 Jiri Kosina
 *
 * Misc code cleanups and updates
 *   Copyright (C) 2007 David Sterba
 */

#ifndef _IPWIRELESS_CS_H_
#define _IPWIRELESS_CS_H_

#include <linux/autoconf.h>
#include <linux/version.h>
#include <linux/types.h>
#include <linux/sched.h>
#include <pcmcia/cs_types.h>
#include <pcmcia/cs.h>
#include <pcmcia/cistpl.h>
#include <pcmcia/ds.h>

#include "ipwireless_cs_hardware.h"

#define IPWIRELESS_PCCARD_NAME		"ipwireless_cs"
#define IPWIRELESS_PCMCIA_VERSION	"1.0.15-jikos2"
#define IPWIRELESS_PCMCIA_AUTHOR        \
	"Stephen Blackheath, Ben Martel and Jiri Kosina"

#define IPWIRELESS_TX_QUEUE_SIZE  262144
#define IPWIRELESS_RX_QUEUE_SIZE  262144

#define IPWIRELESS_STATE_DEBUG

struct ipw_hardware;
struct ipw_network;
struct ipw_tty;

struct ipw_config {
	char phone_number[129];
};

struct ipw_dev {
	struct pcmcia_device *link;
	int isV2Card;
	window_handle_t handleAM;
	void __iomem *attrMemory;
	window_handle_t handleCM;
	void __iomem *commonMemory;
	dev_node_t nodes[2];
	/* Reference to attribute memory, containing CIS data */
	void *attribute_memory;

	/* Hardware context */
	struct ipw_hardware *hardware;
	/* Network layer context */
	struct ipw_network *network;
	/* TTY device context */
	struct ipw_tty *tty;
	struct work_struct work_reboot;
};

/* Options configurable through the proc filesystem. */
extern __u16 ipwireless_cs_debug;
extern __u16 ipwireless_cs_loopback;
extern __u16 ipwireless_cs_out_queue;

#endif
