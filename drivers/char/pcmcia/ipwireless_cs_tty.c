/*
 * IPWireless 3G PCMCIA Network Driver
 *
 * Original code
 *   by Stephen Blackheath <stephen@blacksapphire.com>,
 *      Ben Martel <benm@symmetric.co.nz>
 *
 * Copyrighted as follows:
 *   Copyright (C) 2004 by Symmetric Systems Ltd (NZ)
 *
 * Various driver changes and rewrites, port to new kernels
 *   Copyright (C) 2006-2007 Jiri Kosina
 *
 * Misc code cleanups and updates
 *   Copyright (C) 2007 David Sterba
 */

#include <linux/autoconf.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/tty.h>
#include <linux/tty_driver.h>
#include <linux/tty_flip.h>
#include <linux/uaccess.h>
#include <linux/if.h>
#include <linux/ppp_defs.h>
#include <linux/if_ppp.h>
#include <linux/proc_fs.h>
#include <linux/serial.h>
#include <linux/mutex.h>

#include "ipwireless_cs_tty.h"
#include "ipwireless_cs_network.h"
#include "ipwireless_cs_hardware.h"
#include "ipwireless_cs_main.h"

#define IPWIRELESS_PCMCIA_START 	(0)
#define IPWIRELESS_PCMCIA_MINORS	(24)
#define IPWIRELESS_PCMCIA_MINOR_RANGE	(8)

#define TTYTYPE_MODEM    (0)
#define TTYTYPE_MONITOR  (1)
#define TTYTYPE_RAS_RAW  (2)

struct ipw_tty {
	int index;
	struct ipw_hardware *hardware;
	unsigned int channel_idx;
	unsigned int secondary_channel_idx;
	int tty_type;
	struct ipw_network *network;
	struct tty_struct *linux_tty;
	int open_count;
	unsigned int control_lines;
	struct mutex ipw_tty_mutex;
	int tx_bytes_queued;
	int closing;
};

static struct ipw_tty *ttys[IPWIRELESS_PCMCIA_MINORS];

static struct tty_driver *ipw_tty_driver;

static char *tty_type_name(int tty_type)
{
	static char *channel_names[] = {
		"modem",
		"monitor",
		"RAS-raw"
	};

	return channel_names[tty_type];
}

static void report_registering(struct ipw_tty *tty)
{
	char *iftype = tty_type_name(tty->tty_type);

	printk(KERN_INFO IPWIRELESS_PCCARD_NAME
	       ": registering %s device ttyIPWp%d\n", iftype, tty->index);
}

static void report_deregistering(struct ipw_tty *tty)
{
	char *iftype = tty_type_name(tty->tty_type);

	printk(KERN_INFO IPWIRELESS_PCCARD_NAME
	       ": deregistering %s device ttyIPWp%d\n", iftype,
	       tty->index);
}

static struct ipw_tty *get_tty(int minor)
{
	if (minor < IPWIRELESS_PCMCIA_START
			|| minor >= IPWIRELESS_PCMCIA_START +
			IPWIRELESS_PCMCIA_MINORS)
		return NULL;
	else {
		int minor_offset = minor - IPWIRELESS_PCMCIA_START;

		/*
		 * The 'ras_raw' channel is only available when 'loopback' mode
		 * is enabled.
		 * Number of minor starts with 16 (_RANGE * _RAS_RAW).
		 */
		if (!ipwireless_cs_loopback &&
				minor_offset >=
				 IPWIRELESS_PCMCIA_MINOR_RANGE * TTYTYPE_RAS_RAW)
			return NULL;

		return ttys[minor_offset];
	}
}

static int ipw_open(struct tty_struct *linux_tty, struct file *filp)
{
	int minor = linux_tty->index;
	struct ipw_tty *tty = get_tty(minor);

	if (!tty)
		return -ENODEV;

	mutex_lock(&tty->ipw_tty_mutex);

	if (tty->closing) {
		mutex_unlock(&tty->ipw_tty_mutex);
		return -ENODEV;
	}
	if (tty->open_count == 0)
		tty->tx_bytes_queued = 0;

	tty->open_count++;

	tty->linux_tty = linux_tty;
	linux_tty->driver_data = tty;
	linux_tty->low_latency = 1;

	if (tty->tty_type == TTYTYPE_MODEM)
		ipwireless_ppp_open(tty->network);

	mutex_unlock(&tty->ipw_tty_mutex);

	return 0;
}

static void do_ipw_close(struct ipw_tty *tty)
{
	tty->open_count--;

	if (tty->open_count == 0) {
		struct tty_struct *linux_tty = tty->linux_tty;

		if (linux_tty != NULL) {
			tty->linux_tty = NULL;
			linux_tty->driver_data = NULL;

			if (tty->tty_type == TTYTYPE_MODEM)
				ipwireless_ppp_close(tty->network);
		}
	}
}

static void ipw_hangup(struct tty_struct *linux_tty)
{
	struct ipw_tty *tty = linux_tty->driver_data;

	if (!tty)
		return;

	mutex_lock(&tty->ipw_tty_mutex);
	if (tty->open_count == 0) {
		mutex_unlock(&tty->ipw_tty_mutex);
		return;
	}

	do_ipw_close(tty);

	mutex_unlock(&tty->ipw_tty_mutex);
}

static void ipw_close(struct tty_struct *linux_tty, struct file *filp)
{
	ipw_hangup(linux_tty);
}

/* Take data received from hardware, and send it out the tty */
void ipwireless_tty_received(struct ipw_tty *tty, unsigned char *data,
			unsigned int length)
{
	struct tty_struct *linux_tty;
	int work = 0;

	mutex_lock(&tty->ipw_tty_mutex);
	linux_tty = tty->linux_tty;
	if (linux_tty == NULL) {
		mutex_unlock(&tty->ipw_tty_mutex);
		return;
	}

	if (!tty->open_count) {
		mutex_unlock(&tty->ipw_tty_mutex);
		return;
	}
	mutex_unlock(&tty->ipw_tty_mutex);

	work = tty_insert_flip_string(linux_tty, data, length);

	if (work != length)
		printk(KERN_DEBUG "%d chars not inserted to flip buffer!\n",
				length - work);

	/*
	 * This may sleep if ->low_latency is set
	 */
	if (work)
		tty_flip_buffer_push(linux_tty);
}

static void ipw_write_packet_sent_callback(void *callback_data,
					   unsigned int packet_length)
{
	struct ipw_tty *tty = (struct ipw_tty *) callback_data;

	/*
	 * Packet has been sent, so we subtract the number of bytes from our
	 * tally of outstanding TX bytes.
	 */
	tty->tx_bytes_queued -= packet_length;
}

static int ipw_write(struct tty_struct *linux_tty,
		     const unsigned char *buf, int count)
{
	struct ipw_tty *tty = linux_tty->driver_data;
	int room;

	if (!tty)
		return -ENODEV;

	mutex_lock(&tty->ipw_tty_mutex);
	if (!tty->open_count) {
		mutex_unlock(&tty->ipw_tty_mutex);
		return -EINVAL;
	}

	room = IPWIRELESS_TX_QUEUE_SIZE - tty->tx_bytes_queued;
	if (room < 0)
		room = 0;
	/* Don't allow caller to write any more than we have room for */
	if (count > room)
		count = room;

	if (count == 0) {
		mutex_unlock(&tty->ipw_tty_mutex);
		return 0;
	}

	tty->tx_bytes_queued += count;
	ipwireless_send_packet(tty->hardware, IPW_CHANNEL_RAS,
			       (unsigned char *) buf, count,
			       ipw_write_packet_sent_callback, tty);
	mutex_unlock(&tty->ipw_tty_mutex);

	return count;
}

static int ipw_write_room(struct tty_struct *linux_tty)
{
	struct ipw_tty *tty = linux_tty->driver_data;
	int room;

	if (!tty)
		return -ENODEV;

	if (!tty->open_count)
		return -EINVAL;

	room = IPWIRELESS_TX_QUEUE_SIZE - tty->tx_bytes_queued;
	if (room < 0)
		room = 0;

	return room;
}

static int ipwireless_get_serial_info(struct ipw_tty *tty,
				      struct serial_struct __user *retinfo)
{
	struct serial_struct tmp;

	if (!retinfo)
		return (-EFAULT);

	memset(&tmp, 0, sizeof(tmp));
	tmp.type = PORT_UNKNOWN;
	tmp.line = tty->index;
	tmp.port = 0;
	tmp.irq = 0;
	tmp.flags = 0;
	tmp.baud_base = 115200;
	tmp.close_delay = 0;
	tmp.closing_wait = 0;
	tmp.custom_divisor = 0;
	tmp.hub6 = 0;
	if (copy_to_user(retinfo, &tmp, sizeof(*retinfo)))
		return -EFAULT;

	return 0;
}

static int ipw_chars_in_buffer(struct tty_struct *linux_tty)
{
	struct ipw_tty *tty = linux_tty->driver_data;

	if (!tty)
		return -ENODEV;

	if (!tty->open_count)
		return -EINVAL;

	return tty->tx_bytes_queued;
}

static int get_control_lines(struct ipw_tty *tty)
{
	unsigned int my = tty->control_lines;
	unsigned int out = 0;

	if (my & IPW_CONTROL_LINE_RTS)
		out |= TIOCM_RTS;
	if (my & IPW_CONTROL_LINE_DTR)
		out |= TIOCM_DTR;
	if (my & IPW_CONTROL_LINE_CTS)
		out |= TIOCM_CTS;
	if (my & IPW_CONTROL_LINE_DSR)
		out |= TIOCM_DSR;
	if (my & IPW_CONTROL_LINE_DCD)
		out |= TIOCM_CD;

	return out;
}

static int set_control_lines(struct ipw_tty *tty, unsigned int set,
			     unsigned int clear)
{
	if (set & TIOCM_RTS) {
		ipwireless_set_RTS(tty->hardware, tty->channel_idx, 1);
		if (tty->secondary_channel_idx != -1)
			ipwireless_set_RTS(tty->hardware,
					  tty->secondary_channel_idx, 1);
	}
	if (set & TIOCM_DTR) {
		ipwireless_set_DTR(tty->hardware, tty->channel_idx, 1);
		if (tty->secondary_channel_idx != -1)
			ipwireless_set_DTR(tty->hardware,
					  tty->secondary_channel_idx, 1);
	}
	if (clear & TIOCM_RTS) {
		ipwireless_set_RTS(tty->hardware, tty->channel_idx, 0);
		if (tty->secondary_channel_idx != -1)
			ipwireless_set_RTS(tty->hardware,
					  tty->secondary_channel_idx, 0);
	}
	if (clear & TIOCM_DTR) {
		ipwireless_set_DTR(tty->hardware, tty->channel_idx, 0);
		if (tty->secondary_channel_idx != -1)
			ipwireless_set_DTR(tty->hardware,
					  tty->secondary_channel_idx, 0);
	}
	return 0;
}

static int ipw_tiocmget(struct tty_struct *linux_tty, struct file *file)
{
	struct ipw_tty *tty = linux_tty->driver_data;

	if (!tty)
		return -ENODEV;

	if (!tty->open_count)
		return -EINVAL;

	return get_control_lines(tty);
}

static int
ipw_tiocmset(struct tty_struct *linux_tty, struct file *file,
	     unsigned int set, unsigned int clear)
{
	struct ipw_tty *tty = linux_tty->driver_data;

	if (!tty)
		return -ENODEV;

	if (!tty->open_count)
		return -EINVAL;

	return set_control_lines(tty, set, clear);
}

static int ipw_ioctl(struct tty_struct *linux_tty, struct file *file,
		     unsigned int cmd, unsigned long arg)
{
	struct ipw_tty *tty = linux_tty->driver_data;

	if (!tty)
		return -ENODEV;

	if (!tty->open_count)
		return -EINVAL;

	switch (cmd) {
	case TIOCGSERIAL:
		return ipwireless_get_serial_info(tty, (void __user *) arg);

	case TIOCSSERIAL:
		return 0;	/* Keeps the PCMCIA scripts happy. */
	}

	if (tty->tty_type == TTYTYPE_MODEM) {
		switch (cmd) {
		case PPPIOCGCHAN:
			{
				int chan = ipwireless_ppp_channel_index(
							tty->network);

				if (chan < 0)
					return -ENODEV;
				if (put_user(chan, (int __user *) arg))
					return -EFAULT;
			}
			return 0;

		case PPPIOCGUNIT:
			{
				int unit = ipwireless_ppp_unit_number(
						tty->network);

				if (unit < 0)
					return -ENODEV;
				if (put_user(unit, (int __user *) arg))
					return -EFAULT;
			}
			return 0;

		case TCGETS:
		case TCGETA:
			return n_tty_ioctl(linux_tty, file, cmd, arg);

		case TCFLSH:
			return n_tty_ioctl(linux_tty, file, cmd, arg);

		case FIONREAD:
			{
				int val = 0;

				if (put_user(val, (int __user *) arg))
					return -EFAULT;
			}
			return 0;
		}
	}

	return -ENOIOCTLCMD;
}

static void add_tty(dev_node_t *nodesp, int j,
		    struct ipw_hardware *hardware,
		    struct ipw_network *network, int channel_idx,
		    int secondary_channel_idx, int tty_type)
{
	ttys[j] = kzalloc(sizeof(struct ipw_tty), GFP_KERNEL);
	ttys[j]->index = j;
	ttys[j]->hardware = hardware;
	ttys[j]->channel_idx = channel_idx;
	ttys[j]->secondary_channel_idx = secondary_channel_idx;
	ttys[j]->network = network;
	ttys[j]->tty_type = tty_type;
	mutex_init(&ttys[j]->ipw_tty_mutex);

	ipwireless_associate_network_tty(network, channel_idx, ttys[j]);

	if (secondary_channel_idx != -1)
		ipwireless_associate_network_tty(network,
						 secondary_channel_idx,
						 ttys[j]);
	if (nodesp != NULL) {
		sprintf(nodesp->dev_name, "ttyIPWp%d", j);
		nodesp->major = ipw_tty_driver->major;
		nodesp->minor = j + IPWIRELESS_PCMCIA_START;
	}
	if (get_tty(j + IPWIRELESS_PCMCIA_START) == ttys[j])
		report_registering(ttys[j]);
}

struct ipw_tty *ipwireless_tty_create(struct ipw_hardware *hardware,
				      struct ipw_network *network,
				      dev_node_t *nodes)
{
	int i, j;

	for (i = 0; i < IPWIRELESS_PCMCIA_MINOR_RANGE; i++) {
		int allfree = 1;

		for (j = i; j < IPWIRELESS_PCMCIA_MINORS;
				j += IPWIRELESS_PCMCIA_MINOR_RANGE)
			if (ttys[j] != NULL) {
				allfree = 0;
				break;
			}

		if (allfree) {
			j = i;

			add_tty(&nodes[0], j, hardware, network,
				IPW_CHANNEL_DIALLER, IPW_CHANNEL_RAS,
				TTYTYPE_MODEM);
			j += IPWIRELESS_PCMCIA_MINOR_RANGE;
			add_tty(&nodes[1], j, hardware, network,
				IPW_CHANNEL_DIALLER, -1, TTYTYPE_MONITOR);
			j += IPWIRELESS_PCMCIA_MINOR_RANGE;
			add_tty(NULL, j, hardware, network,
				IPW_CHANNEL_RAS, -1, TTYTYPE_RAS_RAW);

			nodes[0].next = &nodes[1];
			nodes[1].next = NULL;

			return ttys[i];
		}
	}
	return NULL;
}

/*!
 * Must be called before ipwireless_network_free().
 */
void ipwireless_tty_free(struct ipw_tty *tty)
{
	int j;
	struct ipw_network *network = ttys[tty->index]->network;

	for (j = tty->index; j < IPWIRELESS_PCMCIA_MINORS;
			j += IPWIRELESS_PCMCIA_MINOR_RANGE) {
		struct ipw_tty *ttyj = ttys[j];

		if (ttyj) {
			mutex_lock(&ttyj->ipw_tty_mutex);
			if (get_tty(j + IPWIRELESS_PCMCIA_START) == ttyj)
				report_deregistering(ttyj);
			ttyj->closing = 1;
			if (ttyj->linux_tty != NULL) {
				mutex_unlock(&ttyj->ipw_tty_mutex);
				tty_hangup(ttyj->linux_tty);
				/* Wait till the tty_hangup has completed */
				flush_scheduled_work();
				mutex_lock(&ttyj->ipw_tty_mutex);
			}
			while (ttyj->open_count)
				do_ipw_close(ttyj);
			ipwireless_disassociate_network_ttys(network,
							     ttyj->channel_idx);
			ttys[j] = NULL;
			mutex_unlock(&ttyj->ipw_tty_mutex);
			kfree(ttyj);
		}
	}
}

static struct tty_operations tty_ops = {
	.open = ipw_open,
	.close = ipw_close,
	.hangup = ipw_hangup,
	.write = ipw_write,
	.write_room = ipw_write_room,
	.ioctl = ipw_ioctl,
	.chars_in_buffer = ipw_chars_in_buffer,
	.tiocmget = ipw_tiocmget,
	.tiocmset = ipw_tiocmset,
};

static int proc_read(char *page, char **start, off_t off, int count,
		     int *eof, void *data)
{
	int i, j;
	char *p = page;
	int len;

	if (off == 0) {
		p += sprintf(p, "driver: %s\nversion: %s\n\n",
			     IPWIRELESS_PCCARD_NAME,
			     IPWIRELESS_PCMCIA_VERSION);
		for (i = 0; i < IPWIRELESS_PCMCIA_MINOR_RANGE; i++)
			for (j = i; j < IPWIRELESS_PCMCIA_MINORS;
					j += IPWIRELESS_PCMCIA_MINOR_RANGE)
				if (ttys[j] != NULL &&
						get_tty(ttys[j]->index +
							IPWIRELESS_PCMCIA_START)
							== ttys[j])
					p += sprintf(p, "port: /dev/ttyIPWp%d %s %d\n",
							j,
							tty_type_name(ttys[j]->tty_type),
							ttys[j]->open_count);
#ifdef IPWIRELESS_STATE_DEBUG
		*p++ = '\n';
		for (i = 0; i < IPWIRELESS_PCMCIA_MINOR_RANGE; i++)
			if (ttys[i] != NULL) {
				p += ipwireless_dump_hardware_state(p,
						ttys[i]->hardware);
				if (ttys[i]->network != NULL)
					p += ipwireless_dump_network_state(p,
							ttys[i]->network);
				break;
			}
#endif
	}

	len = (p - page);
	if (len <= off + count)
		*eof = 1;
	*start = page + off;
	len -= off;
	if (len > count)
		len = count;
	if (len < 0)
		len = 0;

	return len;
}

static int proc_write(struct file *file, const char __user *buffer,
		      unsigned long count, void *data)
{
	char *command = kmalloc(count + 1, GFP_KERNEL);
	int i, lineLen;
	struct ipw_tty *could_open[IPWIRELESS_PCMCIA_MINORS];

	if (copy_from_user(command, buffer, count)) {
		kfree(command);
		return -EFAULT;
	}
	command[count] = '\0';

	for (i = 0; i < IPWIRELESS_PCMCIA_MINORS; ++i)
		could_open[i] = get_tty(i + IPWIRELESS_PCMCIA_START);

	for (i = 0; i < count; i += lineLen + 1) {
		char *cmd;

		lineLen = 0;
		while (command[i + lineLen] != '\n'
				&& command[i + lineLen] != '\0')
			lineLen++;
		command[i + lineLen] = '\0';

		cmd = command + i;

		if (strncmp(cmd, "debug=", 6) == 0) {
			ipwireless_cs_debug =
			    (int) simple_strtol(cmd + 6, NULL, 10);
			printk(KERN_INFO IPWIRELESS_PCCARD_NAME
			       ": set debug=%d\n", ipwireless_cs_debug);
		} else if (strncmp(cmd, "loopback=", 9) == 0) {
			ipwireless_cs_loopback =
			    (int) simple_strtol(cmd + 9, NULL, 10);
			printk(KERN_INFO IPWIRELESS_PCCARD_NAME
			       ": set loopback=%d\n",
			       ipwireless_cs_loopback);
		} else if (strncmp(cmd, "out_queue=", 10) == 0) {
			ipwireless_cs_out_queue =
			    (int) simple_strtol(cmd + 10, NULL, 10);
			printk(KERN_INFO IPWIRELESS_PCCARD_NAME
			       ": set out_queue=%d\n",
			       ipwireless_cs_out_queue);
		}
	}
	kfree(command);

	/* Report any created or destroyed ttys */
	for (i = 0; i < IPWIRELESS_PCMCIA_MINORS; ++i) {
		struct ipw_tty *can_open = get_tty(i + IPWIRELESS_PCMCIA_START);

		if (can_open && !could_open[i])
			report_registering(can_open);
		if (!can_open && could_open[i])
			report_deregistering(could_open[i]);
	}
	return count;
}

int ipwireless_tty_init(int major)
{
	int result;

	ipw_tty_driver = alloc_tty_driver(IPWIRELESS_PCMCIA_MINORS);
	if (!ipw_tty_driver)
		return -ENOMEM;

	ipw_tty_driver->owner = THIS_MODULE;
	ipw_tty_driver->driver_name = IPWIRELESS_PCCARD_NAME;
	ipw_tty_driver->name = "ttyIPWp";
	ipw_tty_driver->major = major;
	ipw_tty_driver->minor_start = IPWIRELESS_PCMCIA_START;
	ipw_tty_driver->type = TTY_DRIVER_TYPE_SERIAL;
	ipw_tty_driver->subtype = SERIAL_TYPE_NORMAL;
	ipw_tty_driver->flags = TTY_DRIVER_REAL_RAW;
	ipw_tty_driver->init_termios = tty_std_termios;
	ipw_tty_driver->init_termios.c_cflag =
	    B9600 | CS8 | CREAD | HUPCL | CLOCAL;
	tty_set_operations(ipw_tty_driver, &tty_ops);
	result = tty_register_driver(ipw_tty_driver);
	if (result) {
		printk(KERN_ERR IPWIRELESS_PCCARD_NAME
		       ": failed to register tty driver\n");
		put_tty_driver(ipw_tty_driver);
		return result;
	}

	{
		struct proc_dir_entry *proc_ent;

		proc_ent =
		    create_proc_entry(IPWIRELESS_PCCARD_NAME,
				      S_IRUGO | S_IWUSR | S_IWGRP,
				      &proc_root);
		if (proc_ent != NULL) {
			proc_ent->read_proc = proc_read;
			proc_ent->write_proc = proc_write;
		}
	}

	return 0;
}

void ipwireless_tty_release(void)
{
	int ret, loops = 0;

	remove_proc_entry(IPWIRELESS_PCCARD_NAME, &proc_root);

	while (1) {
		ret = tty_unregister_driver(ipw_tty_driver);
		put_tty_driver(ipw_tty_driver);
		if (ret != 0) {
			if (loops == 0)
				printk(KERN_ERR IPWIRELESS_PCCARD_NAME
				       ": tty_unregister_driver failed %d - "
				       "retrying...\n", ret);
		} else
			break;
		set_current_state(TASK_INTERRUPTIBLE);
		schedule_timeout(HZ / 5);
		loops++;
	}
}

int ipwireless_tty_is_modem(struct ipw_tty *tty)
{
	return tty->tty_type == TTYTYPE_MODEM;
}

void
ipwireless_tty_notify_control_line_change(struct ipw_tty *tty,
					  unsigned int channel_idx,
					  unsigned int control_lines,
					  unsigned int changed_mask)
{
	unsigned int old_control_lines = tty->control_lines;

	tty->control_lines = (tty->control_lines & ~changed_mask)
		| (control_lines & changed_mask);

	/*
	 * If DCD is de-asserted, we close the tty so pppd can tell that we
	 * have gone offline.
	 */
	if ((old_control_lines & IPW_CONTROL_LINE_DCD)
			&& !(tty->control_lines & IPW_CONTROL_LINE_DCD)
			&& tty->linux_tty) {
		tty_hangup(tty->linux_tty);
	}
}

