/*
 * IPWireless 3G PCMCIA Network Driver
 *
 * Original code
 *   by Stephen Blackheath <stephen@blacksapphire.com>,
 *      Ben Martel <benm@symmetric.co.nz>
 *
 * Copyrighted as follows:
 *   Copyright (C) 2004 by Symmetric Systems Ltd (NZ)
 *
 * Various driver changes and rewrites, port to new kernels
 *   Copyright (C) 2006-2007 Jiri Kosina
 *
 * Misc code cleanups and updates
 *   Copyright (C) 2007 David Sterba
 */

#include "ipwireless_cs_hardware.h"
#include "ipwireless_cs_network.h"
#include "ipwireless_cs_main.h"
#include "ipwireless_cs_tty.h"

#include <linux/autoconf.h>
#include <linux/version.h>

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/io.h>

#include <pcmcia/version.h>
#include <pcmcia/cisreg.h>
#include <pcmcia/device_id.h>
#include <pcmcia/ss.h>
#include <pcmcia/ds.h>
#include <pcmcia/cs.h>
#include <linux/delay.h>

static struct ipw_config default_config = {
	"*99#"			/* phone number */
};

static struct pcmcia_device_id ipw_ids[] = {
	PCMCIA_DEVICE_MANF_CARD(0x02f2, 0x0100),
	PCMCIA_DEVICE_MANF_CARD(0x02f2, 0x0200),
	PCMCIA_DEVICE_NULL
};
MODULE_DEVICE_TABLE(pcmcia, ipw_ids);

static void ipwireless_detach(struct pcmcia_device *link);

/* Options configurable through the proc filesystem. */
__u16 ipwireless_cs_debug;
__u16 ipwireless_cs_loopback;
__u16 ipwireless_cs_out_queue = 1;
static int major;

module_param(major, int, 0);
MODULE_PARM_DESC(major, "ttyIPWp major number [0]");

static char *drv_name = IPWIRELESS_PCCARD_NAME;

/* Executes in process context. */
static void signalled_reboot_work(struct work_struct *work_reboot)
{
	struct ipw_dev *ipw = container_of(work_reboot, struct ipw_dev,
			work_reboot);
	struct pcmcia_device *link = ipw->link;
	int ret = pccard_reset_card(link->socket);

	if (ret != CS_SUCCESS)
		cs_error(link, ResetCard, ret);
}

static void signalled_reboot_callback(void *callback_data)
{
	struct ipw_dev *ipw = (struct ipw_dev *) callback_data;

	/* Delegate to process context. */
	schedule_work(&ipw->work_reboot);
}

static int config_ipwireless(struct ipw_dev *ipw)
{
	struct pcmcia_device *link = ipw->link;
	int ret;
	config_info_t conf;
	tuple_t tuple;
	unsigned short buf[64];
	cisparse_t parse;
	unsigned short cor_value;
	win_req_t reqAM;
	win_req_t reqCM;
	memreq_t memAM;
	memreq_t memCM;

	ipw->isV2Card = 0;

	tuple.Attributes = 0;
	tuple.TupleData = (cisdata_t *) buf;
	tuple.TupleDataMax = sizeof(buf);
	tuple.TupleOffset = 0;

	tuple.DesiredTuple = RETURN_FIRST_TUPLE;

	ret = pcmcia_get_first_tuple(link, &tuple);

	while (ret == 0) {
		ret = pcmcia_get_tuple_data(link, &tuple);

		if (ret != CS_SUCCESS) {
			cs_error(link, GetTupleData, ret);
			goto exit0;
		}
		ret = pcmcia_get_next_tuple(link, &tuple);
	}

	tuple.DesiredTuple = CISTPL_CFTABLE_ENTRY;

	ret = pcmcia_get_first_tuple(link, &tuple);

	if (ret != CS_SUCCESS) {
		cs_error(link, GetFirstTuple, ret);
		goto exit0;
	}

	ret = pcmcia_get_tuple_data(link, &tuple);

	if (ret != CS_SUCCESS) {
		cs_error(link, GetTupleData, ret);
		goto exit0;
	}

	ret = pcmcia_parse_tuple(link, &tuple, &parse);

	if (ret != CS_SUCCESS) {
		cs_error(link, ParseTuple, ret);
		goto exit0;
	}

	link->io.Attributes1 = IO_DATA_PATH_WIDTH_AUTO;
	link->io.BasePort1 = parse.cftable_entry.io.win[0].base;
	link->io.NumPorts1 = parse.cftable_entry.io.win[0].len;
	link->io.IOAddrLines = 16;

	link->irq.IRQInfo1 = parse.cftable_entry.irq.IRQInfo1;

	/* 0x40 causes it to generate level mode interrupts. */
	/* 0x04 enables IREQ pin. */
	cor_value = parse.cftable_entry.index | 0x44;
	link->conf.ConfigIndex = cor_value;

	/* IRQ and I/O settings */
	tuple.DesiredTuple = CISTPL_CONFIG;

	ret = pcmcia_get_first_tuple(link, &tuple);

	if (ret != CS_SUCCESS) {
		cs_error(link, GetFirstTuple, ret);
		goto exit0;
	}

	ret = pcmcia_get_tuple_data(link, &tuple);

	if (ret != CS_SUCCESS) {
		cs_error(link, GetTupleData, ret);
		goto exit0;
	}

	ret = pcmcia_parse_tuple(link, &tuple, &parse);

	if (ret != CS_SUCCESS) {
		cs_error(link, GetTupleData, ret);
		goto exit0;
	}
	link->conf.Attributes = CONF_ENABLE_IRQ;
	link->conf.ConfigBase = parse.config.base;
	link->conf.Present = parse.config.rmask[0];
	link->conf.IntType = INT_MEMORY_AND_IO;

	link->irq.Attributes = IRQ_TYPE_DYNAMIC_SHARING | IRQ_HANDLE_PRESENT;
	link->irq.Handler = ipwireless_interrupt;
	link->irq.Instance = ipw->hardware;

	ret = pcmcia_request_io(link, &link->io);

	if (ret != CS_SUCCESS) {
		cs_error(link, RequestIO, ret);
		goto exit0;
	}

	/* memory settings */

	tuple.DesiredTuple = CISTPL_CFTABLE_ENTRY;

	ret = pcmcia_get_first_tuple(link, &tuple);

	if (ret != CS_SUCCESS) {
		cs_error(link, GetFirstTuple, ret);
		goto exit1;
	}

	ret = pcmcia_get_tuple_data(link, &tuple);

	if (ret != CS_SUCCESS) {
		cs_error(link, GetTupleData, ret);
		goto exit1;
	}

	ret = pcmcia_parse_tuple(link, &tuple, &parse);

	if (ret != CS_SUCCESS) {
		cs_error(link, ParseTuple, ret);
		goto exit1;
	}

	if (parse.cftable_entry.mem.nwin > 0) {
		reqCM.Attributes =
			WIN_DATA_WIDTH_16 | WIN_MEMORY_TYPE_CM | WIN_ENABLE;
		reqCM.Base = parse.cftable_entry.mem.win[0].host_addr;
		reqCM.Size = parse.cftable_entry.mem.win[0].len;
		if (reqCM.Size < 0x1000)
			reqCM.Size = 0x1000;
		reqCM.AccessSpeed = 0;

		ret = pcmcia_request_window(&link, &reqCM, &ipw->handleCM);

		if (ret != CS_SUCCESS) {
			cs_error(link, RequestWindow, ret);
			goto exit1;
		}

		memCM.CardOffset = parse.cftable_entry.mem.win[0].card_addr;
		memCM.Page = 0;

		ret = pcmcia_map_mem_page(ipw->handleCM, &memCM);

		if (ret != CS_SUCCESS) {
			cs_error(link, MapMemPage, ret);
			goto exit1;
		}

		ipw->isV2Card =
			parse.cftable_entry.mem.win[0].len == 0x100;

		ipw->commonMemory = ioremap(reqCM.Base, reqCM.Size);

		reqAM.Attributes =
			WIN_DATA_WIDTH_16 | WIN_MEMORY_TYPE_AM | WIN_ENABLE;
		reqAM.Base = 0;
		reqAM.Size = 0;	/* this used to be 0x1000 */
		reqAM.AccessSpeed = 0;

		ret = pcmcia_request_window(&link, &reqAM, &ipw->handleAM);

		if (ret != CS_SUCCESS) {
			cs_error(link, RequestWindow, ret);
			goto exit2;
		}

		memAM.CardOffset = 0;
		memAM.Page = 0;

		ret = pcmcia_map_mem_page(ipw->handleAM, &memAM);

		if (ret != CS_SUCCESS) {
			cs_error(link, MapMemPage, ret);
			goto exit2;
		}

		ipw->attrMemory = ioremap(reqAM.Base, reqAM.Size);
	}

	INIT_WORK(&ipw->work_reboot, signalled_reboot_work);

	ipwireless_init_hardware_v1(ipw->hardware, link->io.BasePort1,
				    ipw->attrMemory, ipw->commonMemory,
				    ipw->isV2Card, signalled_reboot_callback,
				    ipw);

	ret = pcmcia_request_irq(link, &link->irq);

	if (ret != CS_SUCCESS) {
		cs_error(link, RequestIRQ, ret);
		goto exit3;
	}

	/* Look up current Vcc */

	ret = pcmcia_get_configuration_info(link, &conf);

	if (ret != CS_SUCCESS) {
		cs_error(link, GetConfigurationInfo, ret);
		goto exit4;
	}

	printk(KERN_INFO IPWIRELESS_PCCARD_NAME ": Card type %s\n",
			ipw->isV2Card ? "V2/V3" : "V1");
	printk(KERN_INFO IPWIRELESS_PCCARD_NAME
			": I/O ports 0x%04x-0x%04x, irq %d\n",
			(unsigned int) link->io.BasePort1,
			(unsigned int) (link->io.BasePort1 +
				link->io.NumPorts1 - 1),
			(unsigned int) link->irq.AssignedIRQ);
	if (ipw->attrMemory && ipw->commonMemory)
		printk(KERN_INFO IPWIRELESS_PCCARD_NAME
				": attr memory 0x%08lx-0x%08lx, "
				"common memory 0x%08lx-0x%08lx\n",
				(unsigned long)reqAM.Base,
				(unsigned long)reqAM.Base +
					(unsigned long)reqAM.Size -
					(unsigned long) 1,
				(unsigned long)reqCM.Base,
				(unsigned long)reqCM.Base +
					(unsigned long)reqCM.Size -
					(unsigned long) 1);

	ipw->network = ipwireless_network_create(ipw->hardware);
	ipw->tty = ipwireless_tty_create(ipw->hardware, ipw->network,
			ipw->nodes);

	ipwireless_init_hardware_v2_v3(ipw->hardware);

	/*
	 * Do the RequestConfiguration last, because it enables interrupts.
	 * Then we don't get any interrupts before we're ready for them.
	 */
	ret = pcmcia_request_configuration(link, &link->conf);

	if (ret != CS_SUCCESS) {
		cs_error(link, RequestConfiguration, ret);
		goto exit4;
	}

	link->dev_node = &ipw->nodes[0];

	return 0;

exit4:
	pcmcia_disable_device(link);
exit3:
	if (ipw->attrMemory) {
		iounmap(ipw->attrMemory);
		pcmcia_release_window(ipw->handleAM);
		pcmcia_disable_device(link);
	}
exit2:
	if (ipw->commonMemory) {
		iounmap(ipw->commonMemory);
		pcmcia_release_window(ipw->handleCM);
	}
exit1:
	pcmcia_disable_device(link);
exit0:
	return -1;
}

static void release_ipwireless(struct ipw_dev *ipw)
{
	struct pcmcia_device *link = ipw->link;

	pcmcia_disable_device(link);

	if (ipw->commonMemory)
		iounmap(ipw->commonMemory);
	if (ipw->attrMemory)
		iounmap(ipw->attrMemory);
	if (ipw->commonMemory)
		pcmcia_release_window(ipw->handleCM);
	if (ipw->attrMemory)
		pcmcia_release_window(ipw->handleAM);
	pcmcia_disable_device(link);
}

/*
 * ipwireless_attach() creates an "instance" of the driver, allocating
 * local data structures for one device (one interface).  The device
 * is registered with Card Services.
 *
 * The pcmcia_device structure is initialized, but we don't actually
 * configure the card at this point -- we wait until we receive a
 * card insertion event.
 */
static int ipwireless_attach(struct pcmcia_device *link)
{
	struct ipw_dev *ipw;
	int ret;

	ipw = kzalloc(sizeof(struct ipw_dev), GFP_KERNEL);

	ipw->link = link;
	link->priv = ipw;
	link->irq.Instance = ipw;

	/* Link this device into our device list. */
	link->dev_node = &ipw->nodes[0];

	ipw->hardware = ipwireless_hardware_create(&default_config);
	/* RegisterClient will call config_ipwireless */

	ret = config_ipwireless(ipw);

	if (ret != 0) {
		cs_error(link, RegisterClient, ret);
		ipwireless_detach(link);
		return ret;
	}

	return 0;
}

/*
 * This deletes a driver "instance".  The device is de-registered with
 * Card Services.  If it has been released, all local data structures
 * are freed.  Otherwise, the structures will be freed when the device
 * is released.
 */
static void ipwireless_detach(struct pcmcia_device *link)
{
	struct ipw_dev *ipw = (struct ipw_dev *) link->priv;

	release_ipwireless(ipw);

	/* Break the link with Card Services */
	if (link)
		pcmcia_disable_device(link);

	if (ipw->tty != NULL)
		ipwireless_tty_free(ipw->tty);
	if (ipw->network != NULL)
		ipwireless_network_free(ipw->network);
	if (ipw->hardware != NULL)
		ipwireless_hardware_free(ipw->hardware);
	kfree(ipw);
}

static struct pcmcia_driver me;

/*
 * Module insertion : initialisation of the module.
 * Register the card with cardmgr...
 */
static int __init init_ipwireless_cs(void)
{
	int ret;

	printk(KERN_INFO IPWIRELESS_PCCARD_NAME " "
	       IPWIRELESS_PCMCIA_VERSION " by " IPWIRELESS_PCMCIA_AUTHOR "\n");
	me.owner = THIS_MODULE;
	me.probe = ipwireless_attach;
	me.remove = ipwireless_detach;
	me.drv.name = drv_name;
	me.id_table = ipw_ids;
	pcmcia_register_driver(&me);

	ret = ipwireless_tty_init(major);
	if (ret != 0)
		pcmcia_unregister_driver(&me);
	return ret;
}

/*
 * Module removal
 */
static void __exit exit_ipwireless_cs(void)
{
	ipwireless_tty_release();

	printk(KERN_INFO IPWIRELESS_PCCARD_NAME " "
			IPWIRELESS_PCMCIA_VERSION " removed\n");

	pcmcia_unregister_driver(&me);
}

module_init(init_ipwireless_cs);
module_exit(exit_ipwireless_cs);

MODULE_AUTHOR(IPWIRELESS_PCMCIA_AUTHOR);
MODULE_DESCRIPTION(IPWIRELESS_PCCARD_NAME " " IPWIRELESS_PCMCIA_VERSION);
MODULE_LICENSE("GPL");
