/*
 * IPWireless 3G PCMCIA Network Driver
 *
 * Original code
 *   by Stephen Blackheath <stephen@blacksapphire.com>,
 *      Ben Martel <benm@symmetric.co.nz>
 *
 * Copyrighted as follows:
 *   Copyright (C) 2004 by Symmetric Systems Ltd (NZ)
 *
 * Various driver changes and rewrites, port to new kernels
 *   Copyright (C) 2006-2007 Jiri Kosina
 *
 * Misc code cleanups and updates
 *   Copyright (C) 2007 David Sterba
 */

#include <linux/autoconf.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/interrupt.h>
#include <linux/list.h>
#include <linux/io.h>
#include <linux/irq.h>

#include "ipwireless_cs_hardware.h"
#include "ipwireless_cs_setup_protocol.h"
#include "ipwireless_cs_network.h"
#include "ipwireless_cs_main.h"

/* Function prototypes */
static void ipw_send_setup_packet(struct ipw_hardware *hw);
static void handle_received_SETUP_packet(struct ipw_hardware *ipw,
					 unsigned int address,
					 unsigned char *data, int len,
					 int is_last);
static void ipwireless_setup_timer(unsigned long data);
static void handle_received_CTRL_packet(struct ipw_hardware *hw,
		unsigned int channel_idx, unsigned char *data, int len);

/*#define TIMING_DIAGNOSTICS*/

#ifdef TIMING_DIAGNOSTICS

static struct timing_stats {
	unsigned long last_report_time;
	unsigned long read_time;
	unsigned long write_time;
	unsigned long read_bytes;
	unsigned long write_bytes;
	unsigned long start_time;
};

static void start_timing(void)
{
	timing_stats.start_time = jiffies;
}

static void end_read_timing(unsigned length)
{
	timing_stats.read_time += (jiffies - start_time);
	timing_stats.read_bytes += length + 2;
	report_timing();
}

static void end_write_timing(unsigned length)
{
	timing_stats.write_time += (jiffies - start_time);
	timing_stats.write_bytes += length + 2;
	report_timing();
}

static void report_timing(void)
{
	unsigned long since = jiffies - timing_stats.last_report_time;

	/* If it's been more than one second... */
	if (since >= HZ) {
		int first = (timing_stats.last_report_time == 0);

		timing_stats.last_report_time = jiffies;
		if (!first)
			printk(KERN_INFO IPWIRELESS_PCCARD_NAME
			       ": %u us elapsed - read %lu bytes in %u us, "
			       "wrote %lu bytes in %u us\n",
			       jiffies_to_usecs(since),
			       timing_stats.read_bytes,
			       jiffies_to_usecs(timing_stats.read_time),
			       timing_stats.write_bytes,
			       jiffies_to_usecs(timing_stats.write_time));

		timing_stats.read_time = 0;
		timing_stats.write_time = 0;
		timing_stats.read_bytes = 0;
		timing_stats.write_bytes = 0;
	}
}
#else
static void start_timing(void) { }
static void end_read_timing(unsigned length) { }
static void end_write_timing(unsigned length) { }
#endif

/* Imported IPW definitions */

#define LL_MTU_V1 318
#define LL_MTU_V2 250
#define LL_MTU_MAX (LL_MTU_V1 > LL_MTU_V2 ? LL_MTU_V1 : LL_MTU_V2)

#define PRIO_DATA  2
#define PRIO_CTRL  1
#define PRIO_SETUP 0

/* Addresses */
#define ADDR_SETUP_PROT 0

/* Protocol ids */
enum {
	/* Identifier for the Com Data protocol */
	TL_PROTOCOLID_COM_DATA = 0,

	/* Identifier for the Com Control protocol */
	TL_PROTOCOLID_COM_CTRL = 1,

	/* Identifier for the Setup protocol */
	TL_PROTOCOLID_SETUP = 2
};

/* Number of bytes in NL packet header (can not do
 * sizeof(nl_packet_header) since it's a bitfield) */
#define NL_FIRST_PACKET_HEADER_SIZE        3

/* Number of bytes in NL packet header (can not do
 * sizeof(nl_packet_header) since it's a bitfield) */
#define NL_FOLLOWING_PACKET_HEADER_SIZE    1

struct nl_first_paket_header {
#if defined(__BIG_ENDIAN)
	unsigned char packet_rank:2;
	unsigned char address:3;
	unsigned char protocol:3;
#else
	unsigned char protocol:3;
	unsigned char address:3;
	unsigned char packet_rank:2;
#endif
	unsigned char length_lsb;
	unsigned char length_msb;
};

struct nl_packet_header {
#if defined(__BIG_ENDIAN)
	unsigned char packet_rank:2;
	unsigned char address:3;
	unsigned char protocol:3;
#else
	unsigned char protocol:3;
	unsigned char address:3;
	unsigned char packet_rank:2;
#endif
};

/* Value of 'packet_rank' above */
#define NL_INTERMEDIATE_PACKET    0x0
#define NL_LAST_PACKET            0x1
#define NL_FIRST_PACKET           0x2

union nl_packet {
	/* Network packet header of the first packet (a special case) */
	struct nl_first_paket_header hdr_first;
	/* Network packet header of the following packets (if any) */
	struct nl_packet_header hdr;
	/* Complete network packet (header + data) */
	unsigned char rawpkt[LL_MTU_MAX];
} __attribute__ ((__packed__));

#define HW_VERSION_UNKNOWN -1
#define HW_VERSION_1 1
#define HW_VERSION_2 2

/* IPW I/O ports */
#define IOIER 0x00		/* Interrupt Enable Register */
#define IOIR  0x02		/* Interrupt Source/ACK register */
#define IODCR 0x04		/* Data Control Register */
#define IODRR 0x06		/* Data Read Register */
#define IODWR 0x08		/* Data Write Register */
#define IOESR 0x0A		/* Embedded Driver Status Register */
#define IORXR 0x0C		/* Rx Fifo Register (Host to Embedded) */
#define IOTXR 0x0E		/* Tx Fifo Register (Embedded to Host) */

/* I/O ports and bit definitions for version 1 of the hardware */

/* IER bits*/
#define IER_RXENABLED   ((unsigned short) 0x1)
#define IER_TXENABLED   ((unsigned short) 0x2)

/* ISR bits */
#define IR_RXINTR       ((unsigned short) 0x1)
#define IR_TXINTR       ((unsigned short) 0x2)

/* DCR bits */
#define DCR_RXDONE      ((unsigned short) 0x1)
#define DCR_TXDONE      ((unsigned short) 0x2)
#define DCR_RXRESET     ((unsigned short) 0x4)
#define DCR_TXRESET     ((unsigned short) 0x8)

/* I/O ports and bit definitions for version 2 of the hardware */

struct MEMCCR {
	unsigned short PCCOR;		/* Configuration Option Register */
	unsigned short PCCSR;		/* Configuration and Status Register */
	unsigned short PCPRR;		/* Pin Replacemant Register */
	unsigned short PCSCR;		/* Socket and Copy Register */
	unsigned short PCESR;		/* Extendend Status Register */
	unsigned short PCIOB;		/* I/O Base Register */
};

struct MEMINFREG {
	unsigned short memreg_tx_old;	/* TX Register (R/W) */
	unsigned short pad1;
	unsigned short memreg_rx_done;	/* RXDone Register (R/W) */
	unsigned short pad2;
	unsigned short memreg_rx;	/* RX Register (R/W) */
	unsigned short pad3;
	unsigned short memreg_pc_interrupt_ack;	/* PC intr Ack Register (W) */
	unsigned short pad4;
	unsigned long memreg_card_present;/* Mask for Host to check (R) for
					   * CARD_PRESENT_VALUE */
	unsigned short memreg_tx_new;	/* TX2 (new) Register (R/W) */
};

#define IODMADPR 0x00		/* DMA Data Port Register (R/W) */

#define CARD_PRESENT_VALUE ((unsigned long)0xBEEFCAFEUL)

#define MEMTX_TX                       0x0001
#define MEMRX_RX                       0x0001
#define MEMRX_RX_DONE                  0x0001
#define MEMRX_PCINTACKK                0x0001
#define MEMRX_MEMSPURIOUSINT           0x0001

#define NL_NUM_OF_PRIORITIES       3
#define NL_NUM_OF_PROTOCOLS        3
#define NL_NUM_OF_ADDRESSES        NO_OF_IPW_CHANNELS

struct ipw_hardware {
	unsigned int base_port;
	short hw_version;
	unsigned short ll_mtu;
	struct ipw_config config;
	spinlock_t spinlock;

	int initializing;
	int init_loops;
	struct timer_list setup_timer;

	int tx_ready;
	struct list_head tx_queue[NL_NUM_OF_PRIORITIES];
	/* True if any packets are queued for transmission */
	int tx_queued;

	int rx_bytes_queued;
	struct list_head rx_queue;
	/* Pool of rx_packet structures that are not currently used. */
	struct list_head rx_pool;
	/* True if reception of data is blocked while userspace processes it. */
	int blocking_rx;
	/* True if there is RX data ready on the hardware. */
	int rx_ready;
	unsigned short last_memtx_serial;
	/*
	 * Newer versions of the V2 card firmware send serial numbers in the
	 * MemTX register. 'serial_number_detected' is set true when we detect
	 * a non-zero serial number (indicating the new firmware).  Thereafter,
	 * the driver can safely ignore the Timer Recovery re-sends to avoid
	 * out-of-sync problems.
	 */
	int serial_number_detected;
	struct work_struct work_rx;

	/* True if we are to send the set-up data to the hardware. */
	int to_setup;

	/* Card has been removed */
	int removed;
	/* Saved irq value when we disable the interrupt. */
	int irq;
	/* True if this driver is shutting down. */
	int shutting_down;
	/* Modem control lines */
	unsigned int control_lines[NL_NUM_OF_ADDRESSES];
	struct ipw_rx_packet *packet_assembler[NL_NUM_OF_ADDRESSES];

	struct tasklet_struct tasklet;

	/* The handle for the network layer, for the sending of events to it. */
	struct ipw_network *network;
	struct MEMINFREG __iomem *memory_info_regs;
	struct MEMCCR __iomem *memregs_CCR;
	int bad_interrupt_count;
	void (*reboot_callback) (void *data);
	void *reboot_callback_data;

	unsigned short __iomem *memreg_tx;
};

/*
 * Packet info structure for tx packets.
 * Note: not all the fields defined here are required for all protocols
 */
struct ipw_tx_packet {
	struct list_head queue;
	/* channel idx + 1 */
	unsigned char dest_addr;
	/* SETUP, CTRL or DATA */
	unsigned char protocol;
	/* Length of data block, which starts at the end of this structure */
	unsigned short length;
	/* Function to call upon packet completion. */
	void (*packet_sent_callback) (struct ipw_tx_packet *packet);
	/* Sending state */
	/* Offset of where we've sent up to so far */
	unsigned long offset;
	/* Count of packet fragments, starting at 0 */
	int fragment_count;

	void (*external_callback) (void *cb, unsigned int length);
	void *external_callback_data;
};

/* Signals from DTE */
enum ComCtrl_DTESignal {
	ComCtrl_RTS = 0,
	ComCtrl_DTR = 1
};

/* Signals from DCE */
enum ComCtrl_DCESignal {
	ComCtrl_CTS = 2,
	ComCtrl_DCD = 3,
	ComCtrl_DSR = 4,
	ComCtrl_RI = 5
};

struct ipw_control_packet_body {
	/* ComCtrl_DTESignal or ComCtrl_DCESignal */
	unsigned char sig_no;
	/* ComCtrl_SET(0) or ComCtrl_CLEAR(1) */
	unsigned char value;
} __attribute__ ((__packed__));

struct ipw_control_packet {
	struct ipw_tx_packet packet;
	struct ipw_control_packet_body body;
};

struct ipw_rx_packet {
	struct list_head queue;
	unsigned int capacity;
	unsigned int length;
	unsigned int protocol;
	unsigned int channel_idx;
};

#ifdef IPWIRELESS_STATE_DEBUG
int ipwireless_dump_hardware_state(char *p, struct ipw_hardware *hw)
{
	int idx = 0;

	idx += sprintf(p + idx, "debug: initializing=%d\n", hw->initializing);
	idx += sprintf(p + idx, "debug: tx_ready=%d\n", hw->tx_ready);
	idx += sprintf(p + idx, "debug: tx_queued=%d\n", hw->tx_queued);
	idx += sprintf(p + idx, "debug: rx_ready=%d\n", hw->rx_ready);
	idx += sprintf(p + idx, "debug: rx_bytes_queued=%d\n",
			hw->rx_bytes_queued);
	idx += sprintf(p + idx, "debug: blocking_rx=%d\n", hw->blocking_rx);
	idx += sprintf(p + idx, "debug: removed=%d\n", hw->removed);
	idx += sprintf(p + idx, "debug: hardware.shutting_down=%d\n",
			hw->shutting_down);
	idx += sprintf(p + idx, "debug: to_setup=%d\n",
			hw->to_setup);
	return idx;
}
#endif

static char *data_type(const unsigned char *buf, unsigned length)
{
	struct nl_packet_header *hdr = (struct nl_packet_header *) buf;

	if (length == 0)
		return "     ";

	if (hdr->packet_rank & NL_FIRST_PACKET) {
		switch (hdr->protocol) {
		case TL_PROTOCOLID_COM_DATA:	return "DATA ";
		case TL_PROTOCOLID_COM_CTRL:	return "CTRL ";
		case TL_PROTOCOLID_SETUP:	return "SETUP";
		default: return "???? ";
		}
	} else
		return "     ";
}

#define DUMP_MAX_BYTES 64

static void dump_data_bytes(const char *prefix, const unsigned char *data,
			    unsigned length)
{
	int i;
	char buf[DUMP_MAX_BYTES * 4 + 16];

	buf[0] = 0;
	for (i = 0; i < length && i < DUMP_MAX_BYTES; i++)
		sprintf(buf + strlen(buf), " %02x", (unsigned int) data[i]);
	if (i > DUMP_MAX_BYTES)
		strcat(buf, " ...");
	printk(KERN_INFO IPWIRELESS_PCCARD_NAME ": %s fragment %s %s\n",
			prefix, data_type(data, length), buf);
}

/*
 * Send a fragment of a packet.
 */
static int do_send_fragment(struct ipw_hardware *hw, const unsigned char *data,
			    unsigned length)
{
	int i;

	start_timing();

	if (length == 0)
		return 0;

	if (length > hw->ll_mtu)
		return -1;

	if (ipwireless_cs_debug)
		dump_data_bytes("send", data, length);

	if (hw->hw_version == HW_VERSION_1) {
		outw((unsigned short) length, hw->base_port + IODWR);

		for (i = 0; i < length; i += 2) {
			unsigned short d = data[i];
			__le16 raw_data;

			if (likely(i + 1 < length))
				d |= data[i + 1] << 8;
			raw_data = cpu_to_le16(d);
			outw(raw_data, hw->base_port + IODWR);
		}

		outw(DCR_TXDONE, hw->base_port + IODCR);
	} else if (hw->hw_version == HW_VERSION_2) {
		outw((unsigned short) length, hw->base_port + IODMADPR);

		for (i = 0; i < length; i += 2) {
			unsigned short d = data[i];
			__le16 raw_data;

			if ((i + 1 < length))
				d |= data[i + 1] << 8;
			raw_data = cpu_to_le16(d);
			outw(raw_data, hw->base_port + IODMADPR);
		}
		while ((i & 3) != 2) {
			outw((unsigned short) 0xDEAD, hw->base_port + IODMADPR);
			i += 2;
		}
		iowrite16(MEMRX_RX, &hw->memory_info_regs->memreg_rx);
	}

	end_write_timing(length);

	return 0;
}

/* Called with spinlock in force.  It unlocks, then re-locks it. */
static int do_send_packet(struct ipw_hardware *hw, struct ipw_tx_packet *packet)
{
	unsigned short fragment_data_len;
	unsigned short data_left = packet->length - packet->offset;
	unsigned short header_size;
	union nl_packet pkt;

	spin_unlock_irq(&hw->spinlock);
	header_size =
	    (packet->fragment_count == 0)
	    ? NL_FIRST_PACKET_HEADER_SIZE
	    : NL_FOLLOWING_PACKET_HEADER_SIZE;
	fragment_data_len = (unsigned short) hw->ll_mtu - header_size;
	if (data_left < fragment_data_len)
		fragment_data_len = data_left;

	pkt.hdr_first.protocol = packet->protocol;
	pkt.hdr_first.address = packet->dest_addr;
	pkt.hdr_first.packet_rank = 0;

	/* First packet? */
	if (packet->fragment_count == 0) {
		pkt.hdr_first.packet_rank |= NL_FIRST_PACKET;
		pkt.hdr_first.length_lsb = (unsigned char) packet->length;
		pkt.hdr_first.length_msb =
			(unsigned char) (packet->length >> 8);
	}

	memcpy(pkt.rawpkt + header_size,
	       ((unsigned char *) packet) + sizeof(struct ipw_tx_packet) +
	       packet->offset, fragment_data_len);
	packet->offset += fragment_data_len;
	packet->fragment_count++;

	/* Last packet? (May also be first packet.) */
	if (packet->offset == packet->length)
		pkt.hdr_first.packet_rank |= NL_LAST_PACKET;
	do_send_fragment(hw, pkt.rawpkt, header_size + fragment_data_len);

	/* If this packet has unsent data, then re-queue it. */
	if (packet->offset < packet->length) {
		spin_lock_irq(&hw->spinlock);
		/*
		 * Re-queue it at the head of the highest priority queue so
		 * it goes before all other packets
		 */
		list_add(&packet->queue, &hw->tx_queue[0]);
	} else {
		packet->packet_sent_callback(packet);
		spin_lock_irq(&hw->spinlock);
	}

	return 0;
}

static void free_packet(struct ipw_tx_packet *packet)
{
       kfree(packet);
}

static void free_packet_and_callback(struct ipw_tx_packet *packet)
{
	if (packet->external_callback)
		packet->external_callback(packet->external_callback_data,
					  packet->length);

	kfree(packet);
}

static void ipw_setup_hardware(struct ipw_hardware *hw)
{
	if (hw->hw_version == HW_VERSION_1) {
		/* Reset RX FIFO */
		outw(DCR_RXRESET, hw->base_port + IODCR);
		/* SB: Reset TX FIFO */
		outw(DCR_TXRESET, hw->base_port + IODCR);

		/* Enable TX and RX interrupts. */
		outw(IER_TXENABLED | IER_RXENABLED, hw->base_port + IOIER);
	} else {
		/*
		 * Set INTRACK bit (bit 0), which means we must explicitly
		 * acknowledge interrupts by clearing bit 2 of PCCSR.
		 */
		unsigned short csr = ioread16(&hw->memregs_CCR->PCCSR);

		csr |= 1;
		iowrite16(csr, &hw->memregs_CCR->PCCSR);
	}
}

/*
 * If 'packet' is NULL, then this function allocates a new packet, setting its
 * length to 0 and ensuring it has the specified minimum amount of free space.
 *
 * If 'packet' is not NULL, then this function enlarges it if it doesn't
 * have the specified minimum amount of free space.
 */
static struct ipw_rx_packet *pool_allocate(struct ipw_hardware *hw,
					   struct ipw_rx_packet *packet,
					   int minimum_free_space)
{
	unsigned long flags;

	if (!packet) {
		/*
		 * If this is the first fragment, then we will need to fetch a
		 * packet to put it in.
		 */
		spin_lock_irqsave(&hw->spinlock, flags);
		/* If we have one in our pool, then pull it out. */
		if (!list_empty(&hw->rx_pool)) {
			packet = list_first_entry(&hw->rx_pool,
					struct ipw_rx_packet, queue);
			list_del_init(&packet->queue);
			spin_unlock_irqrestore(&hw->spinlock, flags);
		} else {
			/* Otherwise allocate a new one. */
			static int min_capacity = 256;
			int new_capacity;

			spin_unlock_irqrestore(&hw->spinlock, flags);
			new_capacity =
			    minimum_free_space > min_capacity
			    ? minimum_free_space
			    : min_capacity;
			packet = kmalloc(sizeof(struct ipw_rx_packet)
					+ new_capacity, GFP_ATOMIC);
			packet->capacity = new_capacity;
		}
		packet->length = 0;
	}

	/*
	 * If this packet does not have sufficient capacity for the data we
	 * want to add, then make it bigger.
	 */
	if (packet->length + minimum_free_space > packet->capacity) {
		struct ipw_rx_packet *old_packet = packet;

		packet = kmalloc(sizeof(struct ipw_rx_packet) +
				old_packet->length + minimum_free_space,
				GFP_ATOMIC);
		memcpy(packet, old_packet,
				sizeof(struct ipw_rx_packet)
					+ old_packet->length);
		packet->capacity = old_packet->length + minimum_free_space;
		kfree(old_packet);
	}

	return packet;
}

static void pool_free(struct ipw_hardware *hw, struct ipw_rx_packet *packet)
{
	static int ticker;

	/*
	 * Every now and again we release one instead of pushing it back onto
	 * the pool.  This isn't perfectly efficient, but it prevents us
	 * wasting memory.
	 */
	if ((ticker++ & 0x3f) == 0)
		kfree(packet);
	else
		list_add_tail(&packet->queue, &hw->rx_pool);
}

static void queue_received_packet(struct ipw_hardware *hw,
				  unsigned int protocol, unsigned int address,
				  unsigned char *data, int length, int is_last)
{
	unsigned int channel_idx = address - 1;
	struct ipw_rx_packet *packet = NULL;

	/* Discard packet if channel index is out of range. */
	if (channel_idx >= NL_NUM_OF_ADDRESSES) {
		printk(KERN_INFO IPWIRELESS_PCCARD_NAME
		       ": data packet has bad address %u\n", address);
		return;
	}

	if (protocol == TL_PROTOCOLID_COM_DATA) {
		struct ipw_rx_packet **assem =
			&hw->packet_assembler[channel_idx];

		/*
		 * Create a new packet, or assembler already contains one
		 * enlarge it by 'length' bytes.
		 */
		(*assem) = pool_allocate(hw, *assem, length);
		(*assem)->protocol = protocol;
		(*assem)->channel_idx = channel_idx;

		/* Append this packet data onto existing data. */
		memcpy((unsigned char *)(*assem) +
			       sizeof(struct ipw_rx_packet)
				+ (*assem)->length, data, length);
		(*assem)->length += length;
		if (is_last) {
			packet = *assem;
			*assem = NULL;
			/* Count queued DATA bytes only */
			hw->rx_bytes_queued += packet->length;
		}
	} else {
		/* If it's a CTRL packet, don't assemble, just queue it. */
		packet = pool_allocate(hw, NULL, length);
		packet->protocol = protocol;
		packet->channel_idx = channel_idx;
		memcpy((unsigned char *)packet + sizeof(struct ipw_rx_packet),
				data, length);
		packet->length = length;
	}


	/*
	 * If this is the last packet, then send the assembled packet on to the
	 * network layer.
	 */
	if (packet) {
		unsigned long flags;

		spin_lock_irqsave(&hw->spinlock, flags);
		list_add_tail(&packet->queue, &hw->rx_queue);
		/* Block reception of incoming packets if queue is full. */
		hw->blocking_rx =
			hw->rx_bytes_queued >= IPWIRELESS_RX_QUEUE_SIZE;

		spin_unlock_irqrestore(&hw->spinlock, flags);
		schedule_work(&hw->work_rx);
	}
}

/*
 * Workqueue callback
 */
static void ipw_receive_data_work(struct work_struct *work_rx)
{
	struct ipw_hardware *hw =
	    container_of(work_rx, struct ipw_hardware, work_rx);
	unsigned long flags;

	spin_lock_irqsave(&hw->spinlock, flags);
	while (!list_empty(&hw->rx_queue)) {
		struct ipw_rx_packet *packet =
			list_first_entry(&hw->rx_queue,
					struct ipw_rx_packet, queue);

		if (hw->shutting_down)
			break;
		list_del_init(&packet->queue);

		/*
		 * Note: ipwireless_network_packet_received must be called in a
		 * process context (i.e. via schedule_work) because the tty
		 * output code can sleep in the tty_flip_buffer_push call.
		 */
		if (packet->protocol == TL_PROTOCOLID_COM_DATA) {
			if (hw->network != NULL) {
				/* If the network hasn't been disconnected. */
				spin_unlock_irqrestore(&hw->spinlock, flags);
				/*
				 * This must run unlocked due to tty processing
				 * and mutex locking
				 */
				ipwireless_network_packet_received(
						hw->network,
						packet->channel_idx,
						(unsigned char *)packet
						+ sizeof(struct ipw_rx_packet),
						packet->length);
				spin_lock_irqsave(&hw->spinlock, flags);
			}
			/* Count queued DATA bytes only */
			hw->rx_bytes_queued -= packet->length;
		} else {
			/*
			 * This is safe to be called locked, callchain does
			 * not block
			 */
			handle_received_CTRL_packet(hw, packet->channel_idx,
					(unsigned char *)packet
					+ sizeof(struct ipw_rx_packet),
					packet->length);
		}
		pool_free(hw, packet);
		/*
		 * Unblock reception of incoming packets if queue is no longer
		 * full.
		 */
		hw->blocking_rx =
			hw->rx_bytes_queued >= IPWIRELESS_RX_QUEUE_SIZE;
		if (hw->shutting_down)
			break;
	}
	spin_unlock_irqrestore(&hw->spinlock, flags);
}

static void handle_received_CTRL_packet(struct ipw_hardware *hw,
					unsigned int channel_idx,
					unsigned char *data, int len)
{
	struct ipw_control_packet_body *body =
		(struct ipw_control_packet_body *) data;
	unsigned int changed_mask;

	if (len != sizeof(struct ipw_control_packet_body)) {
		printk(KERN_INFO IPWIRELESS_PCCARD_NAME
		       ": control packet was %d bytes - wrong size!\n",
		       len);
		return;
	}

	switch (body->sig_no) {
	case ComCtrl_CTS:
		changed_mask = IPW_CONTROL_LINE_CTS;
		break;
	case ComCtrl_DCD:
		changed_mask = IPW_CONTROL_LINE_DCD;
		break;
	case ComCtrl_DSR:
		changed_mask = IPW_CONTROL_LINE_DSR;
		break;
	case ComCtrl_RI:
		changed_mask = IPW_CONTROL_LINE_RI;
		break;
	default:
		changed_mask = 0;
	}

	if (changed_mask != 0) {
		if (body->value)
			hw->control_lines[channel_idx] |= changed_mask;
		else
			hw->control_lines[channel_idx] &= ~changed_mask;
		if (hw->network)
			ipwireless_network_notify_control_line_change(
					hw->network,
					channel_idx,
					hw->control_lines[channel_idx],
					changed_mask);
	}
}

static void handle_received_packet(struct ipw_hardware *hw,
				   union nl_packet *packet,
				   unsigned short len)
{
	unsigned int protocol = packet->hdr.protocol;
	unsigned int address = packet->hdr.address;
	unsigned int header_length;
	unsigned char *data;
	unsigned int data_len;
	int is_last = packet->hdr.packet_rank & NL_LAST_PACKET;

	if (packet->hdr.packet_rank & NL_FIRST_PACKET)
		header_length = NL_FIRST_PACKET_HEADER_SIZE;
	else
		header_length = NL_FOLLOWING_PACKET_HEADER_SIZE;

	data = packet->rawpkt + header_length;
	data_len = len - header_length;
	switch (protocol) {
	case TL_PROTOCOLID_COM_DATA:
	case TL_PROTOCOLID_COM_CTRL:
		queue_received_packet(hw, protocol, address, data, data_len,
				is_last);
		break;
	case TL_PROTOCOLID_SETUP:
		handle_received_SETUP_packet(hw, address, data, data_len,
				is_last);
		break;
	}
}

static void acknowledge_data_read(struct ipw_hardware *hw)
{
	if (hw->hw_version == HW_VERSION_1)
		outw(DCR_RXDONE, hw->base_port + IODCR);
	else
		iowrite16(MEMRX_PCINTACKK,
				&hw->memory_info_regs->memreg_pc_interrupt_ack);
}

/*
 * Retrieve a packet from the IPW hardware.
 */
static void do_receive_packet(struct ipw_hardware *hw)
{
	unsigned short len;
	unsigned int i;
	unsigned char pkt[LL_MTU_MAX];

	start_timing();

	if (hw->hw_version == HW_VERSION_1) {
		len = inw(hw->base_port + IODRR);
		if (len > hw->ll_mtu) {
			printk(KERN_INFO IPWIRELESS_PCCARD_NAME
			       ": received a packet of %d bytes - "
			       "longer than the MTU!\n", (unsigned int)len);
			outw(DCR_RXDONE | DCR_RXRESET, hw->base_port + IODCR);
			return;
		}

		for (i = 0; i < len; i += 2) {
			__le16 raw_data = inw(hw->base_port + IODRR);
			unsigned short data = le16_to_cpu(raw_data);

			pkt[i] = (unsigned char) data;
			pkt[i + 1] = (unsigned char) (data >> 8);
		}
	} else {
		len = inw(hw->base_port + IODMADPR);
		if (len > hw->ll_mtu) {
			printk(KERN_INFO IPWIRELESS_PCCARD_NAME
			       ": received a packet of %d bytes - "
			       "longer than the MTU!\n", (unsigned int)len);
			iowrite16(MEMRX_PCINTACKK,
				&hw->memory_info_regs->memreg_pc_interrupt_ack);
			return;
		}

		for (i = 0; i < len; i += 2) {
			__le16 raw_data = inw(hw->base_port + IODMADPR);
			unsigned short data = le16_to_cpu(raw_data);

			pkt[i] = (unsigned char) data;
			pkt[i + 1] = (unsigned char) (data >> 8);
		}

		while ((i & 3) != 2) {
			inw(hw->base_port + IODMADPR);
			i += 2;
		}
	}

	acknowledge_data_read(hw);

	if (ipwireless_cs_debug)
		dump_data_bytes("recv", pkt, len);

	handle_received_packet(hw, (union nl_packet *) pkt, len);

	end_read_timing(len);
}

static int get_current_packet_priority(struct ipw_hardware *hw)
{
	/*
	 * If we're initializing, don't send anything of higher priority than
	 * PRIO_SETUP.  The network layer therefore need not care about
	 * hardware initialization - any of its stuff will simply be queued
	 * until setup is complete.
	 */
	return (hw->to_setup || hw->initializing
			? PRIO_SETUP + 1 :
			NL_NUM_OF_PRIORITIES);
}

/*
 * TODO: remove intra-func spinlock
 *
 * @return 1 if something has been received from hw
 */
static int get_packets_from_hw(struct ipw_hardware *hw)
{
	int received = 0;

	while (hw->rx_ready && !hw->blocking_rx) {
		received = 1;
		hw->rx_ready--;
		spin_unlock_irq(&hw->spinlock);
		do_receive_packet(hw);
		spin_lock_irq(&hw->spinlock);
	}

	return received;
}

/*
 * Send pending packet up to given priority, prioritize SETUP data until
 * hardware is fully setup.
 *
 * @return 1 if more packets can be sent
 */
static int send_pending_packet(struct ipw_hardware *hw, int priority_limit)
{
	int more_to_send = 0;

	if (hw->tx_queued && hw->tx_ready != 0) {
		int priority;

		hw->tx_ready--;

		for (priority = 0; priority < priority_limit; priority++) {
			if (!list_empty(&hw->tx_queue[priority])) {
				struct ipw_tx_packet *packet =
					list_first_entry(
						&hw->tx_queue[priority],
						struct ipw_tx_packet,
						queue);

				list_del_init(&packet->queue);

				/*
				 * do_send_packet() unlocks, then re-locks the
				 * spinlock.
				 */
				do_send_packet(hw, packet);
				break;
			}
		}

		/*
		 * Now that we've sent (or not sent) a packet, see if there's
		 * any more to send after that (include all data, including
		 * non-setup data if we are setting up.
		 */
		for (priority = 0; priority < NL_NUM_OF_PRIORITIES; priority++)
			if (!list_empty(&hw->tx_queue[priority])) {
				more_to_send = 1;
				break;
			}

		if (!more_to_send)
			hw->tx_queued = 0;
	}

	return more_to_send;
}

/*
 * Send and receive all queued packets. Must be called with spin_lock locked.
 * May be called either from a interrupt tasklet, or from a process context.
 */
static void ipwireless_do_tasklet(unsigned long hw_)
{
	struct ipw_hardware *hw = (struct ipw_hardware *) hw_;

	spin_lock_irq(&hw->spinlock);
	if (hw->shutting_down) {
		spin_unlock_irq(&hw->spinlock);
		return;
	}

	if (hw->to_setup == 1) {
		/*
		 * Initial setup data sent to hardware
		 */
		hw->to_setup = 2;
		spin_unlock_irq(&hw->spinlock);

		ipw_setup_hardware(hw);
		ipw_send_setup_packet(hw);

		spin_lock_irq(&hw->spinlock);
		send_pending_packet(hw, PRIO_SETUP + 1);
		get_packets_from_hw(hw);
	} else {
		int priority_limit = get_current_packet_priority(hw);
		int again = 0;

		do {
			again = send_pending_packet(hw, priority_limit);
			again |= get_packets_from_hw(hw);
		} while (again);
	}
	spin_unlock_irq(&hw->spinlock);
}

/*!
 * @return true if the card is physically present.
 */
static int is_card_present(struct ipw_hardware *hw)
{
	if (hw->hw_version == HW_VERSION_1)
		return inw(hw->base_port + IOIR) != (unsigned short) 0xFFFF;
	else
		return ioread32(&hw->memory_info_regs->memreg_card_present) ==
		    CARD_PRESENT_VALUE;
}

static irqreturn_t ipwireless_handle_v1_interrupt(int irq,
						  struct ipw_hardware *hw)
{
	unsigned short irqn;
	unsigned short ack;

	irqn = inw(hw->base_port + IOIR);

	/* Check if card is present */
	if (irqn == (unsigned short) 0xFFFF) {
		if (++hw->bad_interrupt_count >= 100) {
			/*
			 * It is necessary to disable the interrupt at this
			 * point, or the kernel hangs, interrupting repeatedly
			 * forever.
			 */
			hw->irq = irq;
			hw->removed = 1;
			disable_irq_nosync(irq);
			printk(KERN_DEBUG IPWIRELESS_PCCARD_NAME
					": Mr. Fluffy is not happy!\n");
		}
		return IRQ_HANDLED;
	} else if (irqn != 0) {
		ack = 0;
		/* Transmit complete. */
		if (irqn & IR_TXINTR) {
			hw->tx_ready++;
			ack |= IR_TXINTR;
		}

		/* Received data */
		if (irqn & IR_RXINTR) {
			ack |= IR_RXINTR;
			hw->rx_ready++;
		}
		if (ack != 0) {
			outw(ack, hw->base_port + IOIR);
			tasklet_schedule(&hw->tasklet);
		}
		return IRQ_HANDLED;
	} else
		return IRQ_NONE;

}

static void acknowledge_pcmcia_interrupt(struct ipw_hardware *hw)
{
	unsigned short csr = ioread16(&hw->memregs_CCR->PCCSR);

	csr &= 0xfffd;
	iowrite16(csr, &hw->memregs_CCR->PCCSR);
}

static irqreturn_t ipwireless_handle_v2_v3_interrupt(int irq,
						     struct ipw_hardware *hw)
{
	int tx = 0;
	int rx = 0;
	int rx_repeat = 0;
	int try_mem_tx_old;

	do {

	unsigned short memtx = ioread16(hw->memreg_tx);
	unsigned short memtx_serial;
	unsigned short memrxdone =
		ioread16(&hw->memory_info_regs->memreg_rx_done);

	try_mem_tx_old = 0;

	/* check whether the interrupt was generated by ipwireless card */
	if (!(memtx & MEMTX_TX) && !(memrxdone & MEMRX_RX_DONE)) {

		/* check if the card uses memreg_tx_old register */
		if (hw->memreg_tx == &hw->memory_info_regs->memreg_tx_new) {
			memtx = ioread16(&hw->memory_info_regs->memreg_tx_old);
			if (memtx & MEMTX_TX) {
				printk(KERN_INFO IPWIRELESS_PCCARD_NAME
					": Using memreg_tx_old\n");
				hw->memreg_tx =
					&hw->memory_info_regs->memreg_tx_old;
			} else {
				return IRQ_NONE;
			}
		} else {
			return IRQ_NONE;
		}
	}

	/*
	 * See if the card is physically present. Note that while it is
	 * powering up, it appears not to be present.
	 */
	if (!is_card_present(hw)) {
		acknowledge_pcmcia_interrupt(hw);
		return IRQ_HANDLED;
	}

	memtx_serial = memtx & (unsigned short) 0xff00;
	if (memtx & MEMTX_TX) {
		iowrite16(memtx_serial, hw->memreg_tx);

		if (hw->serial_number_detected) {
			if (memtx_serial != hw->last_memtx_serial) {
				hw->last_memtx_serial = memtx_serial;
				hw->rx_ready++;
				rx = 1;
			} else
				/* Ignore 'Timer Recovery' duplicates. */
				rx_repeat = 1;
		} else {
			/*
			 * If a non-zero serial number is seen, then enable
			 * serial number checking.
			 */
			if (memtx_serial != 0)
				hw->serial_number_detected = 1;
				printk(KERN_DEBUG IPWIRELESS_PCCARD_NAME
					": memreg_tx serial num detected\n");

			hw->rx_ready++;
			rx = 1;
		}
	}
	if (memrxdone & MEMRX_RX_DONE) {
		iowrite16(0, &hw->memory_info_regs->memreg_rx_done);
		hw->tx_ready++;
		tx = 1;
	}
	if (tx)
		iowrite16(MEMRX_PCINTACKK,
				&hw->memory_info_regs->memreg_pc_interrupt_ack);

	acknowledge_pcmcia_interrupt(hw);

	if (tx || rx)
		tasklet_schedule(&hw->tasklet);
	else if (!rx_repeat) {
		if (hw->memreg_tx == &hw->memory_info_regs->memreg_tx_new) {
			if (hw->serial_number_detected)
				printk(KERN_WARNING IPWIRELESS_PCCARD_NAME
					": spurious interrupt - new_tx mode\n");
			else {
				printk(KERN_WARNING IPWIRELESS_PCCARD_NAME
					": no valid memreg_tx value - "
					"switching to the old memreg_tx\n");
				hw->memreg_tx =
					&hw->memory_info_regs->memreg_tx_old;
				try_mem_tx_old = 1;
			}
		} else
			printk(KERN_WARNING IPWIRELESS_PCCARD_NAME
					": spurious interrupt - old_tx mode\n");
	}

	} while (try_mem_tx_old == 1);

	return IRQ_HANDLED;
}

irqreturn_t ipwireless_interrupt(int irq, void *dev_id, struct pt_regs *regs)
{
	struct ipw_hardware *hw = (struct ipw_hardware *) dev_id;

	if (hw->hw_version == HW_VERSION_1)
		return ipwireless_handle_v1_interrupt(irq, hw);
	else
		return ipwireless_handle_v2_v3_interrupt(irq, hw);
}

static void flush_packets_to_hw(struct ipw_hardware *hw)
{
	int priority_limit = get_current_packet_priority(hw);

	while (send_pending_packet(hw, priority_limit));
}

static void send_packet(struct ipw_hardware *hw, int priority,
			struct ipw_tx_packet *packet)
{
	spin_lock_irq(&hw->spinlock);
	list_add_tail(&packet->queue, &hw->tx_queue[priority]);
	hw->tx_queued = 1;
	flush_packets_to_hw(hw);
	spin_unlock_irq(&hw->spinlock);
}

static void *create_packet(int packet_size,
			   unsigned char dest_addr,
			   unsigned char protocol,
			   void (*packet_sent_callback) (struct ipw_tx_packet *
							 packet))
{
	struct ipw_tx_packet *packet = kzalloc(packet_size, GFP_ATOMIC);

	INIT_LIST_HEAD(&packet->queue);
	packet->dest_addr = dest_addr;
	packet->protocol = protocol;
	packet->packet_sent_callback = packet_sent_callback;
	packet->length = packet_size - sizeof(struct ipw_tx_packet);
	return packet;
}

void ipwireless_send_packet(struct ipw_hardware *hw, unsigned int channel_idx,
			    unsigned char *data, unsigned int length,
			    void (*callback) (void *cb, unsigned int length),
			    void *callback_data)
{
	struct ipw_tx_packet *packet;
	unsigned long flags;

	packet = create_packet(sizeof(struct ipw_tx_packet) + length,
			       (unsigned char) (channel_idx + 1),
			       TL_PROTOCOLID_COM_DATA,
			       free_packet_and_callback);
	packet->external_callback = callback;
	packet->external_callback_data = callback_data;
	memcpy((unsigned char *) packet +
			sizeof(struct ipw_tx_packet), data, length);
	/*
	 * send_packet does not save flags since it is called from other parts
	 * which do wild spinlocking accross function boundaries
	 */
	local_irq_save(flags);
	send_packet(hw, PRIO_DATA, packet);
	local_irq_restore(flags);
}

static void set_control_line(struct ipw_hardware *hw, int prio,
			   unsigned int channel_idx, int line, int state)
{
	struct ipw_control_packet *packet;
	int protocolid = TL_PROTOCOLID_COM_CTRL;

	if (prio == PRIO_SETUP)
		protocolid = TL_PROTOCOLID_SETUP;

	packet = create_packet(sizeof(struct ipw_control_packet),
			(unsigned char) (channel_idx + 1),
			protocolid, free_packet);
	packet->packet.length = sizeof(struct ipw_control_packet_body);
	packet->body.sig_no = (unsigned char) line;
	packet->body.value = (unsigned char) (state == 0 ? 0 : 1);
	send_packet(hw, prio, &packet->packet);
}


static void set_DTR(struct ipw_hardware *hw, int priority,
		   unsigned int channel_idx, int state)
{
	if (state != 0)
		hw->control_lines[channel_idx] |= IPW_CONTROL_LINE_DTR;
	else
		hw->control_lines[channel_idx] &= ~IPW_CONTROL_LINE_DTR;

	set_control_line(hw, priority, channel_idx, ComCtrl_DTR, state);
}

static void set_RTS(struct ipw_hardware *hw, int priority,
		   unsigned int channel_idx, int state)
{
	if (state != 0)
		hw->control_lines[channel_idx] |= IPW_CONTROL_LINE_RTS;
	else
		hw->control_lines[channel_idx] &= ~IPW_CONTROL_LINE_RTS;

	set_control_line(hw, priority, channel_idx, ComCtrl_RTS, state);
}

void ipwireless_set_DTR(struct ipw_hardware *hw, unsigned int channel_idx,
		       int state)
{
	set_DTR(hw, PRIO_CTRL, channel_idx, state);
}

void ipwireless_set_RTS(struct ipw_hardware *hw, unsigned int channel_idx,
		       int state)
{
	set_RTS(hw, PRIO_CTRL, channel_idx, state);
}

struct ipw_setup_get_version_query_packet {
	struct ipw_tx_packet packet;
	struct TlSetupGetVersionQry body;
};

struct ipw_setup_config_packet {
	struct ipw_tx_packet packet;
	struct TlSetupConfigMsg body;
};

struct ipw_setup_config_done_packet {
	struct ipw_tx_packet packet;
	struct TlSetupConfigDoneMsg body;
};

struct ipw_setup_open_packet {
	struct ipw_tx_packet packet;
	struct TlSetupOpenMsg body;
};

struct ipw_setup_info_packet {
	struct ipw_tx_packet packet;
	struct TlSetupInfoMsg body;
};

struct ipw_setup_reboot_msg_ack {
	struct ipw_tx_packet packet;
	struct TlSetupRebootMsgAck body;
};

/* This handles the actual initialization of the card */
static void __handle_setup_get_version_rsp(struct ipw_hardware *hw)
{
	struct ipw_setup_config_packet *config_packet;
	struct ipw_setup_config_done_packet *config_done_packet;
	struct ipw_setup_open_packet *open_packet;
	struct ipw_setup_info_packet *info_packet;
	int port;
	unsigned int channel_idx;

	/* generate config packet */
	for (port = 1; port <= NL_NUM_OF_ADDRESSES; port++) {
		config_packet = create_packet(
				sizeof(struct ipw_setup_config_packet),
				ADDR_SETUP_PROT,
				TL_PROTOCOLID_SETUP,
				free_packet);
		config_packet->packet. length = sizeof(struct TlSetupConfigMsg);
		config_packet->body.sig_no = TL_SETUP_SIGNO_CONFIG_MSG;
		config_packet->body.port_no = port;
		config_packet->body.prio_data = PRIO_DATA;
		config_packet->body.prio_ctrl = PRIO_CTRL;
		send_packet(hw, PRIO_SETUP, &config_packet->packet);
	}
	config_done_packet = create_packet(
			sizeof(struct ipw_setup_config_done_packet),
			ADDR_SETUP_PROT,
			TL_PROTOCOLID_SETUP,
			free_packet);
	config_done_packet->packet.length = sizeof(struct TlSetupConfigDoneMsg);
	config_done_packet->body.sig_no = TL_SETUP_SIGNO_CONFIG_DONE_MSG;
	send_packet(hw, PRIO_SETUP, &config_done_packet->packet);

	/* generate open packet */
	for (port = 1; port <= NL_NUM_OF_ADDRESSES; port++) {
		open_packet = create_packet(
				sizeof(struct ipw_setup_open_packet),
				ADDR_SETUP_PROT,
				TL_PROTOCOLID_SETUP,
				free_packet);
		open_packet->packet.length = sizeof(struct TlSetupOpenMsg);
		open_packet->body.sig_no = TL_SETUP_SIGNO_OPEN_MSG;
		open_packet->body.port_no = port;
		send_packet(hw, PRIO_SETUP, &open_packet->packet);
	}
	for (channel_idx = 0;
			channel_idx < NL_NUM_OF_ADDRESSES; channel_idx++) {
		set_DTR(hw, PRIO_SETUP, channel_idx,
			(hw->control_lines[channel_idx] &
			 IPW_CONTROL_LINE_DTR) != 0);
		set_RTS(hw, PRIO_SETUP, channel_idx,
			(hw->control_lines [channel_idx] &
			 IPW_CONTROL_LINE_RTS) != 0);
	}
	/*
	 * For NDIS we assume that we are using sync PPP frames, for COM async.
	 * This driver uses NDIS mode too. We don't bother with translation
	 * from async -> sync PPP.
	 */
	info_packet = create_packet(sizeof(struct ipw_setup_info_packet),
			ADDR_SETUP_PROT,
			TL_PROTOCOLID_SETUP,
			free_packet);
	info_packet->packet.length = sizeof(struct TlSetupInfoMsg);
	info_packet->body.sig_no = TL_SETUP_SIGNO_INFO_MSG;
	info_packet->body.driver_type = NDISWAN_DRIVER;
	info_packet->body.major_version = NDISWAN_DRIVER_MAJOR_VERSION;
	info_packet->body.minor_version = NDISWAN_DRIVER_MINOR_VERSION;
	send_packet(hw, PRIO_SETUP, &info_packet->packet);

	/* Initialization is now complete, so we clear the 'to_setup' flag */
	hw->to_setup = 0;
}

static void handle_setup_get_version_rsp(struct ipw_hardware *hw,
		unsigned char vers_no)
{
	del_timer(&hw->setup_timer);
	hw->initializing = 0;
	printk(KERN_INFO IPWIRELESS_PCCARD_NAME ": card is ready.\n");

	if (vers_no == TL_SETUP_VERSION)
		__handle_setup_get_version_rsp(hw);
	else
		printk(KERN_ERR
				IPWIRELESS_PCCARD_NAME
				": invalid hardware version no %u\n",
				(unsigned int) vers_no);
}

static void ipw_send_setup_packet(struct ipw_hardware *hw)
{
	struct ipw_setup_get_version_query_packet *ver_packet;

	ver_packet = create_packet(
			sizeof(struct ipw_setup_get_version_query_packet),
			ADDR_SETUP_PROT, TL_PROTOCOLID_SETUP, free_packet);
	ver_packet->body.sig_no = TL_SETUP_SIGNO_GET_VERSION_QRY;
	ver_packet->packet.length = sizeof(struct TlSetupGetVersionQry);

	/*
	 * Response is handled in handle_received_SETUP_packet
	 */
	send_packet(hw, PRIO_SETUP, &ver_packet->packet);
}

static void handle_received_SETUP_packet(struct ipw_hardware *hw,
					 unsigned int address,
					 unsigned char *data, int len,
					 int is_last)
{
	union ipw_setup_rx_msg *rx_msg = (union ipw_setup_rx_msg *) data;

	if (address != ADDR_SETUP_PROT) {
		printk(KERN_INFO IPWIRELESS_PCCARD_NAME
		       ": setup packet has bad address %d\n", address);
		return;
	}

	switch (rx_msg->sig_no) {
	case TL_SETUP_SIGNO_GET_VERSION_RSP:
		if (hw->to_setup)
			handle_setup_get_version_rsp(hw,
					rx_msg->VersRspMsg.version);
		break;

	case TL_SETUP_SIGNO_OPEN_MSG:
		if (ipwireless_cs_debug) {
			unsigned int channel_idx = rx_msg->OpenMsg.port_no - 1;

			printk(KERN_INFO IPWIRELESS_PCCARD_NAME
			       ": OPEN_MSG [channel %u] reply received\n",
			       channel_idx);
		}
		break;

	case TL_SETUP_SIGNO_INFO_MSG_ACK:
		if (ipwireless_cs_debug)
			printk(KERN_DEBUG IPWIRELESS_PCCARD_NAME
			       ": card successfully configured as NDISWAN\n");
		break;

	case TL_SETUP_SIGNO_REBOOT_MSG:
		if (hw->to_setup)
			printk(KERN_DEBUG IPWIRELESS_PCCARD_NAME
			       ": Setup not completed - ignoring reboot msg\n");
		else {
			struct ipw_setup_reboot_msg_ack *packet;

			printk(KERN_DEBUG IPWIRELESS_PCCARD_NAME
			       ": Acknowledging REBOOT message\n");
			packet = create_packet(
					sizeof(struct ipw_setup_reboot_msg_ack),
					ADDR_SETUP_PROT, TL_PROTOCOLID_SETUP,
					free_packet);
			packet->packet.length =
				sizeof(struct TlSetupRebootMsgAck);
			packet->body.sig_no = TL_SETUP_SIGNO_REBOOT_MSG_ACK;
			send_packet(hw, PRIO_SETUP, &packet->packet);
			if (hw->reboot_callback)
				hw->reboot_callback(hw->reboot_callback_data);
		}
		break;

	default:
		printk(KERN_INFO IPWIRELESS_PCCARD_NAME
		       ": unknown setup message %u received\n",
		       (unsigned int) rx_msg->sig_no);
	}
}

static void do_close_hardware(struct ipw_hardware *hw)
{
	unsigned int irqn;

	if (hw->hw_version == HW_VERSION_1) {
		/* Disable TX and RX interrupts. */
		outw(0, hw->base_port + IOIER);

		/* Acknowledge any outstanding interrupt requests */
		irqn = inw(hw->base_port + IOIR);
		if (irqn & IR_TXINTR)
			outw(IR_TXINTR, hw->base_port + IOIR);
		if (irqn & IR_RXINTR)
			outw(IR_RXINTR, hw->base_port + IOIR);
	}
}

struct ipw_hardware *ipwireless_hardware_create(struct ipw_config *config)
{
	int i;
	struct ipw_hardware *hw =
		kzalloc(sizeof(struct ipw_hardware), GFP_KERNEL);

	if (!hw)
		return NULL;

	hw->initializing = 1;
	hw->tx_ready = 1;
	memcpy(&hw->config, config, sizeof(struct ipw_config));
	for (i = 0; i < NL_NUM_OF_PRIORITIES; i++)
		INIT_LIST_HEAD(&hw->tx_queue[i]);

	hw->rx_bytes_queued = 0;
	INIT_LIST_HEAD(&hw->rx_queue);
	INIT_LIST_HEAD(&hw->rx_pool);

	spin_lock_init(&hw->spinlock);
	tasklet_init(&hw->tasklet, ipwireless_do_tasklet, (unsigned long) hw);

	init_timer(&hw->setup_timer);
	hw->setup_timer.function = ipwireless_setup_timer;
	hw->setup_timer.data = (unsigned long) hw;

	INIT_WORK(&hw->work_rx, ipw_receive_data_work);

	hw->last_memtx_serial = (unsigned short) 0xffff;

	return hw;
}

void ipwireless_init_hardware_v1(struct ipw_hardware *hw,
		unsigned int base_port,
		void __iomem *attr_memory,
		void __iomem *common_memory,
		int is_v2_card,
		void (*reboot_callback) (void *data),
		void *reboot_callback_data)
{
	hw->bad_interrupt_count = 0;
	if (hw->removed) {
		hw->removed = 0;
		enable_irq(hw->irq);
	}
	hw->base_port = base_port;
	hw->hw_version = is_v2_card ? HW_VERSION_2 : HW_VERSION_1;
	hw->ll_mtu = hw->hw_version == HW_VERSION_1 ? LL_MTU_V1 : LL_MTU_V2;
	hw->memregs_CCR = (struct MEMCCR __iomem *)
			((unsigned short __iomem *) attr_memory + 0x200);
	hw->memory_info_regs = (struct MEMINFREG __iomem *) common_memory;
	hw->memreg_tx = &hw->memory_info_regs->memreg_tx_new;
	hw->reboot_callback = reboot_callback;
	hw->reboot_callback_data = reboot_callback_data;
}

void ipwireless_init_hardware_v2_v3(struct ipw_hardware *hw)
{
	hw->initializing = 1;
	hw->init_loops = 0;
	printk(KERN_INFO IPWIRELESS_PCCARD_NAME
	       ": waiting for card to start up...\n");
	ipwireless_setup_timer((unsigned long) hw);
}

static void ipwireless_setup_timer(unsigned long data)
{
	struct ipw_hardware *hw = (struct ipw_hardware *) data;

	hw->init_loops++;

	if (hw->init_loops == TL_SETUP_MAX_VERSION_QRY &&
			hw->hw_version == HW_VERSION_2 &&
			hw->memreg_tx == &hw->memory_info_regs->memreg_tx_new) {
		printk(KERN_INFO IPWIRELESS_PCCARD_NAME
				": failed to startup using TX2, trying TX\n");

		hw->memreg_tx = &hw->memory_info_regs->memreg_tx_old;
		hw->init_loops = 0;
	}
	/* Give up after a certain number of retries */
	if (hw->init_loops == TL_SETUP_MAX_VERSION_QRY) {
		printk(KERN_INFO IPWIRELESS_PCCARD_NAME
		       ": card failed to start up!\n");
		hw->initializing = 0;
	} else {
		/* Do not attempt to write to the board if it is not present. */
		if (is_card_present(hw)) {
			unsigned long flags;

			hw->to_setup = 1;
			hw->tx_ready = 1;
			spin_lock_irqsave(&hw->spinlock, flags);
			tasklet_schedule(&hw->tasklet);
			spin_unlock_irqrestore(&hw->spinlock, flags);
		}

		hw->setup_timer.expires = jiffies + (unsigned long)
			TL_SETUP_VERSION_QRY_TMO * HZ / 1000UL;
		add_timer(&hw->setup_timer);
	}
}

/*
 * Stop any interrupts from executing so that, once this function returns,
 * other layers of the driver can be sure they won't get any more callbacks.
 * Thus must be called on a proper process context.
 */
void ipwireless_stop_interrupts(struct ipw_hardware *hw)
{
	if (!hw->shutting_down) {
		/* Tell everyone we are going down. */
		hw->shutting_down = 1;
		del_timer(&hw->setup_timer);

		/* Prevent the hardware from sending any more interrupts */
		do_close_hardware(hw);
	}
}

void ipwireless_hardware_free(struct ipw_hardware *hw)
{
	int i;
	struct ipw_rx_packet *rp, *rq;
	struct ipw_tx_packet *tp, *tq;

	ipwireless_stop_interrupts(hw);

	flush_scheduled_work();

	for (i = 0; i < NL_NUM_OF_ADDRESSES; i++)
		if (hw->packet_assembler[i] != NULL)
			kfree(hw->packet_assembler[i]);

	for (i = 0; i < NL_NUM_OF_PRIORITIES; i++)
		list_for_each_entry_safe(tp, tq, &hw->tx_queue[i], queue) {
			list_del_init(&tp->queue);
			kfree(tp);
		}

	list_for_each_entry_safe(rp, rq, &hw->rx_queue, queue) {
		list_del_init(&rp->queue);
		kfree(rp);
	}

	list_for_each_entry_safe(rp, rq, &hw->rx_pool, queue) {
		list_del_init(&rp->queue);
		kfree(rp);
	}
	kfree(hw);
}

/*
 * Associate the specified network with this hardware, so it will receive events
 * from it.
 */
void ipwireless_associate_network(struct ipw_hardware *hw,
				  struct ipw_network *network)
{
	hw->network = network;
}
