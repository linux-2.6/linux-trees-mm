/*
 * Driver for the PMC MSP71xx reference board GPIO pins
 *
 * Copyright 2005-2007 PMC-Sierra, Inc.
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 *
 *  THIS  SOFTWARE  IS PROVIDED   ``AS  IS'' AND   ANY  EXPRESS OR IMPLIED
 *  WARRANTIES,   INCLUDING, BUT NOT  LIMITED  TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
 *  NO  EVENT  SHALL   THE AUTHOR  BE    LIABLE FOR ANY   DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *  NOT LIMITED   TO, PROCUREMENT OF  SUBSTITUTE GOODS  OR SERVICES; LOSS OF
 *  USE, DATA,  OR PROFITS; OR  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  You should have received a copy of the  GNU General Public License along
 *  with this program; if not, write  to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <linux/module.h>
#include <linux/ioport.h>
#include <linux/spinlock.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/kthread.h>
#include <linux/sched.h>
#include <linux/completion.h>
#include <linux/freezer.h>
#include <linux/io.h>

#include <asm/war.h>

#include <msp_regs.h>
#include <msp_regops.h>
#include <msp_gpio.h>

/* -- Private definitions -- */

/* Special bitflags and whatnot for the driver's convenience */
#define GPIO_REG_COUNT		4
#define GPIO_CFG_SHIFT(i)	(i * 4)
#define GPIO_CFG_PINMASK	0xf

static const u32 gpio_data_count[] = { 2, 4, 4, 6 };
static const u32 gpio_data_shift[] = { 0, 2, 6, 10 };
static const u32 gpio_data_mask[] = { 0x03, 0x0f, 0x0f, 0x3f };
static u32 *const gpio_data_reg[] = {
	(u32 *)GPIO_DATA1_REG, (u32 *)GPIO_DATA2_REG,
	(u32 *)GPIO_DATA3_REG, (u32 *)GPIO_DATA4_REG,
};
static const u32 gpio_cfg_mask[] = { 0x0000ff, 0x00ffff, 0x00ffff, 0xffffff };
static u32 *const gpio_cfg_reg[] = {
	(u32 *)GPIO_CFG1_REG, (u32 *)GPIO_CFG2_REG,
	(u32 *)GPIO_CFG3_REG, (u32 *)GPIO_CFG4_REG,
};

/* Maps MODE to allowed pin mask */
static const u32 gpio_mode_allowed[] = {
	0xfffff,	/* Mode 0 - INPUT */
	0x00000,	/* Mode 1 - INTERRUPT */
	0x00030,	/* Mode 2 - UART_INPUT (GPIO 4, 5)*/
	0, 0, 0, 0, 0,	/* Modes 3, 4, 5, 6, and 7 are reserved */
	0xfffff,	/* Mode 8 - OUTPUT */
	0x0000f,	/* Mode 9 - UART_OUTPUT/PERF_TIMERA (GPIO 0,1,2,3) */
	0x00003,	/* Mode a - PERF_TIMERB (GPIO 0, 1) */
	0x00000,	/* Mode b - Not really a mode! */
};

/* The extended gpio register */

#define EXTENDED_GPIO_COUNT	4
#define EXTENDED_GPIO_SHIFT	16
#define EXTENDED_GPIO_MASK	0x0f

#define EXTENDED_GPIO_DATA_SHIFT(i)	(i * 2)
#define EXTENDED_GPIO_DATA_MASK(i)	(0x3 << (i*2))
#define EXTENDED_GPIO_DATA_SET		0x2
#define EXTENDED_GPIO_DATA_CLR		0x1
#define EXTENDED_GPIO_CFG_SHIFT(i)	((i * 2) + 16)
#define EXTENDED_GPIO_CFG_MASK(i)	(0x3 << ((i*2)+16))
#define EXTENDED_GPIO_CFG_DISABLE	0x2
#define EXTENDED_GPIO_CFG_ENABLE	0x1

/* -- Data structures -- */

static DEFINE_SPINLOCK(msp_gpio_spinlock);

static struct task_struct *msp_blinkthread;
static DEFINE_SPINLOCK(msp_blink_lock);
static DECLARE_COMPLETION(msp_blink_wait);

struct blink_table {
	u32 count;
	u32 period;
	u32 dcycle;
};

static struct blink_table blink_table[MSP_NUM_GPIOS];

/* -- Utility functions -- */

/* Reads the data bits from a single register set */
static u32 msp_gpio_read_data_basic(int reg)
{
	return read_reg32(gpio_data_reg[reg], gpio_data_mask[reg]);
}

/* Reads the data bits from the extended register set */
static u32 msp_gpio_read_data_extended(void)
{
	int pin;
	u32 reg = *EXTENDED_GPIO_REG;
	u32 retval = 0;

	for (pin = 0; pin < EXTENDED_GPIO_COUNT; pin++) {
		u32 bit = 0;

		/*
		 * In output mode, read CLR bit
		 * In input mode, read SET bit
		 */
		if (reg & (EXTENDED_GPIO_CFG_ENABLE <<
				EXTENDED_GPIO_CFG_SHIFT(pin)))
			bit = EXTENDED_GPIO_DATA_CLR <<
				EXTENDED_GPIO_DATA_SHIFT(pin);
		else
			bit = EXTENDED_GPIO_DATA_SET <<
				EXTENDED_GPIO_DATA_SHIFT(pin);

		if (reg & bit)
			retval |= 1 << pin;
	}

	return retval;
}

/*
 * Reads the current state of all 20 pins, putting the values in
 * the lowest 20 bits (1=HI, 0=LO)
 */
static u32 msp_gpio_read_data(void)
{
	int reg;
	u32 retval = 0;

	spin_lock(&msp_gpio_spinlock);
	for (reg = 0; reg < GPIO_REG_COUNT; reg++)
		retval |= msp_gpio_read_data_basic(reg) <<
				gpio_data_shift[reg];
	retval |= msp_gpio_read_data_extended() << EXTENDED_GPIO_SHIFT;
	spin_unlock(&msp_gpio_spinlock);

	pr_debug("%s: 0x%08x\n", __FUNCTION__, retval);

	return retval;
}

/*
 * This assumes both data and mask are register-ready, and does
 * the set atomically
 */
static void msp_gpio_write_data_basic(int reg, u32 data, u32 mask)
{
	set_value_reg32(gpio_data_reg[reg], mask, data);
}

/*
 * The four lowest bits of 'data' and 'mask' are used, and the set
 * is done atomically
 */
static void msp_gpio_write_data_extended(u32 data, u32 mask)
{
	int i;
	u32 tmpmask = 0xffffffff, tmpdata = 0;

	/* Set all SET/CLR values based on data bits passed in */
	for (i = 0; i < EXTENDED_GPIO_COUNT; i++) {
		if (mask & (1 << i)) {
			if (data & (1 << i))
				/* Set the bit HI */
				tmpdata |= EXTENDED_GPIO_DATA_SET <<
					   EXTENDED_GPIO_DATA_SHIFT(i);
			else
				/* Set the bit LO */
				tmpdata |= EXTENDED_GPIO_DATA_CLR <<
					   EXTENDED_GPIO_DATA_SHIFT(i);
		}
	}

	set_value_reg32(EXTENDED_GPIO_REG, tmpmask, tmpdata);
}

/*
 * Sets all masked GPIOs based on the first 20 bits of the data
 * passed in (1=HI, 0=LO)
 */
static void msp_gpio_write_data(u32 data, u32 mask)
{
	int reg;

	spin_lock(&msp_gpio_spinlock);
	for (reg = 0; reg < GPIO_REG_COUNT; reg++) {
		u32 tmpdata = (data >> gpio_data_shift[reg]) &
					gpio_data_mask[reg];
		u32 tmpmask = (mask >> gpio_data_shift[reg]) &
					gpio_data_mask[reg];
		if (tmpmask > 0)
			msp_gpio_write_data_basic(reg, tmpdata, tmpmask);
	}
	msp_gpio_write_data_extended(data >> EXTENDED_GPIO_SHIFT,
					mask >> EXTENDED_GPIO_SHIFT);
	spin_unlock(&msp_gpio_spinlock);
}

/* Reads the config bits from a single register set */
static u32 msp_gpio_read_cfg_basic(int reg)
{
	return read_reg32(gpio_cfg_reg[reg], gpio_cfg_mask[reg]);
}

/*
 * This assumes both data and mask are register-ready, and does
 * the write atomically
 */
static void msp_gpio_write_cfg_basic(int reg, u32 data, u32 mask)
{
	set_value_reg32(gpio_cfg_reg[reg], mask, data);
}

/*
 * Reads the configuration of the extended pins, returning the current
 * configuration in the lowest 4 bits (1-output, 0-input)
 */
static u32 msp_gpio_read_cfg_extended(void)
{
	int i;
	u32 reg = *EXTENDED_GPIO_REG;
	u32 retval = 0;

	/* Read all ENABLE/DISABLE values and translate to single bits */
	for (i = 0; i < EXTENDED_GPIO_COUNT; i++) {
		if (reg & (EXTENDED_GPIO_CFG_ENABLE <<
				EXTENDED_GPIO_CFG_SHIFT(i)))
			/* Pin is OUTPUT */
			retval |= 1 << i;
		else
			/* Pin is INPUT */
			retval &= ~(1 << i);
	}

	return retval;
}

/*
 * Sets the masked extended pins to (1-output, 0-input)
 * (uses 4 lowest bits of the mask)
 * Does the write atomically
 */
static void msp_gpio_write_cfg_extended(u32 data, u32 mask)
{
	int i;
	u32 tmpmask = 0xffffffff, tmpdata = 0;

	/* Set all ENABLE/DISABLE values based on mask bits passed in */
	for (i = 0; i < EXTENDED_GPIO_COUNT; i++) {
		if (mask & (1 << i)) {
			if (data & (1 << i))
				/* Set the pin to OUTPUT */
				tmpdata |= EXTENDED_GPIO_CFG_ENABLE <<
					   EXTENDED_GPIO_CFG_SHIFT(i);
			else
				/* Set the pin to INPUT */
				tmpdata |= EXTENDED_GPIO_CFG_DISABLE <<
					   EXTENDED_GPIO_CFG_SHIFT(i);
		}
	}

	set_value_reg32(EXTENDED_GPIO_REG, tmpmask, tmpdata);
}

/*
 * Sets all GPIOs to input/output based on the first 20 bits of the mask
 * (1-output, 0-input)
 */
static void msp_gpio_write_cfg(enum msp_gpio_mode mode, u32 mask)
{
	int reg;
	u32 extdata = 0, extmask = 0;

	spin_lock(&msp_gpio_spinlock);
	for (reg = 0; reg < GPIO_REG_COUNT; reg++) {
		int pin;
		u32 tmpdata = 0, tmpmask = 0;
		for (pin = 0; pin < gpio_data_count[reg]; pin++) {
			if (mask & (1 << (gpio_data_shift[reg] + pin))) {
				tmpmask |= GPIO_CFG_PINMASK <<
					   GPIO_CFG_SHIFT(pin);
				tmpdata |= (u32)mode <<
					   GPIO_CFG_SHIFT(pin);
			}
		}
		if (tmpmask)
			msp_gpio_write_cfg_basic(reg, tmpdata, tmpmask);
	}

	extmask = mask >> EXTENDED_GPIO_SHIFT;
	if (mode == MSP_GPIO_INPUT)
		extdata = 0;
	else if (mode == MSP_GPIO_OUTPUT)
		extdata = 0xf;
	else
		extmask = 0;
	if (extmask)
		msp_gpio_write_cfg_extended(extdata, extmask);
	spin_unlock(&msp_gpio_spinlock);
}

/*
 * Reads all GPIO config values and checks if they match the pin mode given,
 * placing the result in the lowest 20 bits of the result, one bit per pin
 * (1-pin matches mode give, 0-pin does not match)
 */
static u32 msp_gpio_read_cfg(u32 mode)
{
	u32 retval = 0;
	int reg;

	spin_lock(&msp_gpio_spinlock);
	for (reg = 0; reg < GPIO_REG_COUNT; reg++) {
		int pin;
		u32 tmpdata = msp_gpio_read_cfg_basic(reg);
		for (pin = 0; pin < gpio_data_count[reg]; pin++) {
			u32 val = (tmpdata >> GPIO_CFG_SHIFT(pin)) &
					GPIO_CFG_PINMASK;
			if (val == mode)
				retval |= 1 << (gpio_data_shift[reg] + pin);
		}
	}

	/* Extended pins only have INPUT or OUTPUT pins */
	if (mode == MSP_GPIO_INPUT)
		retval |= (~msp_gpio_read_cfg_extended() & EXTENDED_GPIO_MASK)
			  << EXTENDED_GPIO_SHIFT;
	else if (mode == MSP_GPIO_OUTPUT)
		retval |= (msp_gpio_read_cfg_extended() & EXTENDED_GPIO_MASK)
			  << EXTENDED_GPIO_SHIFT;
	spin_unlock(&msp_gpio_spinlock);

	pr_debug("%s(0x%02x): 0x%08x\n", __FUNCTION__, mode, retval);

	return retval;
}

/* -- Public functions -- */

/*
 * Reads the bits specified by the mask and puts the values in data.
 * May include output statuses also, if in mask.
 *
 * Returns 0 on success
 */
int msp_gpio_in(u32 *data, u32 mask)
{
	*data = msp_gpio_read_data() & mask;

	return 0;
}
EXPORT_SYMBOL(msp_gpio_in);

/*
 * Sets the specified data on the masked pins
 *
 * Returns 0 on success or one of the following:
 *  -EINVAL if any of the pins in the mask are not free or not already
 *  in output mode
 */
int msp_gpio_out(u32 data, u32 mask)
{
	if ((mask & ~MSP_GPIO_FREE_MASK) != 0) {
		printk(KERN_WARNING
			"Invalid GPIO mask - References non-free pins\n");
		return -EINVAL;
	}
	if ((mask & ~msp_gpio_read_cfg(MSP_GPIO_OUTPUT)) != 0) {
		printk(KERN_WARNING
			"Invalid GPIO mask - Cannot set non-output pins\n");
		return -EINVAL;
	}

	msp_gpio_noblink(mask);
	msp_gpio_write_data(data, mask);

	return 0;
}
EXPORT_SYMBOL(msp_gpio_out);

/*
 * Sets masked pins to the specified msp_gpio_mode
 *
 * Returns 0 on success or one of the following:
 *  -EINVAL if any of the pins in the mask are not free or if any pins
 *  are not allowed to be set to the specified mode
 */
int msp_gpio_mode(enum msp_gpio_mode mode, u32 mask)
{
	u32 allowedmask;

	if ((mask & ~MSP_GPIO_FREE_MASK) != 0) {
		printk(KERN_WARNING
			"Invalid GPIO mask - References non-free pins\n");
		return -EINVAL;
	}

	/* Enforce pin-mode rules */
	allowedmask = gpio_mode_allowed[mode];
	if ((mask & ~allowedmask) != 0) {
		printk(KERN_WARNING
			"Invalid GPIO mode for masked pins\n");
		return -EINVAL;
	}

	msp_gpio_noblink(mask);
	msp_gpio_write_cfg(mode, mask);

	return 0;
}
EXPORT_SYMBOL(msp_gpio_mode);

/*
 * Stops the specified GPIOs from blinking
 */
int msp_gpio_noblink(u32 mask)
{
	int i;

	if ((mask & ~msp_gpio_read_cfg(MSP_GPIO_OUTPUT)) != 0)
		return -EINVAL;

	spin_lock(&msp_blink_lock);
	for (i = 0; i < MSP_NUM_GPIOS; i++) {
		if (mask & (1 << i)) {
			blink_table[i].count = 0;
			blink_table[i].period = 0;
			blink_table[i].dcycle = 0;
		}
	}
	spin_unlock(&msp_blink_lock);

	msp_gpio_write_data(0, mask);

	return 0;
}
EXPORT_SYMBOL(msp_gpio_noblink);

/*
 * Configures GPIO(s) to blink
 *  - mask shows which GPIOs to blink
 *  - period is the time in 100ths of a second for the total period
 *    (0 disables blinking)
 *  - dcycle is the percentage of the period where the GPIO is HI
 */
int msp_gpio_blink(u32 mask, u32 period, u32 dcycle)
{
	int i;

	if ((mask & ~msp_gpio_read_cfg(MSP_GPIO_OUTPUT)) != 0) {
		printk(KERN_WARNING
			"Invalid GPIO mask - Cannot blink non-output pins\n");
		return -EINVAL;
	}

	if (period == 0)
		return msp_gpio_noblink(mask);

	spin_lock(&msp_blink_lock);
	for (i = 0; i < MSP_NUM_GPIOS; i++) {
		if (mask & (1 << i)) {
			blink_table[i].count = 0;
			blink_table[i].period = period;
			blink_table[i].dcycle = dcycle;
		}
	}
	spin_unlock(&msp_blink_lock);

	complete(&msp_blink_wait);

	return 0;
}
EXPORT_SYMBOL(msp_gpio_blink);

/* -- File functions -- */

static int msp_gpio_open(struct inode *inode, struct file *file)
{
	return 0;
}

static int msp_gpio_release(struct inode *inode, struct file *file)
{
	return 0;
}

static int msp_gpio_ioctl(struct inode *inode, struct file *file,
			  unsigned int cmd, unsigned long arg)
{
	static struct msp_gpio_ioctl_io_data data;
	static struct msp_gpio_ioctl_blink_data blink;

	switch (cmd) {
	case MSP_GPIO_BLINK:
		if (copy_from_user(&blink,
		    (struct msp_gpio_ioctl_blink_data *)arg, sizeof(blink)))
			return -EFAULT;
		break;
	default:
		if (copy_from_user(&data,
		    (struct msp_gpio_ioctl_io_data *)arg, sizeof(data)))
			return -EFAULT;
		break;
	}

	switch (cmd) {
	case MSP_GPIO_IN:
		if (msp_gpio_in(&data.data, data.mask))
			return -EFAULT;
		if (copy_to_user((struct msp_gpio_ioctl_io_data *)arg,
		    &data, sizeof(data)))
			return -EFAULT;
		break;
	case MSP_GPIO_OUT:
		if (msp_gpio_out(data.data, data.mask))
			return -EFAULT;
		break;
	case MSP_GPIO_MODE:
		if (msp_gpio_mode(data.data, data.mask))
			return -EFAULT;
		break;
	case MSP_GPIO_BLINK:
		if (msp_gpio_blink(blink.mask, blink.period, blink.dcycle))
			return -EFAULT;
		break;
	default:
		return -ENOIOCTLCMD;
	}

	return 0;
}

static struct file_operations msp_gpio_fops = {
	.owner		= THIS_MODULE,
	.ioctl		= msp_gpio_ioctl,
	.open		= msp_gpio_open,
	.release	= msp_gpio_release,
};

static struct miscdevice msp_gpio_miscdev = {
	MISC_DYNAMIC_MINOR,
	"pmcmsp_gpio",
	&msp_gpio_fops
};

static int msp_gpio_blinkthread(void *none)
{
	int firstrun = 1;

	do {
		u32 mask = 0, data = 0;
		int i, blinking = 0;
		spin_lock(&msp_blink_lock);
		for (i = 0; i < MSP_NUM_GPIOS; i++) {
			/* use blink_table[i].period as 'blink enabled' test */
			if (blink_table[i].period) {
				blinking = 1;
				mask |= 1 << i;
				blink_table[i].count++;

				if (blink_table[i].count >=
				    blink_table[i].period)
					blink_table[i].count = 0;

				if (blink_table[i].count <
				    (blink_table[i].period *
				    blink_table[i].dcycle / 100))
					data |= 1 << i;
			}
		}
		spin_unlock(&msp_blink_lock);

		if (!firstrun || blinking)
			msp_gpio_write_data(data, mask);
		else
			firstrun = 0;

		if (blinking)
			schedule_timeout_interruptible(HZ/100);
		else
			wait_for_completion_interruptible(&msp_blink_wait);

		/* make swsusp happy with our thread */
		try_to_freeze();
	} while (!kthread_should_stop());

	return 0;
}

/* -- Module functions -- */

static int __init msp_gpio_init(void)
{
	if (misc_register(&msp_gpio_miscdev)) {
		printk(KERN_ERR "Device registration failed\n");
		goto err_miscdev;
	}

	msp_blinkthread = kthread_run(msp_gpio_blinkthread, NULL, "gpio_blink");
	if (msp_blinkthread == NULL) {
		printk(KERN_ERR "Could not start kthread\n");
		goto err_blinkthread;
	}

	printk(KERN_WARNING "MSP7120 GPIO subsystem initialized\n");
	return 0;

err_blinkthread:
	misc_deregister(&msp_gpio_miscdev);
err_miscdev:
	return -ENOMEM;
}

static void __exit msp_gpio_exit(void)
{
	complete(&msp_blink_wait);
	kthread_stop(msp_blinkthread);

	misc_deregister(&msp_gpio_miscdev);
}

MODULE_DESCRIPTION("PMC MSP GPIO driver");
MODULE_LICENSE("GPL");

module_init(msp_gpio_init);
module_exit(msp_gpio_exit);
