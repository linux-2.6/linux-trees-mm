/*
 *	watchdog.c
 *
 *	(c) Copyright 2007 Wim Van Sebroeck <wim@iguana.be>.
 *
 *	This code is generic code that can be shared by all the
 *	watchdog drivers.
 *
 *	Based on source code of the following authors:
 *	  Alan Cox <alan@redhat.com>,
 *	  Matt Domsch <Matt_Domsch@dell.com>,
 *	  Rob Radez <rob@osinvestor.com>,
 *	  Rusty Lynch <rusty@linux.co.intel.com>
 *	  Satyam Sharma <satyam@infradead.org>
 *	  Randy Dunlap <randy.dunlap@oracle.com>
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version
 *	2 of the License, or (at your option) any later version.
 *
 *	Neither Wim Van Sebroeck nor Iguana vzw. admit liability nor
 *	provide warranty for any of this software. This material is
 *	provided "AS-IS" and at no charge.
 */

#include <linux/module.h>	/* For module related things/EXPORT_SYMBOL/... */
#include <linux/types.h>	/* For standard types */
#include <linux/errno.h>	/* For the -ENODEV/... values */
#include <linux/kernel.h>	/* For printk/panic/... */
#include <linux/mm.h>		/* For memory allocations ... */
#include <linux/watchdog.h>	/* For watchdog specific items */
#include <linux/init.h>		/* For __init/__exit/... */

/*
 *	Version information
 */
#define DRV_VERSION	"0.01"
#define DRV_NAME	"watchdog_core"
#define PFX DRV_NAME	": "

/*
 *	External functions/procedures
 */
extern int watchdog_dev_register(struct watchdog_device *, struct device *);
extern int watchdog_dev_unregister(struct watchdog_device *);

/**
 *	alloc_watchdogdev - allocates a watchdog device
 *
 *	Creates a new watchdog device structure.
 *	Returns the new structure, or NULL if an error occured.
 */
struct watchdog_device *alloc_watchdogdev(void)
{
	int alloc_size = sizeof(struct watchdog_device);
	void *p;
	struct watchdog_device *dev;

	/* allocate memory for our device and initialize it */
	p = kzalloc(alloc_size, GFP_KERNEL);
	if (!p) {
		printk(KERN_ERR PFX "Unable to allocate watchdog device\n");
		return NULL;
	}
	dev = (struct watchdog_device *) p;

	return dev;
}
EXPORT_SYMBOL(alloc_watchdogdev);

/**
 *	free_watchdogdev - free watchdog device
 *	@dev: watchdog device
 *
 *	This function does the last stage of destroying an allocated
 *	watchdog device.
 */
int free_watchdogdev(struct watchdog_device *dev)
{
	if (!((dev->watchdog_state == WATCHDOG_UNINITIALIZED) ||
	      (dev->watchdog_state == WATCHDOG_UNREGISTERED))) {
		printk(KERN_ERR PFX "Unable to destroy a watchdog device that is still in use\n");
		return -1;
	}

	kfree(dev);
	return 0;
}
EXPORT_SYMBOL(free_watchdogdev);

/**
 *	register_watchdogdevice - register a watchdog device
 *	@dev: watchdog device
 *	@parent: parent device for the watchdog class device
 *
 *	This function registers a watchdog device in the kernel so
 *	that it can be accessed from userspace.
 */
int register_watchdogdevice(struct watchdog_device *dev, struct device *parent)
{
	int ret;

	if (dev == NULL ||
	    dev->watchdog_ops == NULL)
		return -ENODATA;

	if (dev->watchdog_ops->start == NULL ||
	    dev->watchdog_ops->stop == NULL ||
	    dev->watchdog_ops->keepalive == NULL)
		return -ENODATA;

	if (!((dev->watchdog_state == WATCHDOG_UNINITIALIZED) ||
	      (dev->watchdog_state == WATCHDOG_UNREGISTERED))) {
		printk(KERN_ERR PFX "Unable to register a watchdog device that is allready in use\n");
		return -1;
	}

	dev->options |= WDIOF_MAGICCLOSE;
	if (dev->watchdog_ops->set_heartbeat) {
		dev->options |= WDIOF_SETTIMEOUT;
	} else {
		dev->options &= ~WDIOF_SETTIMEOUT;
	}

	ret = watchdog_dev_register(dev, parent);
	if (ret) {
		printk(KERN_ERR PFX "error registering /dev/watchdog (err=%d)",
			ret);
		return ret;
	}

	dev->watchdog_state = WATCHDOG_REGISTERED;
	return 0;
}
EXPORT_SYMBOL(register_watchdogdevice);

/**
 *	unregister_watchdogdevice - unregister a watchdog device
 *	@dev: watchdog device
 *
 *	This function unregisters a watchdog device from the kernel.
 */
int unregister_watchdogdevice(struct watchdog_device *dev)
{
	int ret;

	if (dev == NULL)
		return -ENODATA;

	if ((dev->watchdog_state == WATCHDOG_UNINITIALIZED) ||
	    (dev->watchdog_state == WATCHDOG_UNREGISTERED)) {
		printk(KERN_ERR PFX "Unable to unregister a watchdog device that has not been registered\n");
		return -ENODEV;
	}

	ret = watchdog_dev_unregister(dev);
	if (ret) {
		printk(KERN_ERR PFX "error unregistering /dev/watchdog (err=%d)",
			ret);
		return ret;
	}

	dev->watchdog_state = WATCHDOG_UNREGISTERED;
	return 0;
}
EXPORT_SYMBOL(unregister_watchdogdevice);

static int __init watchdog_init(void)
{
	printk(KERN_INFO "Uniform watchdog device driver v%s loaded\n",
		DRV_VERSION);
	return 0;
}

static void __exit watchdog_exit(void)
{
	printk(KERN_INFO "Uniform watchdog device driver unloaded\n");
}

module_init(watchdog_init);
module_exit(watchdog_exit);

MODULE_AUTHOR("Wim Van Sebroeck <wim@iguana.be>");
MODULE_DESCRIPTION("Uniform Watchdog Device Driver");
MODULE_VERSION(DRV_VERSION);
MODULE_LICENSE("GPL");
MODULE_SUPPORTED_DEVICE("watchdog");

