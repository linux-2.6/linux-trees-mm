/*
 *	watchdog_dev.c
 *
 *	(c) Copyright 2007 Wim Van Sebroeck <wim@iguana.be>.
 *
 *	This source code is part of the generic code that can be used
 *	by all the watchdog drivers.
 *
 *	This part of the generic code takes care of the following
 *	misc device: /dev/watchdog.
 *
 *	Based on source code of the following authors:
 *	  Alan Cox <alan@redhat.com>,
 *	  Matt Domsch <Matt_Domsch@dell.com>,
 *	  Rob Radez <rob@osinvestor.com>,
 *	  Rusty Lynch <rusty@linux.co.intel.com>
 *	  Satyam Sharma <satyam@infradead.org>
 *	  Randy Dunlap <randy.dunlap@oracle.com>
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version
 *	2 of the License, or (at your option) any later version.
 *
 *	Neither Wim Van Sebroeck nor Iguana vzw. admit liability nor
 *	provide warranty for any of this software. This material is
 *	provided "AS-IS" and at no charge.
 */

#include <linux/module.h>	/* For module related things/EXPORT_SYMBOL/... */
#include <linux/types.h>	/* For standard types (like size_t) */
#include <linux/errno.h>	/* For the -ENODEV/... values */
#include <linux/kernel.h>	/* For printk/panic/... */
#include <linux/fs.h>		/* For file operations */
#include <linux/watchdog.h>	/* For watchdog specific items */
#include <linux/miscdevice.h>	/* For handling misc devices */
#include <linux/mutex.h>	/* For mutex locking */
#include <linux/init.h>		/* For __init/__exit/... */
#include <linux/uaccess.h>	/* For copy_to_user/put_user/... */

#ifdef CONFIG_WATCHDOG_DEBUG_CORE
#define trace(format, args...) \
	printk(KERN_INFO "%s(" format ")\n", __FUNCTION__ , ## args)
#define dbg(format, arg...) \
	printk(KERN_DEBUG "%s: " format "\n", __FUNCTION__, ## arg)
#else
#define trace(format, arg...) do { } while (0)
#define dbg(format, arg...) do { } while (0)
#endif

/*
 *	Version information
 */
#define DRV_VERSION	"0.01"
#define DRV_NAME	"watchdog_dev"
#define PFX DRV_NAME	": "

/*
 *	Locally used variables
 */

static struct watchdog_device *watchdogdev;	/* the watchdog device behind /dev/watchdog */
static unsigned long watchdog_dev_open;		/* wether or not /dev/watchdog has been opened */
static char received_magic_char;		/* wether or not we received the magic char */
static DEFINE_MUTEX(watchdog_register_mtx);	/* prevent races between register & unregister */

/*
 *	/dev/watchdog operations
 */

/*
 *	watchdog_write: writes to the watchdog.
 *	@file: file from VFS
 *	@data: user address of data
 *	@len: length of data
 *	@ppos: pointer to the file offset
 *
 *	A write to a watchdog device is defined as a keepalive signal.
 *	Writing the magic 'V' sequence allows the next close to turn
 *	off the watchdog (if 'nowayout' is not set).
 */

static ssize_t watchdog_write(struct file *file, const char __user *data,
				size_t len, loff_t *ppos)
{
	trace("%p, %p, %zu, %p", file, data, len, ppos);

	if (!watchdogdev ||
	    !watchdogdev->watchdog_ops ||
	    !watchdogdev->watchdog_ops->keepalive)
		return -ENODEV;

	/* See if we got the magic character 'V' and reload the timer */
	if (len) {
		if (!watchdogdev->nowayout) {
			size_t i;

			/* note: just in case someone wrote the magic character
			 * five months ago... */
			received_magic_char = 0;

			/* scan to see wether or not we got the magic character */
			for (i = 0; i != len; i++) {
				char c;
				if (get_user(c, data + i))
					return -EFAULT;
				if (c == 'V') {
					received_magic_char = 42;
					dbg("received the magic character\n");
				}
			}
		}

		/* someone wrote to us, so we sent the watchdog a keepalive signal if
		 * the watchdog is active */
		if (watchdogdev->watchdog_state == WATCHDOG_STARTED)
			watchdogdev->watchdog_ops->keepalive(watchdogdev);
	}
	return len;
}

/*
 *	watchdog_ioctl: handle the different ioctl's for the watchdog device.
 *	@inode: inode of the device
 *	@file: file handle to the device
 *	@cmd: watchdog command
 *	@arg: argument pointer
 *
 *	The watchdog API defines a common set of functions for all watchdogs
 *	according to their available features.
 */

static int watchdog_ioctl(struct inode *inode, struct file *file,
				unsigned int cmd, unsigned long arg)
{
	int status;
	int err;
	int new_options;
	int new_heartbeat;
	int time_left;
	void __user *argp = (void __user *)arg;
	int __user *p = argp;
	static struct watchdog_info ident = {
		.options =		0,
		.firmware_version =	0,
		.identity =		"Watchdog Device",
	};

	trace("%p, %p, %u, %li", inode, file, cmd, arg);

	if (!watchdogdev || !watchdogdev->watchdog_ops)
		return -ENODEV;

	switch (cmd) {
	case WDIOC_GETSUPPORT:
	{
		ident.options = watchdogdev->options;
		ident.firmware_version = watchdogdev->firmware;

		strncpy(ident.identity, watchdogdev->name, 31);
		ident.identity[32] = 0;

		return copy_to_user(argp, &ident,
			sizeof(ident)) ? -EFAULT : 0;
	}

	case WDIOC_GETSTATUS:
	{
		status = 0;

		if (watchdogdev->watchdog_ops->get_status &&
		    watchdogdev->watchdog_ops->get_status(watchdogdev, &status))
			return -EFAULT;

		return put_user(status, p);
	}

	case WDIOC_GETBOOTSTATUS:
		return put_user(watchdogdev->bootstatus, p);

	case WDIOC_KEEPALIVE:
	{
		if (!watchdogdev->watchdog_ops->keepalive)
			return -EFAULT;

		/* We only sent a keepalive when the watchdog is active */
		if (watchdogdev->watchdog_state == WATCHDOG_STARTED)
			watchdogdev->watchdog_ops->keepalive(watchdogdev);

		return 0;
	}

	case WDIOC_SETOPTIONS:
	{
		if (get_user(new_options, p))
			return -EFAULT;

		if (!watchdogdev->watchdog_ops->start ||
		    !watchdogdev->watchdog_ops->stop)
			return -EFAULT;

		if (new_options & WDIOS_DISABLECARD) {
			/* only try to stop the watchdog if it's allready running */
			if (watchdogdev->watchdog_state == WATCHDOG_STARTED) {
				err =  watchdogdev->watchdog_ops->stop(watchdogdev);
				if (err == 0) {
					watchdogdev->watchdog_state = WATCHDOG_STOPPED;
				} else {
					printk(KERN_CRIT PFX "WDIOS_DISABLECARD not successfull! (err=%d)",
						err);
					return -EFAULT;
				}
			}
		}

		if (new_options & WDIOS_ENABLECARD) {
			/* if the watchdog is not allready running, try to start it */
			if (watchdogdev->watchdog_state != WATCHDOG_STARTED) {
				err = watchdogdev->watchdog_ops->start(watchdogdev);
				if (err == 0) {
					watchdogdev->watchdog_state = WATCHDOG_STARTED;
				} else {
					printk(KERN_CRIT PFX "WDIOS_ENABLECARD not successfull! (err=%d)",
						err);
					return -EFAULT;
				}
			}
		}

		return 0;
	}

	case WDIOC_SETTIMEOUT:
	{
		if (!watchdogdev->watchdog_ops->set_heartbeat)
			return -ENOTTY;

		if (get_user(new_heartbeat, p))
			return -EFAULT;

		if (watchdogdev->watchdog_ops->set_heartbeat(watchdogdev, new_heartbeat))
			return -EFAULT;

		/* If the watchdog is active then we sent a keepalive to make sure
		 * that the watchdog keep's running (and if possible takes the new
		 * heartbeat) */
		if (watchdogdev->watchdog_ops->keepalive &&
		    (watchdogdev->watchdog_state == WATCHDOG_STARTED))
			watchdogdev->watchdog_ops->keepalive(watchdogdev);
		/* Fall */
	}

	case WDIOC_GETTIMEOUT:
		return put_user(watchdogdev->heartbeat, p);

	case WDIOC_GETTIMELEFT:
	{
		if (!watchdogdev->watchdog_ops->get_timeleft)
			return -ENOTTY;

		if (watchdogdev->watchdog_ops->get_timeleft(watchdogdev, &time_left))
			return -EFAULT;

		return put_user(time_left, p);
	}

	default:
		return -ENOTTY;
	}
}

/*
 *	watchdog_open: open the /dev/watchdog device.
 *	@inode: inode of device
 *	@file: file handle to device
 *
 *	When the /dev/watchdog device get's opened, we start the watchdog
 *	and feed it with his first keepalive signal. Watch out: the
 *	/dev/watchdog device is single open, so make sure it can only be
 *	opened once.
 */

static int watchdog_open(struct inode *inode, struct file *file)
{
	trace("%p, %p", inode, file);

	/* only open if we have a valid watchdog device */
	if (!watchdogdev ||
	    !watchdogdev->watchdog_ops ||
	    !watchdogdev->watchdog_ops->start ||
	    !watchdogdev->watchdog_ops->stop ||
	    !watchdogdev->watchdog_ops->keepalive)
		return -EBUSY;

	/* the watchdog is single open! */
	if (test_and_set_bit(0, &watchdog_dev_open))
		return -EBUSY;

	/* if the watchdog is not allready running, try to start it */
	if (watchdogdev->watchdog_state != WATCHDOG_STARTED) {
		if (watchdogdev->watchdog_ops->start(watchdogdev) == 0)
			watchdogdev->watchdog_state = WATCHDOG_STARTED;
	}

	/* if the watchdog started, then feed the watchdog it's first keepalive signal */
	if (watchdogdev->watchdog_state == WATCHDOG_STARTED)
		watchdogdev->watchdog_ops->keepalive(watchdogdev);

	return nonseekable_open(inode, file);
}

/*
 *      watchdog_release: release the /dev/watchdog device.
 *      @inode: inode of device
 *      @file: file handle to device
 *
 *	This is the code for when /dev/watchdog get's closed. We will only
 *	stop the watchdog when we have received the magic char, else the
 *	watchdog will keep running.
 */

static int watchdog_release(struct inode *inode, struct file *file)
{
	int err;

	trace("%p, %p", inode, file);
	dbg("received_magic_char=%d", received_magic_char);

	if (watchdogdev && (watchdogdev->watchdog_state == WATCHDOG_STARTED)) {
		/* Only stop a watchdog if it actually started */
		if (received_magic_char == 42) {
			/* we received the magic char -> we can stop the watchdog */
			if (watchdogdev->watchdog_ops && watchdogdev->watchdog_ops->stop) {
				err =  watchdogdev->watchdog_ops->stop(watchdogdev);
				if (err == 0) {
					watchdogdev->watchdog_state = WATCHDOG_STOPPED;
				} else {
					printk(KERN_CRIT PFX "Watchdog didn't stop successfull! (err=%d)",
						err);
				}
			} else {
				printk(KERN_CRIT PFX "Unable to stop watchdog!");
			}
		} else {
			/* If we didn't receive the magic char, then we will close
			 * /dev/watchdog but the watchdog keeps running... */
			printk(KERN_CRIT PFX "Unexpected close, not stopping watchdog!");
			if (watchdogdev->watchdog_ops && watchdogdev->watchdog_ops->keepalive) {
				watchdogdev->watchdog_ops->keepalive(watchdogdev);
			}
		}
	}

	received_magic_char = 0;

	/* make sure that /dev/watchdog can be re-opened */
	clear_bit(0, &watchdog_dev_open);

	return 0;
}

/*
 *	/dev/watchdog kernel interfaces
 */

static struct file_operations watchdog_fops = {
	.owner =	THIS_MODULE,
	.llseek =	no_llseek,
	.write =	watchdog_write,
	.ioctl =	watchdog_ioctl,
	.open =		watchdog_open,
	.release =	watchdog_release,
};

static struct miscdevice watchdog_miscdev = {
	.minor =	WATCHDOG_MINOR,
	.name =		"watchdog",
	.fops =		&watchdog_fops,
};

/*
 *	/dev/watchdog register and unregister functions
 */

/*
 *	watchdog_dev_register:
 *
 *	Register a watchdog device as /dev/watchdog. /dev/watchdog
 *	is actually a miscdevice and thus we set it up like that.
 */

int watchdog_dev_register(struct watchdog_device *wdd, struct device *parent)
{
	int err = -EBUSY;

	trace("%p %p", wdd, parent);

	mutex_lock(&watchdog_register_mtx);

	if (watchdogdev) {
		printk(KERN_ERR PFX "another watchdog device is allready registered as /dev/watchdog\n");
		goto out;
	}

	watchdog_miscdev.parent = parent;

	dbg("Register a new /dev/watchdog device\n");
	err = misc_register(&watchdog_miscdev);
	if (err != 0) {
		printk(KERN_ERR PFX "cannot register miscdev on minor=%d (err=%d)\n",
			watchdog_miscdev.minor, err);
		goto out;
	}

	watchdogdev = wdd;

out:
	mutex_unlock(&watchdog_register_mtx);
	return err;
}
EXPORT_SYMBOL(watchdog_dev_register);

/*
 *	watchdog_dev_unregister:
 *
 *	Deregister the /dev/watchdog device.
 */

int watchdog_dev_unregister(struct watchdog_device *wdd)
{
	trace("%p", wdd);

	mutex_lock(&watchdog_register_mtx);

	if (!watchdogdev) {
		printk(KERN_ERR PFX "there is no watchdog registered\n");
		mutex_unlock(&watchdog_register_mtx);
		return -1;
	}

	if (!wdd) {
		printk(KERN_ERR PFX "cannot unregister non-existing watchdog-driver\n");
		mutex_unlock(&watchdog_register_mtx);
		return -2;
	}

	if (watchdogdev != wdd) {
		printk(KERN_ERR PFX "another watchdog device is running\n");
		mutex_unlock(&watchdog_register_mtx);
		return -3;
	}

	dbg("Unregister /dev/watchdog device\n");
	misc_deregister(&watchdog_miscdev);
	watchdogdev = NULL;
	mutex_unlock(&watchdog_register_mtx);
	return 0;
}
EXPORT_SYMBOL(watchdog_dev_unregister);

MODULE_AUTHOR("Wim Van Sebroeck <wim@iguana.be>");
MODULE_DESCRIPTION("Generic /dev/watchdog Code");
MODULE_VERSION(DRV_VERSION);
MODULE_LICENSE("GPL");

