/*
 * Elantech Touchpad driver
 *
 * Copyright (C) 2007 Arjan Opmeer <arjan@opmeer.net>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 *
 * Trademarks are the property of their respective owners.
 */

#ifndef _ELANTECH_H
#define _ELANTECH_H

/*
 * Commands start with this value
 */
#define ELANTECH_COMMAND_START		0x11

/*
 * Register bitmasks
 */
#define ETP_R10_ABSOLUTE_MODE		0x04
#define ETP_R11_PARITY_CHECKING		0x01
#define ETP_R11_4_BYTE_MODE 		0x02

/*
 * Capability bitmasks
 */
#define ETP_CAP_REPORTS_MIDDLE_BUTTON	0x02
#define ETP_CAP_HAS_ROCKER		0x04
#define ETP_CAP_ALTERNATE_TAP_BITS	0x10

/*
 * One hard to find application note states that X axis range is 0 to 576
 * and Y axis range is 0 to 384.
 * Edge fuzz might be necessary because of bezel around the touchpad.
 */
#define ETP_EDGE_FUZZ			32

#define ETP_XMIN 			(  0 + ETP_EDGE_FUZZ)
#define ETP_XMAX 			(576 - ETP_EDGE_FUZZ)
#define ETP_YMIN 			(  0 + ETP_EDGE_FUZZ)
#define ETP_YMAX 			(384 - ETP_EDGE_FUZZ)

/*
 * It seems the touchpad does not report pressure.
 * Just choose some values for compatibility with X Synaptics driver
 */
#define ETP_MAX_PRESSURE		127
#define ETP_DEF_PRESSURE		64

struct elantech_data {
	unsigned char reg_10;
	unsigned char reg_11;
	unsigned char reg_20;
	unsigned char reg_21;
	unsigned char reg_22;
	unsigned char reg_23;
	unsigned char reg_24;
	unsigned char reg_25;
	unsigned char reg_26;
	unsigned char debug;
	unsigned char capabilities;
};

#ifdef CONFIG_MOUSE_PS2_ELANTECH
int elantech_detect(struct psmouse *psmouse, int set_properties);
int elantech_init(struct psmouse *psmouse);
#else
static inline int elantech_detect(struct psmouse *psmouse, int set_properties)
{
	return -ENOSYS;
}
static inline int elantech_init(struct psmouse *psmouse)
{
	return -ENOSYS;
}
#endif /* CONFIG_MOUSE_PS2_ELANTECH */

#endif
