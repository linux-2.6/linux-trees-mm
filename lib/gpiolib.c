#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/irq.h>
#include <linux/spinlock.h>

#include <asm/gpio.h>


/* Optional implementation infrastructure for GPIO interfaces.
 *
 * Platforms may want to use this if they tend to use very many GPIOs
 * that aren't part of a System-On-Chip core; or across I2C/SPI/etc.
 *
 * When kernel footprint or instruction count is an issue, simpler
 * implementations may be preferred.  The GPIO programming interface
 * allows for inlining speed-critical get/set operations for common
 * cases, so that access to SOC-integrated GPIOs can sometimes cost
 * only an instruction or two per bit.
 */


/* When debugging, extend minimal trust to callers and platform code.
 * Otherwise, minimize overhead in what may be bitbanging codepaths.
 */
#ifdef	CONFIG_DEBUG_GPIO
#define	extra_checks	1
#else
#define	extra_checks	0
#endif

/* gpio_lock protects the table of chips and gpio_chip->requested.
 * While any gpio is requested, its gpio_chip is not removable.  It's
 * a raw spinlock to ensure safe access from hardirq contexts, and to
 * shrink bitbang overhead:  per-bit preemption would be very wrong.
 */
static raw_spinlock_t gpio_lock = __RAW_SPIN_LOCK_UNLOCKED;
static struct gpio_chip *chips[DIV_ROUND_UP(ARCH_NR_GPIOS,
					ARCH_GPIOS_PER_CHIP)];


/* Warn when drivers omit gpio_request() calls -- legal but
 * ill-advised when setting direction, and otherwise illegal.
 */
static void gpio_ensure_requested(struct gpio_chip *chip, unsigned offset)
{
	int		requested;

#ifdef CONFIG_DEBUG_FS
	requested = (int) chip->requested[offset];
	if (!requested)
		chip->requested[offset] = "[auto]";
#else
	requested = test_and_set_bit(offset, chip->requested);
#endif

	if (!requested)
		printk(KERN_DEBUG "GPIO-%d autorequested\n",
				chip->base + offset);
}

/* caller holds gpio_lock */
static inline struct gpio_chip *gpio_to_chip(unsigned gpio)
{
	return chips[gpio / ARCH_GPIOS_PER_CHIP];
}

/**
 * gpiochip_add() - register a gpio_chip
 * @chip: the chip to register, with chip->base initialized
 * Context: potentially before irqs or kmalloc will work
 *
 * Returns a negative errno if the chip can't be registered, such as
 * because the chip->base is invalid or already associated with a
 * different chip.  Otherwise it returns zero as a success code.
 */
int gpiochip_add(struct gpio_chip *chip)
{
	unsigned long	flags;
	int		status = 0;
	unsigned	id;

	if (chip->base < 0 || (chip->base % ARCH_GPIOS_PER_CHIP) != 0)
		return -EINVAL;
	if ((chip->base + chip->ngpio) >= ARCH_NR_GPIOS)
		return -EINVAL;
	if (chip->ngpio > ARCH_GPIOS_PER_CHIP)
		return -EINVAL;

	local_irq_save(flags);
	__raw_spin_lock(&gpio_lock);

	id = chip->base / ARCH_GPIOS_PER_CHIP;
	if (chips[id] == NULL)
		chips[id] = chip;
	else
		status = -EBUSY;

	__raw_spin_unlock(&gpio_lock);
	local_irq_restore(flags);
	return status;
}
EXPORT_SYMBOL_GPL(gpiochip_add);

/**
 * gpiochip_remove() - unregister a gpio_chip
 * @chip: the chip to unregister
 *
 * A gpio_chip with any GPIOs still requested may not be removed.
 */
int gpiochip_remove(struct gpio_chip *chip)
{
	unsigned long	flags;
	int		status = 0;
	int		offset;
	unsigned	id = chip->base / ARCH_GPIOS_PER_CHIP;

	local_irq_save(flags);
	__raw_spin_lock(&gpio_lock);

	for (offset = 0; offset < chip->ngpio; offset++) {
		if (gpiochip_is_requested(chip, offset)) {
			status = -EBUSY;
			break;
		}
	}

	if (status == 0) {
		if (chips[id] == chip)
			chips[id] = NULL;
		else
			status = -EINVAL;
	}

	__raw_spin_unlock(&gpio_lock);
	local_irq_restore(flags);
	return status;
}
EXPORT_SYMBOL_GPL(gpiochip_remove);


/* These "optional" allocation calls help prevent drivers from stomping
 * on each other, and help provide better diagnostics in debugfs.
 * They're called even less than the "set direction" calls.
 */
int gpio_request(unsigned gpio, const char *label)
{
	struct gpio_chip	*chip;
	int			status = -EINVAL;
	unsigned long		flags;

	local_irq_save(flags);
	__raw_spin_lock(&gpio_lock);

	chip = gpio_to_chip(gpio);
	if (!chip)
		goto done;
	gpio -= chip->base;
	if (gpio >= chip->ngpio)
		goto done;

	/* NOTE:  gpio_request() can be called in early boot,
	 * before IRQs are enabled.
	 */

	status = 0;
#ifdef CONFIG_DEBUG_FS
	if (!label)
		label = "?";
	if (chip->requested[gpio])
		status = -EBUSY;
	else
		chip->requested[gpio] = label;
#else
	if (test_and_set_bit(gpio, chip->requested))
		status = -EBUSY;
#endif

done:
	__raw_spin_unlock(&gpio_lock);
	local_irq_restore(flags);
	return status;
}
EXPORT_SYMBOL_GPL(gpio_request);

void gpio_free(unsigned gpio)
{
	unsigned long		flags;
	struct gpio_chip	*chip;

	local_irq_save(flags);
	__raw_spin_lock(&gpio_lock);

	chip = gpio_to_chip(gpio);
	if (!chip)
		goto done;
	gpio -= chip->base;
	if (gpio >= chip->ngpio)
		goto done;

#ifdef CONFIG_DEBUG_FS
	if (chip->requested[gpio])
		chip->requested[gpio] = NULL;
	else
		chip = NULL;
#else
	if (!test_and_clear_bit(gpio, chip->requested))
		chip = NULL;
#endif
	WARN_ON(extra_checks && chip == NULL);
done:
	__raw_spin_unlock(&gpio_lock);
	local_irq_restore(flags);
}
EXPORT_SYMBOL_GPL(gpio_free);



/* Drivers MUST make configuration calls before get/set calls
 *
 * As a rule these aren't called more than once (except for drivers
 * using the open-drain emulation idiom) so these are natural places
 * to accumulate extra debugging checks.  Note that we can't rely on
 * gpio_request() having been called beforehand.
 */

int gpio_direction_input(unsigned gpio)
{
	unsigned long		flags;
	struct gpio_chip	*chip;
	int			status = -EINVAL;

	local_irq_save(flags);
	__raw_spin_lock(&gpio_lock);

	chip = gpio_to_chip(gpio);
	if (!chip || !chip->get || !chip->direction_input)
		goto fail;
	gpio -= chip->base;
	if (gpio >= chip->ngpio)
		goto fail;
	gpio_ensure_requested(chip, gpio);

	/* now we know the gpio is valid and chip won't vanish */

	__raw_spin_unlock(&gpio_lock);
	local_irq_restore(flags);

	might_sleep_if(extra_checks && chip->can_sleep);

	status = chip->direction_input(chip, gpio);
	if (status == 0)
		clear_bit(gpio, chip->is_out);
	return status;
fail:
	__raw_spin_unlock(&gpio_lock);
	local_irq_restore(flags);
	return status;
}
EXPORT_SYMBOL_GPL(gpio_direction_input);

int gpio_direction_output(unsigned gpio, int value)
{
	unsigned long		flags;
	struct gpio_chip	*chip;
	int			status = -EINVAL;

	local_irq_save(flags);
	__raw_spin_lock(&gpio_lock);

	chip = gpio_to_chip(gpio);
	if (!chip || !chip->set || !chip->direction_output)
		goto fail;
	gpio -= chip->base;
	if (gpio >= chip->ngpio)
		goto fail;
	gpio_ensure_requested(chip, gpio);

	/* now we know the gpio is valid and chip won't vanish */

	__raw_spin_unlock(&gpio_lock);
	local_irq_restore(flags);

	might_sleep_if(extra_checks && chip->can_sleep);

	status = chip->direction_output(chip, gpio, value);
	if (status == 0)
		set_bit(gpio, chip->is_out);
	return status;
fail:
	__raw_spin_unlock(&gpio_lock);
	local_irq_restore(flags);
	return status;
}
EXPORT_SYMBOL_GPL(gpio_direction_output);


/* I/O calls are only valid after configuration completed; the relevant
 * "is this a valid GPIO" error checks should already have been done.
 *
 * "Get" operations are often inlinable as reading a pin value register,
 * and masking the relevant bit in that register.
 *
 * When "set" operations are inlinable, they involve writing that mask to
 * one register to set a low value, or a different register to set it high.
 * Otherwise locking is needed, so there may be little value to inlining.
 */
int __gpio_get_value(unsigned gpio)
{
	unsigned long		flags;
	struct gpio_chip	*chip;

	local_irq_save(flags);
	__raw_spin_lock(&gpio_lock);

	chip = gpio_to_chip(gpio);
	if (extra_checks)
		gpio_ensure_requested(chip, gpio - chip->base);

	__raw_spin_unlock(&gpio_lock);
	local_irq_restore(flags);

	WARN_ON(extra_checks && chip->can_sleep);
	return chip->get(chip, gpio - chip->base);
}
EXPORT_SYMBOL_GPL(__gpio_get_value);

void __gpio_set_value(unsigned gpio, int value)
{
	unsigned long		flags;
	struct gpio_chip	*chip;

	local_irq_save(flags);
	__raw_spin_lock(&gpio_lock);

	chip = gpio_to_chip(gpio);
	if (extra_checks)
		gpio_ensure_requested(chip, gpio - chip->base);

	__raw_spin_unlock(&gpio_lock);
	local_irq_restore(flags);

	WARN_ON(extra_checks && chip->can_sleep);
	chip->set(chip, gpio - chip->base, value);
}
EXPORT_SYMBOL_GPL(__gpio_set_value);

int __gpio_cansleep(unsigned gpio)
{
	unsigned long		flags;
	struct gpio_chip	*chip;

	local_irq_save(flags);
	__raw_spin_lock(&gpio_lock);

	chip = gpio_to_chip(gpio);
	if (extra_checks)
		gpio_ensure_requested(chip, gpio - chip->base);

	__raw_spin_unlock(&gpio_lock);
	local_irq_restore(flags);

	return chip->can_sleep;
}
EXPORT_SYMBOL_GPL(__gpio_cansleep);



/* There's no value in inlining GPIO calls that may sleep.
 * Common examples include ones connected to I2C or SPI chips.
 */

int gpio_get_value_cansleep(unsigned gpio)
{
	unsigned long		flags;
	struct gpio_chip	*chip;

	might_sleep_if(extra_checks);

	local_irq_save(flags);
	__raw_spin_lock(&gpio_lock);

	chip = gpio_to_chip(gpio);
	if (extra_checks)
		gpio_ensure_requested(chip, gpio - chip->base);

	__raw_spin_unlock(&gpio_lock);
	local_irq_restore(flags);

	return chip->get(chip, gpio - chip->base);
}
EXPORT_SYMBOL_GPL(gpio_get_value_cansleep);

void gpio_set_value_cansleep(unsigned gpio, int value)
{
	unsigned long		flags;
	struct gpio_chip	*chip;

	might_sleep_if(extra_checks);

	local_irq_save(flags);
	__raw_spin_lock(&gpio_lock);

	chip = gpio_to_chip(gpio);
	if (extra_checks)
		gpio_ensure_requested(chip, gpio - chip->base);

	__raw_spin_unlock(&gpio_lock);
	local_irq_restore(flags);

	chip->set(chip, gpio - chip->base, value);
}
EXPORT_SYMBOL_GPL(gpio_set_value_cansleep);


#ifdef CONFIG_DEBUG_FS

#include <linux/debugfs.h>
#include <linux/seq_file.h>


static void gpiolib_dbg_show(struct seq_file *s, struct gpio_chip *chip)
{
	unsigned	i;

	for (i = 0; i < chip->ngpio; i++) {
		unsigned	gpio;
		int		is_out;

		if (!chip->requested[i])
			continue;

		gpio = chip->base + i;
		is_out = test_bit(i, chip->is_out);

		seq_printf(s, " gpio-%-3d (%-12s) %s %s",
			gpio, chip->requested[i],
			is_out ? "out" : "in ",
			chip->get
				? (chip->get(chip, i) ? "hi" : "lo")
				: "?  ");

		if (!is_out) {
			int		irq = gpio_to_irq(gpio);
			struct irq_desc	*desc = irq_desc + irq;

			/* This races with request_irq(), set_irq_type(),
			 * and set_irq_wake() ... but those are "rare".
			 *
			 * More significantly, trigger type flags aren't
			 * currently maintained by genirq.
			 */
			if (irq >= 0 && desc->action) {
				char *trigger;

				switch (desc->status & IRQ_TYPE_SENSE_MASK) {
				case IRQ_TYPE_NONE:
					trigger = "(default)";
					break;
				case IRQ_TYPE_EDGE_FALLING:
					trigger = "edge-falling";
					break;
				case IRQ_TYPE_EDGE_RISING:
					trigger = "edge-rising";
					break;
				case IRQ_TYPE_EDGE_BOTH:
					trigger = "edge-both";
					break;
				case IRQ_TYPE_LEVEL_HIGH:
					trigger = "level-high";
					break;
				case IRQ_TYPE_LEVEL_LOW:
					trigger = "level-low";
					break;
				default:
					trigger = "?trigger?";
					break;
				}

				seq_printf(s, " irq-%d %s%s",
					irq, trigger,
					(desc->status & IRQ_WAKEUP)
						? " wakeup" : "");
			}
		}

		seq_printf(s, "\n");
	}
}

static int gpiolib_show(struct seq_file *s, void *unused)
{
	struct gpio_chip	*chip;
	unsigned		id;
	int			started = 0;

	/* REVISIT this isn't locked against gpio_chip removal ... */

	for (id = 0; id < ARRAY_SIZE(chips); id++) {
		chip = chips[id];
		if (!chip)
			continue;

		seq_printf(s, "%sGPIOs %d-%d, %s%s:\n",
				started ? "\n" : "",
				chip->base, chip->base + chip->ngpio - 1,
				chip->label ? : "generic",
				chip->can_sleep ? ", can sleep" : "");
		started = 1;
		if (chip->dbg_show)
			chip->dbg_show(s, chip);
		else
			gpiolib_dbg_show(s, chip);
	}
	return 0;
}

static int gpiolib_open(struct inode *inode, struct file *file)
{
	return single_open(file, gpiolib_show, NULL);
}

static struct file_operations gpiolib_operations = {
	.open		= gpiolib_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= single_release,
};

static int __init gpiolib_debugfs_init(void)
{
	/* /sys/kernel/debug/gpio */
	(void) debugfs_create_file("gpio", S_IFREG | S_IRUGO,
				NULL, NULL, &gpiolib_operations);
	return 0;
}
subsys_initcall(gpiolib_debugfs_init);

#endif	/* DEBUG_FS */
