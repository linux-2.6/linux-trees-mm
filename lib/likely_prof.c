/*
 * This code should enable profiling the likely and unlikely macros.
 *
 * Output goes in /proc/likely_prof
 *
 * Authors:
 * Daniel Walker <dwalker@mvista.com>
 * Hua Zhong <hzhong@gmail.com>
 * Andrew Morton <akpm@osdl.org>
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/fs.h>
#include <linux/seq_file.h>
#include <linux/proc_fs.h>

#include <asm/bug.h>
#include <asm/atomic.h>

static struct likeliness *likeliness_head;

int do_check_likely(struct likeliness *likeliness, int ret)
{
	static unsigned long likely_lock;

	if (ret)
		likeliness->count[1]++;
	else
		likeliness->count[0]++;

	if (likeliness->type & LP_UNSEEN) {
		/*
		 * We don't simple use a spinlock because internally to the
		 * spinlock there is a call to unlikely which causes recursion.
		 * We opted for this method because we didn't need a preempt/irq
		 * disable and it was a bit cleaner then using internal __raw
		 * spinlock calls.
		 */
		if (!test_and_set_bit(0, &likely_lock)) {
			if (likeliness->type & LP_UNSEEN) {
				likeliness->type &= (~LP_UNSEEN);
				likeliness->next = likeliness_head;
				likeliness_head = likeliness;
			}
			smp_mb__before_clear_bit();
			clear_bit(0, &likely_lock);
		}
	}

	return ret;
}
EXPORT_SYMBOL(do_check_likely);

static void * lp_seq_start(struct seq_file *out, loff_t *pos)
{

	if (!*pos) {

		seq_printf(out, "Likely Profiling Results\n");
		seq_printf(out, " --------------------------------------------"
				"------------------------\n");
		seq_printf(out, "[+- ] Type | # True | # False | Function:"
				"Filename@Line\n");

		out->private = likeliness_head;
	}

	return out->private;
}

static void *lp_seq_next(struct seq_file *out, void *p, loff_t *pos)
{
	struct likeliness *entry = p;

	if (entry->next) {
		++(*pos);
		out->private = entry->next;
	} else
		out->private = NULL;

	return out->private;
}

static int lp_seq_show(struct seq_file *out, void *p)
{
	struct likeliness *entry = p;
	unsigned int true = entry->count[1];
	unsigned int false = entry->count[0];

	if (!entry->type) {
		if (true > false)
			seq_printf(out, "+");
		else
			seq_printf(out, " ");

		seq_printf(out, "unlikely ");
	} else {
		if (true < false)
			seq_printf(out, "-");
		else
			seq_printf(out, " ");

		seq_printf(out, "likely   ");
	}

	seq_printf(out, "|%9u|%9u\t%s()@:%s@%d\n", true, false,
			entry->func, entry->file, entry->line);

	return 0;
}

static void lp_seq_stop(struct seq_file *m, void *p)
{
}

struct seq_operations likely_profiling_ops = {
	.start  = lp_seq_start,
	.next   = lp_seq_next,
	.stop   = lp_seq_stop,
	.show   = lp_seq_show
};

static int lp_results_open(struct inode *inode, struct file *file)
{
	return seq_open(file, &likely_profiling_ops);
}

static struct file_operations proc_likely_operations  = {
	.open           = lp_results_open,
	.read           = seq_read,
	.llseek         = seq_lseek,
	.release        = seq_release,
};

static int __init init_likely(void)
{
	struct proc_dir_entry *entry;
	entry = create_proc_entry("likely_prof", 0, &proc_root);
	if (entry)
		entry->proc_fops = &proc_likely_operations;

	return 0;
}
__initcall(init_likely);
