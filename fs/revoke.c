/*
 * fs/revoke.c - Invalidate all current open file descriptors of an inode.
 *
 * Copyright (C) 2006-2007  Pekka Enberg
 *
 * This file is released under the GPLv2.
 */

#include <linux/file.h>
#include <linux/fs.h>
#include <linux/namei.h>
#include <linux/magic.h>
#include <linux/mm.h>
#include <linux/mman.h>
#include <linux/module.h>
#include <linux/mount.h>
#include <linux/sched.h>
#include <linux/revoked_fs_i.h>
#include <linux/syscalls.h>

/**
 * fileset - an array of file pointers.
 * @files:    the array of file pointers
 * @nr:               number of elements in the array
 * @end:      index to next unused file pointer
 */
struct fileset {
	struct file	**files;
	unsigned long	nr;
	unsigned long	end;
};

/**
 * revoke_details - details of the revoke operation
 * @inode:            invalidate open file descriptors of this inode
 * @fset:             set of files that point to a revoked inode
 * @restore_start:    index to the first file pointer that is currently in
 *                    use by a file descriptor but the real file has not
 *                    been revoked
 */
struct revoke_details {
	struct fileset	*fset;
	unsigned long	restore_start;
};

static struct kmem_cache *revokefs_inode_cache;

static inline bool fset_is_full(struct fileset *set)
{
	return set->nr == set->end;
}

static inline struct file *fset_get_filp(struct fileset *set)
{
	return set->files[set->end++];
}

static struct fileset *alloc_fset(unsigned long size)
{
	struct fileset *fset;

	fset = kzalloc(sizeof *fset, GFP_KERNEL);
	if (!fset)
		return NULL;

	fset->files = kcalloc(size, sizeof(struct file *), GFP_KERNEL);
	if (!fset->files) {
		kfree(fset);
		return NULL;
	}
	fset->nr = size;
	return fset;
}

static void free_fset(struct fileset *fset)
{
      int i;

      for (i = fset->end; i < fset->nr; i++)
              fput(fset->files[i]);

      kfree(fset->files);
      kfree(fset);
}

/*
 * Revoked file descriptors point to inodes in the revokefs filesystem.
 */
static struct vfsmount *revokefs_mnt;

static struct file *get_revoked_file(void)
{
	struct dentry *dentry;
	struct inode *inode;
	struct file *filp;
	struct qstr name;

	filp = get_empty_filp();
	if (!filp)
		goto err;

	inode = new_inode(revokefs_mnt->mnt_sb);
	if (!inode)
		goto err_inode;

	name.name = "revoked_file";
	name.len = strlen(name.name);
	dentry = d_alloc(revokefs_mnt->mnt_sb->s_root, &name);
	if (!dentry)
		goto err_dentry;

	d_instantiate(dentry, inode);

	filp->f_mapping = inode->i_mapping;
	filp->f_dentry = dget(dentry);
	filp->f_vfsmnt = mntget(revokefs_mnt);
	filp->f_op = fops_get(inode->i_fop);
	filp->f_pos = 0;

	return filp;

  err_dentry:
	iput(inode);
  err_inode:
	fput(filp);
  err:
	return NULL;
}

static inline bool can_revoke_file(struct file *file, struct inode *inode,
				   struct file *to_exclude)
{
	if (!file || file == to_exclude)
		return false;

	return file->f_dentry->d_inode == inode;
}

/*
 * 	LOCKING: task_lock(owner)
 */
static int revoke_fds(struct task_struct *owner,
		      struct inode *inode,
		      struct file *to_exclude, struct fileset *fset)
{
	struct files_struct *files;
	struct fdtable *fdt;
	unsigned int fd;
	int err = 0;

	files = get_files_struct(owner);
	if (!files)
		goto out;

	spin_lock(&files->file_lock);
	fdt = files_fdtable(files);

	for (fd = 0; fd < fdt->max_fds; fd++) {
		struct revokefs_inode_info *info;
		struct file *filp, *new_filp;
		struct inode *new_inode;

		filp = fcheck_files(files, fd);
		if (!can_revoke_file(filp, inode, to_exclude))
			continue;

		if (!filp->f_op->revoke) {
			err = -EOPNOTSUPP;
			goto failed;
		}

		if (fset_is_full(fset)) {
			err = -ENOMEM;
			goto failed;
		}

		new_filp = fset_get_filp(fset);

		/*
		 * Replace original struct file pointer with a pointer to
		 * a 'revoked file.'  After this point, we don't need to worry
		 * about racing with sys_close or sys_dup.
		 */
		rcu_assign_pointer(fdt->fd[fd], new_filp);

		/*
		 * Hold on to task until we can take down the file and its
		 * mmap.
		 */
		get_task_struct(owner);

		new_inode = new_filp->f_dentry->d_inode;
		make_revoked_inode(new_inode, inode->i_mode & S_IFMT);

		info = revokefs_i(new_inode);
		info->fd = fd;
		info->file = filp;
		info->owner = owner;
	}
  failed:
	spin_unlock(&files->file_lock);
	put_files_struct(files);
  out:
	return err;
}

static inline bool can_revoke_vma(struct vm_area_struct *vma,
				  struct inode *inode, struct file *to_exclude)
{
	struct file *file = vma->vm_file;

	if (vma->vm_flags & VM_REVOKED)
		return false;

	if (!file || file == to_exclude)
		return false;

	return file->f_path.dentry->d_inode == inode;
}

static int __revoke_break_cow(struct task_struct *tsk, struct inode *inode,
			      struct file *to_exclude)
{
	struct mm_struct *mm = tsk->mm;
	struct vm_area_struct *vma;
	int err = 0;

	down_read(&mm->mmap_sem);
	for (vma = mm->mmap; vma != NULL; vma = vma->vm_next) {
		int ret;

		if (vma->vm_flags & VM_SHARED)
			continue;

		if (!can_revoke_vma(vma, inode, to_exclude))
			continue;

		ret = get_user_pages(tsk, tsk->mm, vma->vm_start,
				     vma_pages(vma), 1, 1, NULL, NULL);
		if (ret < 0) {
			err = ret;
			break;
		}

		unlink_file_vma(vma);
		fput(vma->vm_file);
		vma->vm_file = NULL;
	}
	up_read(&mm->mmap_sem);
	return err;
}

static int revoke_break_cow(struct fileset *fset, struct inode *inode,
			    struct file *to_exclude)
{
	unsigned long i;
	int err = 0;

	for (i = 0; i < fset->end; i++) {
		struct revokefs_inode_info *info;
		struct file *this;

		this = fset->files[i];
		info = revokefs_i(this->f_dentry->d_inode);

		err = __revoke_break_cow(info->owner, inode, to_exclude);
		if (err)
			break;
	}
	return err;
}

/*
 *	 LOCKING: down_write(&mm->mmap_sem)
 *	 	    -> spin_lock(&mapping->i_mmap_lock)
 */
static int revoke_vma(struct vm_area_struct *vma, struct zap_details *details)
{
	unsigned long restart_addr, start_addr, end_addr;
	int need_break;

	start_addr = vma->vm_start;
	end_addr = vma->vm_end;

  again:
	restart_addr = zap_page_range(vma, start_addr, end_addr - start_addr,
				      details);

	need_break = need_resched() || need_lockbreak(details->i_mmap_lock);
	if (need_break)
		goto out_need_break;

	if (restart_addr < end_addr) {
		start_addr = restart_addr;
		goto again;
	}
	vma->vm_flags |= VM_REVOKED;
	return 0;

  out_need_break:
	spin_unlock(details->i_mmap_lock);
	cond_resched();
	spin_lock(details->i_mmap_lock);
	return -EINTR;
}

/*
 *	LOCKING: spin_lock(&mapping->i_mmap_lock)
 */
static int revoke_mm(struct mm_struct *mm, struct address_space *mapping,
		     struct file *to_exclude)
{
	struct vm_area_struct *vma;
	struct zap_details details;
	int err = 0;

	details.i_mmap_lock = &mapping->i_mmap_lock;

	/*
 	 * If ->mmap_sem is under contention, we continue scanning other
 	 * mms and try again later.
 	 */
	if (!down_write_trylock(&mm->mmap_sem)) {
		err = -EAGAIN;
		goto out;
	}
	for (vma = mm->mmap; vma != NULL; vma = vma->vm_next) {
		if (!(vma->vm_flags & VM_SHARED))
			continue;

		if (!can_revoke_vma(vma, mapping->host, to_exclude))
			continue;

		err = revoke_vma(vma, &details);
		if (err)
			break;

		__unlink_file_vma(vma);
		fput(vma->vm_file);
		vma->vm_file = NULL;
	}
	up_write(&mm->mmap_sem);
  out:
	return err;
}

/*
 *	LOCKING: spin_lock(&mapping->i_mmap_lock)
 */
static void revoke_mapping_tree(struct address_space *mapping,
				struct file *to_exclude)
{
	struct vm_area_struct *vma;
	struct prio_tree_iter iter;
	int try_again = 0;

  restart:
	vma_prio_tree_foreach(vma, &iter, &mapping->i_mmap, 0, ULONG_MAX) {
		int err;

		if (!(vma->vm_flags & VM_SHARED))
			continue;

		if (likely(!can_revoke_vma(vma, mapping->host, to_exclude)))
			continue;

		err = revoke_mm(vma->vm_mm, mapping, to_exclude);
		if (err == -EAGAIN)
			try_again = 1;

		goto restart;
	}
	if (try_again) {
		cond_resched();
		goto restart;
	}
}

/*
 *	LOCKING: spin_lock(&mapping->i_mmap_lock)
 */
static void revoke_mapping_list(struct address_space *mapping,
				struct file *to_exclude)
{
	struct vm_area_struct *vma;
	int try_again = 0;

  restart:
	list_for_each_entry(vma, &mapping->i_mmap_nonlinear, shared.vm_set.list) {
		int err;

		if (likely(!can_revoke_vma(vma, mapping->host, to_exclude)))
			continue;

		err = revoke_mm(vma->vm_mm, mapping, to_exclude);
		if (err == -EAGAIN) {
			try_again = 1;
			continue;
		}
		if (err == -EINTR)
			goto restart;
	}
	if (try_again) {
		cond_resched();
		goto restart;
	}
}

static void revoke_mapping(struct address_space *mapping, struct file *to_exclude)
{
	spin_lock(&mapping->i_mmap_lock);
	if (unlikely(!prio_tree_empty(&mapping->i_mmap)))
		revoke_mapping_tree(mapping, to_exclude);
	if (unlikely(!list_empty(&mapping->i_mmap_nonlinear)))
		revoke_mapping_list(mapping, to_exclude);
	spin_unlock(&mapping->i_mmap_lock);
}

static void restore_file(struct revokefs_inode_info *info)
{
	struct files_struct *files;

	files = get_files_struct(info->owner);
	if (files) {
		struct fdtable *fdt;
		struct file *filp;

		spin_lock(&files->file_lock);
		fdt = files_fdtable(files);

		filp = fdt->fd[info->fd];
		if (filp)
			fput(filp);

		rcu_assign_pointer(fdt->fd[info->fd], info->file);
		FD_SET(info->fd, fdt->close_on_exec);
		spin_unlock(&files->file_lock);
		put_files_struct(files);
	}
	put_task_struct(info->owner);
	info->owner = NULL;	/* To avoid double-restore. */
}

static void restore_files(struct revoke_details *details)
{
	unsigned long i;

	for (i = details->restore_start; i < details->fset->end; i++) {
		struct revokefs_inode_info *info;
		struct file *filp;

		filp = details->fset->files[i];
		info = revokefs_i(filp->f_dentry->d_inode);

		restore_file(info);
	}
}

static int revoke_files(struct revoke_details *details)
{
	unsigned long i;
	int err = 0;

	for (i = 0; i < details->fset->end; i++) {
		struct revokefs_inode_info *info;
		struct file *this, *filp;
		struct inode *inode;

		this = details->fset->files[i];
		inode = this->f_dentry->d_inode;
		info = revokefs_i(inode);

		/*
		 * Increase count before attempting to close file as
		 * an partially closed file can no longer be restored.
		 */
		details->restore_start++;
		filp = info->file;
		err = filp->f_op->revoke(filp, inode->i_mapping);
		put_task_struct(info->owner);
		info->owner = NULL;	/* To avoid restoring closed file. */
		if (err)
			goto out;
	}
  out:
	return err;
}

/*
 *	Returns the maximum number of file descriptors pointing to an inode.
 *
 *	LOCKING: read_lock(&tasklist_lock)
 */
static unsigned long inode_fds(struct inode *inode, struct file *to_exclude)
{
	struct task_struct *g, *p;
	unsigned long nr_fds = 0;

	do_each_thread(g, p) {
		struct files_struct *files;
		struct fdtable *fdt;
		unsigned int fd;

		files = get_files_struct(p);
		if (!files)
			continue;

		spin_lock(&files->file_lock);
		fdt = files_fdtable(files);
		for (fd = 0; fd < fdt->max_fds; fd++) {
			struct file *file;

			file = fcheck_files(files, fd);
			if (can_revoke_file(file, inode, to_exclude)) {
				nr_fds += fdt->max_fds;
				break;
			}
		}
		spin_unlock(&files->file_lock);
		put_files_struct(files);
	}
	while_each_thread(g, p);
	return nr_fds;
}

static struct fileset *__alloc_revoke_fset(unsigned long size)
{
	struct fileset *fset;
	int i;

	fset = alloc_fset(size);
	if (!fset)
		return NULL;

	for (i = 0; i < fset->nr; i++) {
		struct file *filp;

		filp = get_revoked_file();
		if (!filp)
			goto err;

		fset->files[i] = filp;
	}
	return fset;
  err:
	free_fset(fset);
	return NULL;
}

static int do_revoke(struct inode *inode, struct file *to_exclude)
{
	struct revoke_details details;
	struct fileset *fset = NULL;
	struct task_struct *g, *p;
	unsigned long nr_fds;
	int err = 0;

	if (current->fsuid != inode->i_uid && !capable(CAP_FOWNER)) {
		err = -EPERM;
		goto out;
	}

  retry:
	if (signal_pending(current)) {
		err = -ERESTARTSYS;
		goto out;
	}

	read_lock(&tasklist_lock);
	nr_fds = inode_fds(inode, to_exclude);
	read_unlock(&tasklist_lock);

	if (!nr_fds)
		goto out;

	/*
	 * Pre-allocate memory because the first pass is done under
	 * tasklist_lock.
	 */
	fset = __alloc_revoke_fset(nr_fds);
	if (!fset) {
		err = -ENOMEM;
		goto out;
	}

	read_lock(&tasklist_lock);

	/*
	 * If someone forked while we were allocating memory, try again.
	 */
	if (inode_fds(inode, to_exclude) > fset->nr) {
		read_unlock(&tasklist_lock);
		free_fset(fset);
		goto retry;
	}

	details.fset = fset;
	details.restore_start = 0;

	/*
	 * First revoke the descriptors. After we are done, no one can start
	 * new operations on them.
	 */
	do_each_thread(g, p) {
		err = revoke_fds(p, inode, to_exclude, fset);
		if (err)
			goto exit_loop;
	}
	while_each_thread(g, p);
  exit_loop:
	read_unlock(&tasklist_lock);

	if (err)
		goto out_restore;

	/*
	 * Take down shared memory mappings.
	 */
	revoke_mapping(inode->i_mapping, to_exclude);

	/*
 	 * Break COW for private mappings.
 	 */
	err = revoke_break_cow(fset, inode, to_exclude);
	if (err)
		goto out_restore;

	/*
	 * Now, revoke the files for good.
	 */
	err = revoke_files(&details);
	if (err)
		goto out_restore;

  out_free_table:
	free_fset(fset);
  out:
	return err;

  out_restore:
	restore_files(&details);
	goto out_free_table;
}

asmlinkage long sys_revokeat(int dfd, const char __user * filename)
{
	struct nameidata nd;
	int err;

	err = __user_walk_fd(dfd, filename, 0, &nd);
	if (!err) {
		err = do_revoke(nd.path.dentry->d_inode, NULL);
		path_put(&nd.path);
	}
	return err;
}

asmlinkage long sys_frevoke(unsigned int fd)
{
	struct file *file = fget(fd);
	int err = -EBADF;

	if (file) {
		err = do_revoke(file->f_dentry->d_inode, file);
		fput(file);
	}
	return err;
}

int generic_file_revoke(struct file *file, struct address_space *new_mapping)
{
	struct address_space *mapping = file->f_mapping;
	int err;

	/*
	 * Flush pending writes.
	 */
	err = do_fsync(file, 1);
	if (err)
		goto out;

	file->f_mapping = new_mapping;

	/*
	 * Make pending reads fail.
	 */
	err = invalidate_inode_pages2(mapping);

  out:
	return err;
}
EXPORT_SYMBOL(generic_file_revoke);

/*
 *	Filesystem for revoked files.
 */

static struct inode *revokefs_alloc_inode(struct super_block *sb)
{
	struct revokefs_inode_info *info;

	info = kmem_cache_alloc(revokefs_inode_cache, GFP_KERNEL);
	if (!info)
		return NULL;

	return &info->vfs_inode;
}

static void revokefs_destroy_inode(struct inode *inode)
{
	kmem_cache_free(revokefs_inode_cache, revokefs_i(inode));
}

static struct super_operations revokefs_super_ops = {
	.alloc_inode = revokefs_alloc_inode,
	.destroy_inode = revokefs_destroy_inode,
	.drop_inode = generic_delete_inode,
};

static int revokefs_get_sb(struct file_system_type *fs_type,
			   int flags, const char *dev_name, void *data,
			   struct vfsmount *mnt)
{
	return get_sb_pseudo(fs_type, "revoke:", &revokefs_super_ops,
			     REVOKEFS_MAGIC, mnt);
}

static struct file_system_type revokefs_fs_type = {
	.name = "revokefs",
	.get_sb = revokefs_get_sb,
	.kill_sb = kill_anon_super
};

static void revokefs_init_inode(struct kmem_cache *cache, void *obj)
{
	struct revokefs_inode_info *info = obj;

	info->owner = NULL;
	inode_init_once(&info->vfs_inode);
}

static int __init revokefs_init(void)
{
	int err = -ENOMEM;

	revokefs_inode_cache =
	    kmem_cache_create("revokefs_inode_cache",
			      sizeof(struct revokefs_inode_info),
			      0,
			      (SLAB_HWCACHE_ALIGN | SLAB_RECLAIM_ACCOUNT |
			       SLAB_MEM_SPREAD), revokefs_init_inode);
	if (!revokefs_inode_cache)
		goto out;

	err = register_filesystem(&revokefs_fs_type);
	if (err)
		goto err_register;

	revokefs_mnt = kern_mount(&revokefs_fs_type);
	if (IS_ERR(revokefs_mnt)) {
		err = PTR_ERR(revokefs_mnt);
		goto err_mnt;
	}
  out:
	return err;
  err_mnt:
	unregister_filesystem(&revokefs_fs_type);
  err_register:
	kmem_cache_destroy(revokefs_inode_cache);
	return err;
}

late_initcall(revokefs_init);
