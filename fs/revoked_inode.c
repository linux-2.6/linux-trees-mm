/*
 * fs/revoked_inode.c
 *
 * Copyright (C) 2007  Pekka Enberg
 *
 * Provide stub functions for revoked inodes. Based on fs/bad_inode.c which is
 *
 * Copyright (C) 1997  Stephen Tweedie
 *
 * This file is released under the GPLv2.
 */

#include <linux/fs.h>
#include <linux/module.h>
#include <linux/stat.h>
#include <linux/time.h>
#include <linux/smp_lock.h>
#include <linux/namei.h>
#include <linux/poll.h>
#include <linux/revoked_fs_i.h>

static loff_t revoked_file_llseek(struct file *file, loff_t offset, int origin)
{
	return -EBADF;
}

static ssize_t revoked_file_read(struct file *filp, char __user * buf,
				 size_t size, loff_t * ppos)
{
	return -EBADF;
}

static ssize_t revoked_special_file_read(struct file *filp, char __user * buf,
					 size_t size, loff_t * ppos)
{
	return 0;
}

static ssize_t revoked_file_write(struct file *filp, const char __user * buf,
				  size_t siz, loff_t * ppos)
{
	return -EBADF;
}

static ssize_t revoked_file_aio_read(struct kiocb *iocb,
				     const struct iovec *iov,
				     unsigned long nr_segs, loff_t pos)
{
	return -EBADF;
}

static ssize_t revoked_file_aio_write(struct kiocb *iocb,
				      const struct iovec *iov,
				      unsigned long nr_segs, loff_t pos)
{
	return -EBADF;
}

static int revoked_file_readdir(struct file *filp, void *dirent,
				filldir_t filldir)
{
	return -EBADF;
}

static unsigned int revoked_file_poll(struct file *filp, poll_table * wait)
{
	return POLLERR;
}

static int revoked_file_ioctl(struct inode *inode, struct file *filp,
			      unsigned int cmd, unsigned long arg)
{
	return -EBADF;
}

static long revoked_file_unlocked_ioctl(struct file *file, unsigned cmd,
					unsigned long arg)
{
	return -EBADF;
}

static long revoked_file_compat_ioctl(struct file *file, unsigned int cmd,
				      unsigned long arg)
{
	return -EBADF;
}

static int revoked_file_mmap(struct file *file, struct vm_area_struct *vma)
{
	return -EBADF;
}

static int revoked_file_open(struct inode *inode, struct file *filp)
{
	return -EBADF;
}

static int revoked_file_flush(struct file *file, fl_owner_t id)
{
	return filp_close(file, id);
}

static int revoked_file_release(struct inode *inode, struct file *filp)
{
	return -EBADF;
}

static int revoked_file_fsync(struct file *file, struct dentry *dentry,
			      int datasync)
{
	return -EBADF;
}

static int revoked_file_aio_fsync(struct kiocb *iocb, int datasync)
{
	return -EBADF;
}

static int revoked_file_fasync(int fd, struct file *filp, int on)
{
	return -EBADF;
}

static int revoked_file_lock(struct file *file, int cmd, struct file_lock *fl)
{
	return -EBADF;
}

static ssize_t revoked_file_sendpage(struct file *file, struct page *page,
				     int off, size_t len, loff_t * pos,
				     int more)
{
	return -EBADF;
}

static unsigned long revoked_file_get_unmapped_area(struct file *file,
						    unsigned long addr,
						    unsigned long len,
						    unsigned long pgoff,
						    unsigned long flags)
{
	return -EBADF;
}

static int revoked_file_check_flags(int flags)
{
	return -EBADF;
}

static int revoked_file_dir_notify(struct file *file, unsigned long arg)
{
	return -EBADF;
}

static int revoked_file_flock(struct file *filp, int cmd, struct file_lock *fl)
{
	return -EBADF;
}

static ssize_t revoked_file_splice_write(struct pipe_inode_info *pipe,
					 struct file *out, loff_t * ppos,
					 size_t len, unsigned int flags)
{
	return -EBADF;
}

static ssize_t revoked_file_splice_read(struct file *in, loff_t * ppos,
					struct pipe_inode_info *pipe,
					size_t len, unsigned int flags)
{
	return -EBADF;
}

static const struct file_operations revoked_file_ops = {
	.llseek = revoked_file_llseek,
	.read = revoked_file_read,
	.write = revoked_file_write,
	.aio_read = revoked_file_aio_read,
	.aio_write = revoked_file_aio_write,
	.readdir = revoked_file_readdir,
	.poll = revoked_file_poll,
	.ioctl = revoked_file_ioctl,
	.unlocked_ioctl = revoked_file_unlocked_ioctl,
	.compat_ioctl = revoked_file_compat_ioctl,
	.mmap = revoked_file_mmap,
	.open = revoked_file_open,
	.flush = revoked_file_flush,
	.release = revoked_file_release,
	.fsync = revoked_file_fsync,
	.aio_fsync = revoked_file_aio_fsync,
	.fasync = revoked_file_fasync,
	.lock = revoked_file_lock,
	.sendpage = revoked_file_sendpage,
	.get_unmapped_area = revoked_file_get_unmapped_area,
	.check_flags = revoked_file_check_flags,
	.dir_notify = revoked_file_dir_notify,
	.flock = revoked_file_flock,
	.splice_write = revoked_file_splice_write,
	.splice_read = revoked_file_splice_read,
};

static const struct file_operations revoked_special_file_ops = {
	.llseek = revoked_file_llseek,
	.read = revoked_special_file_read,
	.write = revoked_file_write,
	.aio_read = revoked_file_aio_read,
	.aio_write = revoked_file_aio_write,
	.readdir = revoked_file_readdir,
	.poll = revoked_file_poll,
	.ioctl = revoked_file_ioctl,
	.unlocked_ioctl = revoked_file_unlocked_ioctl,
	.compat_ioctl = revoked_file_compat_ioctl,
	.mmap = revoked_file_mmap,
	.open = revoked_file_open,
	.flush = revoked_file_flush,
	.release = revoked_file_release,
	.fsync = revoked_file_fsync,
	.aio_fsync = revoked_file_aio_fsync,
	.fasync = revoked_file_fasync,
	.lock = revoked_file_lock,
	.sendpage = revoked_file_sendpage,
	.get_unmapped_area = revoked_file_get_unmapped_area,
	.check_flags = revoked_file_check_flags,
	.dir_notify = revoked_file_dir_notify,
	.flock = revoked_file_flock,
	.splice_write = revoked_file_splice_write,
	.splice_read = revoked_file_splice_read,
};

static int revoked_inode_create(struct inode *dir, struct dentry *dentry,
				int mode, struct nameidata *nd)
{
	return -EBADF;
}

static struct dentry *revoked_inode_lookup(struct inode *dir,
					   struct dentry *dentry,
					   struct nameidata *nd)
{
	return ERR_PTR(-EBADF);
}

static int revoked_inode_link(struct dentry *old_dentry, struct inode *dir,
			      struct dentry *dentry)
{
	return -EBADF;
}

static int revoked_inode_unlink(struct inode *dir, struct dentry *dentry)
{
	return -EBADF;
}

static int revoked_inode_symlink(struct inode *dir, struct dentry *dentry,
				 const char *symname)
{
	return -EBADF;
}

static int revoked_inode_mkdir(struct inode *dir, struct dentry *dentry,
			       int mode)
{
	return -EBADF;
}

static int revoked_inode_rmdir(struct inode *dir, struct dentry *dentry)
{
	return -EBADF;
}

static int revoked_inode_mknod(struct inode *dir, struct dentry *dentry,
			       int mode, dev_t rdev)
{
	return -EBADF;
}

static int revoked_inode_rename(struct inode *old_dir,
				struct dentry *old_dentry,
				struct inode *new_dir,
				struct dentry *new_dentry)
{
	return -EBADF;
}

static int revoked_inode_readlink(struct dentry *dentry, char __user * buffer,
				  int buflen)
{
	return -EBADF;
}

static int revoked_inode_permission(struct inode *inode, int mask,
				    struct nameidata *nd)
{
	return -EBADF;
}

static int revoked_inode_getattr(struct vfsmount *mnt, struct dentry *dentry,
				 struct kstat *stat)
{
	return -EBADF;
}

static int revoked_inode_setattr(struct dentry *direntry, struct iattr *attrs)
{
	return -EBADF;
}

static int revoked_inode_setxattr(struct dentry *dentry, const char *name,
				  const void *value, size_t size, int flags)
{
	return -EBADF;
}

static ssize_t revoked_inode_getxattr(struct dentry *dentry, const char *name,
				      void *buffer, size_t size)
{
	return -EBADF;
}

static ssize_t revoked_inode_listxattr(struct dentry *dentry, char *buffer,
				       size_t buffer_size)
{
	return -EBADF;
}

static int revoked_inode_removexattr(struct dentry *dentry, const char *name)
{
	return -EBADF;
}

static struct inode_operations revoked_inode_ops = {
	.create = revoked_inode_create,
	.lookup = revoked_inode_lookup,
	.link = revoked_inode_link,
	.unlink = revoked_inode_unlink,
	.symlink = revoked_inode_symlink,
	.mkdir = revoked_inode_mkdir,
	.rmdir = revoked_inode_rmdir,
	.mknod = revoked_inode_mknod,
	.rename = revoked_inode_rename,
	.readlink = revoked_inode_readlink,
	/* follow_link must be no-op, otherwise unmounting this inode
	   won't work */
	/* put_link returns void */
	/* truncate returns void */
	.permission = revoked_inode_permission,
	.getattr = revoked_inode_getattr,
	.setattr = revoked_inode_setattr,
	.setxattr = revoked_inode_setxattr,
	.getxattr = revoked_inode_getxattr,
	.listxattr = revoked_inode_listxattr,
	.removexattr = revoked_inode_removexattr,
	/* truncate_range returns void */
};

static int revoked_readpage(struct file *file, struct page *page)
{
	return -EIO;
}

static int revoked_writepage(struct page *page, struct writeback_control *wbc)
{
	return -EIO;
}

static int revoked_write_begin(struct file *file, struct address_space *mapping,
				loff_t pos, unsigned len, unsigned flags,
				struct page **pagep, void **fsdata)
{
	return -EIO;
}

static int revoked_write_end(struct file *file, struct address_space *mapping,
				loff_t pos, unsigned len, unsigned copied,
				struct page *page, void *fsdata)
{
	return -EIO;
}

static ssize_t revoked_direct_IO(int rw, struct kiocb *iocb,
				 const struct iovec *iov, loff_t offset,
				 unsigned long nr_segs)
{
	return -EIO;
}

static const struct address_space_operations revoked_aops = {
        .readpage       = revoked_readpage,
        .writepage      = revoked_writepage,
	.write_begin	= revoked_write_begin,
	.write_end	= revoked_write_end,
        .direct_IO      = revoked_direct_IO,
};

void make_revoked_inode(struct inode *inode, int mode)
{
	remove_inode_hash(inode);

	inode->i_mode = mode;
	inode->i_atime = inode->i_mtime = inode->i_ctime =
	    current_fs_time(inode->i_sb);
	inode->i_op = &revoked_inode_ops;

	if (special_file(mode))
		inode->i_fop = &revoked_special_file_ops;
	else
		inode->i_fop = &revoked_file_ops;

	inode->i_mapping->a_ops = &revoked_aops;
}
