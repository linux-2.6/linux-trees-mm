/*
 *	Generic watchdog defines. Derived from..
 *
 * Berkshire PC Watchdog Defines
 * by Ken Hollis <khollis@bitgate.com>
 *
 */

#ifndef _LINUX_WATCHDOG_H
#define _LINUX_WATCHDOG_H

#include <linux/ioctl.h>
#include <linux/types.h>

#define	WATCHDOG_IOCTL_BASE	'W'

struct watchdog_info {
	__u32 options;		/* Options the card/driver supports */
	__u32 firmware_version;	/* Firmware version of the card */
	__u8  identity[32];	/* Identity of the board */
};

#define	WDIOC_GETSUPPORT	_IOR(WATCHDOG_IOCTL_BASE, 0, struct watchdog_info)
#define	WDIOC_GETSTATUS		_IOR(WATCHDOG_IOCTL_BASE, 1, int)
#define	WDIOC_GETBOOTSTATUS	_IOR(WATCHDOG_IOCTL_BASE, 2, int)
#define	WDIOC_GETTEMP		_IOR(WATCHDOG_IOCTL_BASE, 3, int)
#define	WDIOC_SETOPTIONS	_IOR(WATCHDOG_IOCTL_BASE, 4, int)
#define	WDIOC_KEEPALIVE		_IOR(WATCHDOG_IOCTL_BASE, 5, int)
#define	WDIOC_SETTIMEOUT        _IOWR(WATCHDOG_IOCTL_BASE, 6, int)
#define	WDIOC_GETTIMEOUT        _IOR(WATCHDOG_IOCTL_BASE, 7, int)
#define	WDIOC_SETPRETIMEOUT	_IOWR(WATCHDOG_IOCTL_BASE, 8, int)
#define	WDIOC_GETPRETIMEOUT	_IOR(WATCHDOG_IOCTL_BASE, 9, int)
#define	WDIOC_GETTIMELEFT	_IOR(WATCHDOG_IOCTL_BASE, 10, int)

#define	WDIOF_UNKNOWN		-1	/* Unknown flag error */
#define	WDIOS_UNKNOWN		-1	/* Unknown status error */

#define	WDIOF_OVERHEAT		0x0001	/* Reset due to CPU overheat */
#define	WDIOF_FANFAULT		0x0002	/* Fan failed */
#define	WDIOF_EXTERN1		0x0004	/* External relay 1 */
#define	WDIOF_EXTERN2		0x0008	/* External relay 2 */
#define	WDIOF_POWERUNDER	0x0010	/* Power bad/power fault */
#define	WDIOF_CARDRESET		0x0020	/* Card previously reset the CPU */
#define	WDIOF_POWEROVER		0x0040	/* Power over voltage */
#define	WDIOF_SETTIMEOUT	0x0080  /* Set timeout (in seconds) */
#define	WDIOF_MAGICCLOSE	0x0100	/* Supports magic close char */
#define	WDIOF_PRETIMEOUT	0x0200  /* Pretimeout (in seconds), get/set */
#define	WDIOF_KEEPALIVEPING	0x8000	/* Keep alive ping reply */

#define	WDIOS_DISABLECARD	0x0001	/* Turn off the watchdog timer */
#define	WDIOS_ENABLECARD	0x0002	/* Turn on the watchdog timer */
#define	WDIOS_TEMPPANIC		0x0004	/* Kernel panic on temperature trip */

#ifdef __KERNEL__

#include <linux/device.h>

#ifdef CONFIG_WATCHDOG_NOWAYOUT
#define WATCHDOG_NOWAYOUT	1
#else
#define WATCHDOG_NOWAYOUT	0
#endif

struct watchdog_ops;
struct watchdog_device;

struct watchdog_ops {
	/* mandatory routines */
		/* operation = start watchdog */
		int	(*start)(struct watchdog_device *);
		/* operation = stop watchdog */
		int	(*stop)(struct watchdog_device *);
		/* operation = send keepalive ping */
		int	(*keepalive)(struct watchdog_device *);
	/* optional routines */
		/* operation = set watchdog's heartbeat */
		int	(*set_heartbeat)(struct watchdog_device *, int);
		/* operation = get the watchdog's status */
		int	(*get_status)(struct watchdog_device *, int *);
		/* operation = get the time left before reboot */
		int	(*get_timeleft)(struct watchdog_device *, int *);
};

struct watchdog_device {
	unsigned char name[32];			/* The watchdog's 'identity' */
	unsigned long options;			/* The supported capabilities/options */
	unsigned long firmware;			/* The Watchdog's Firmware version */
	int nowayout;				/* The nowayout setting for this watchdog */
	int heartbeat;				/* The watchdog's heartbeat */
	int bootstatus;				/* The watchdog's bootstatus */
	struct watchdog_ops *watchdog_ops;	/* link to watchdog_ops */

	/* watchdog status (register/unregister) state machine */
	enum { WATCHDOG_UNINITIALIZED = 0,
	       WATCHDOG_REGISTERED,		/* completed register_watchdogdevice */
	       WATCHDOG_STARTED,		/* watchdog device started */
	       WATCHDOG_STOPPED,		/* watchdog device stopped */
	       WATCHDOG_UNREGISTERED,		/* completed unregister_watchdogdevice */
	} watchdog_state;

	/* From here on everything is device dependent */
	void	*private;
};

/* drivers/watchdog/watchdog_core.c */
extern struct watchdog_device *alloc_watchdogdev(void);
extern int register_watchdogdevice(struct watchdog_device *, struct device *);
extern int unregister_watchdogdevice(struct watchdog_device *);
extern int free_watchdogdev(struct watchdog_device *);

#endif	/* __KERNEL__ */

#endif  /* ifndef _LINUX_WATCHDOG_H */
