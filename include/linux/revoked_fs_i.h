#ifndef _LINUX_REVOKED_FS_I_H
#define _LINUX_REVOKED_FS_I_H

struct revokefs_inode_info {
	struct task_struct *owner;
	struct file *file;
	unsigned int fd;
	struct inode vfs_inode;
};

static inline struct revokefs_inode_info *revokefs_i(struct inode *inode)
{
	return container_of(inode, struct revokefs_inode_info, vfs_inode);
}

void make_revoked_inode(struct inode *, int);

#endif
