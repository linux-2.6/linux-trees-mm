#ifndef _ASM_GENERIC_GPIO_H
#define _ASM_GENERIC_GPIO_H

#ifdef CONFIG_GPIO_LIB

/* Platforms may implement their GPIO interface with library code,
 * at a small performance cost for non-inlined operations.
 *
 * While the GPIO programming interface defines valid GPIO numbers
 * to be in the range 0..MAX_INT, this library restricts them to the
 * smaller range 0..ARCH_NR_GPIOS and allocates them in groups of
 * ARCH_GPIOS_PER_CHIP (which will usually be the word size used for
 * each bank of a SOC processor's integrated GPIO modules).
 */

#ifndef ARCH_NR_GPIOS
#define ARCH_NR_GPIOS		512
#endif

#ifndef ARCH_GPIOS_PER_CHIP
#define ARCH_GPIOS_PER_CHIP	BITS_PER_LONG
#endif

struct seq_file;

/**
 * struct gpio_chip - abstract a GPIO controller
 * @label: for diagnostics
 * @direction_input: configures signal "offset" as input, or returns error
 * @get: returns value for signal "offset"; for output signals this
 *	returns either the value actually sensed, or zero
 * @direction_output: configures signal "offset" as output, or returns error
 * @set: assigns output value for signal "offset"
 * @dbg_show: optional routine to show contents in debugfs; default code
 *	will be used when this is omitted, but custom code can show extra
 *	state (such as pullup/pulldown configuration).
 * @base: identifies the first GPIO number handled by this chip; or, if
 *	negative during registration, requests dynamic ID allocation.
 * @ngpio: the number of GPIOs handled by this controller; the value must
 *	be at most ARCH_GPIOS_PER_CHIP, so the last GPIO handled is
 *	(base + ngpio - 1).
 * @can_sleep: flag must be set iff get()/set() methods sleep, as they
 *	must while accessing GPIO expander chips over I2C or SPI
 * @is_out: bit array where bit N is true iff GPIO with offset N has been
 *	 called successfully to configure this as an output
 *
 * A gpio_chip can help platforms abstract various sources of GPIOs so
 * they can all be accessed through a common programing interface.
 * Example sources would be SOC controllers, FPGAs, multifunction
 * chips, dedicated GPIO expanders, and so on.
 *
 * Each chip controls a number of signals, numbered 0..@ngpio, which are
 * identified in method calls by an "offset" value.  When those signals
 * are referenced through calls like gpio_get_value(gpio), the offset
 * is calculated by subtracting @base from the gpio number.
 */
struct gpio_chip {
	char			*label;

	int			(*direction_input)(struct gpio_chip *chip,
						unsigned offset);
	int			(*get)(struct gpio_chip *chip,
						unsigned offset);
	int			(*direction_output)(struct gpio_chip *chip,
						unsigned offset, int value);
	void			(*set)(struct gpio_chip *chip,
						unsigned offset, int value);
	void			(*dbg_show)(struct seq_file *s,
						struct gpio_chip *chip);
	int			base;
	u16			ngpio;
	unsigned		can_sleep:1;

	/* other fields are modified by the gpio library only */
	DECLARE_BITMAP(is_out, ARCH_GPIOS_PER_CHIP);

#ifdef CONFIG_DEBUG_FS
	/* fat bits */
	const char		*requested[ARCH_GPIOS_PER_CHIP];
#else
	/* thin bits */
	DECLARE_BITMAP(requested, ARCH_GPIOS_PER_CHIP);
#endif
};

/* returns true iff a given gpio signal has been requested;
 * primarily for code dumping gpio_chip state.
 */
static inline int
gpiochip_is_requested(struct gpio_chip *chip, unsigned offset)
{
#ifdef CONFIG_DEBUG_FS
	return chip->requested[offset] != NULL;
#else
	return test_bit(offset, chip->requested);
#endif
}

/* add/remove chips */
extern int gpiochip_add(struct gpio_chip *chip);
extern int __must_check gpiochip_remove(struct gpio_chip *chip);


/* Always use the library code for GPIO management calls,
 * or when sleeping may be involved.
 */
extern int gpio_request(unsigned gpio, const char *label);
extern void gpio_free(unsigned gpio);

extern int gpio_direction_input(unsigned gpio);
extern int gpio_direction_output(unsigned gpio, int value);

extern int gpio_get_value_cansleep(unsigned gpio);
extern void gpio_set_value_cansleep(unsigned gpio, int value);


/* A platform's <asm/gpio.h> code may want to inline the I/O calls when
 * the GPIO is constant and refers to some always-present controller,
 * giving direct access to chip registers and tight bitbanging loops.
 */
extern int __gpio_get_value(unsigned gpio);
extern void __gpio_set_value(unsigned gpio, int value);

extern int __gpio_cansleep(unsigned gpio);


#else

/* platforms that don't directly support access to GPIOs through I2C, SPI,
 * or other blocking infrastructure can use these wrappers.
 */

static inline int gpio_cansleep(unsigned gpio)
{
	return 0;
}

static inline int gpio_get_value_cansleep(unsigned gpio)
{
	might_sleep();
	return gpio_get_value(gpio);
}

static inline void gpio_set_value_cansleep(unsigned gpio, int value)
{
	might_sleep();
	gpio_set_value(gpio, value);
}

#endif

#endif /* _ASM_GENERIC_GPIO_H */
