/*
 * Driver for the PMC MSP71xx reference board GPIO pins
 *
 * Copyright 2005-2007 PMC-Sierra, Inc.
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 *
 *  THIS  SOFTWARE  IS PROVIDED   ``AS  IS'' AND   ANY  EXPRESS OR IMPLIED
 *  WARRANTIES,   INCLUDING, BUT NOT  LIMITED  TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
 *  NO  EVENT  SHALL   THE AUTHOR  BE    LIABLE FOR ANY   DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *  NOT LIMITED   TO, PROCUREMENT OF  SUBSTITUTE GOODS  OR SERVICES; LOSS OF
 *  USE, DATA,  OR PROFITS; OR  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  You should have received a copy of the  GNU General Public License along
 *  with this program; if not, write  to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __MSP_GPIO_H__
#define __MSP_GPIO_H__

#ifdef __KERNEL__
#include <linux/ioctl.h>
#endif

/* Bit flags and masks for GPIOs */
#define MSP_NUM_GPIOS		20
#define MSP_GPIO_ALL_MASK 	((1 << MSP_NUM_GPIOS) - 1)
#define MSP_GPIO_NONE_MASK 	0LL
#define MSP_GPIO_FREE_MASK	MSP_GPIO_ALL_MASK

/* GPIO modes */
enum msp_gpio_mode {
	MSP_GPIO_INPUT		= 0x0,
	/* MSP_GPIO_ INTERRUPT	= 0x1,	Not supported yet */
	MSP_GPIO_UART_INPUT	= 0x2,	/* Only GPIO 4 or 5 */
	MSP_GPIO_OUTPUT		= 0x8,
	MSP_GPIO_UART_OUTPUT	= 0x9,	/* Only GPIO 2 or 3 */
	MSP_GPIO_PERIF_TIMERA	= 0x9,	/* Only GPIO 0 or 1 */
	MSP_GPIO_PERIF_TIMERB	= 0xa,	/* Only GPIO 0 or 1 */
	MSP_GPIO_UNKNOWN	= 0xb,  /* No such GPIO or mode */
};

/*
 * Inline functions available even when GPIO driver is disabled
 */

/* This gives you the 'register relative offset gpio' numbers */
inline static u32
basic_offset_gpio(u32 gpio)
{
	/* GPIO 0 and 1 on the first register */
	if (gpio < 2)
		return gpio;
	/* GPIO 2, 3, 4, and 5 on the second register */
	else if (gpio < 6)
		return gpio - 2;
	/* GPIO 6, 7, 8, and 9 on the third register */
	else if (gpio < 10)
		return gpio - 6;
	/* GPIO 10, 11, 12, 13, 14, and 15 on the fourth register */
	else
		return gpio - 10;
}

/* These take the 'register relative offset gpio' number */
inline static u32
basic_data_reg_mask(u32 ogpio)
{
	return 1 << ogpio;
}

inline static u32
basic_mode_reg_shift(u32 ogpio)
{
	return ogpio * 4;
}

inline static u32
basic_mode_reg_value(enum msp_gpio_mode mode, u32 ogpio)
{
	return mode << basic_mode_reg_shift(ogpio);
}

inline static u32
basic_mode_reg_mask(u32 ogpio)
{
	return basic_mode_reg_value(0xf, ogpio);
}

inline static u32
basic_mode_reg_from_reg(u32 data, u32 ogpio)
{
	return (data & basic_mode_reg_mask(ogpio)) >>
			basic_mode_reg_shift(ogpio);
}

/* These take the actual GPIO number (0 through 15) */
inline static u32
basic_data_mask(u32 gpio)
{
	return basic_data_reg_mask(basic_offset_gpio(gpio));
}

inline static u32
basic_mode_mask(u32 gpio)
{
	return basic_mode_reg_mask(basic_offset_gpio(gpio));
}

inline static u32
basic_mode(enum msp_gpio_mode mode, u32 gpio)
{
	return basic_mode_reg_value(mode, basic_offset_gpio(gpio));
}

inline static u32
basic_mode_shift(u32 gpio)
{
	return basic_mode_reg_shift(basic_offset_gpio(gpio));
}

inline static u32
basic_mode_from_reg(u32 data, u32 gpio)
{
	return basic_mode_reg_from_reg(data, basic_offset_gpio(gpio));
}

/* This gives you the extended 'register relative offset gpio' number */
inline static u32
extended_offset_gpio(u32 gpio)
{
	return gpio - 16;
}

/* These take the 'register relative offset gpio' number */
inline static u32
extended_reg_disable(u32 ogpio)
{
	return 0x2 << ((ogpio * 2) + 16);
}

inline static u32
extended_reg_enable(u32 ogpio)
{
	return 0x1 << ((ogpio * 2) + 16);
}

inline static u32
extended_reg_set(u32 ogpio)
{
	return 0x2 << (ogpio * 2);
}

inline static u32
extended_reg_clr(u32 ogpio)
{
	return 0x1 << (ogpio * 2);
}

/* These take the actual GPIO number (16 through 19) */
inline static u32
extended_disable(u32 gpio)
{
	return extended_reg_disable(extended_offset_gpio(gpio));
}

inline static u32
extended_enable(u32 gpio)
{
	return extended_reg_enable(extended_offset_gpio(gpio));
}

inline static u32
extended_set(u32 gpio)
{
	return extended_reg_set(extended_offset_gpio(gpio));
}

inline static u32
extended_clr(u32 gpio)
{
	return extended_reg_clr(extended_offset_gpio(gpio));
}

#ifdef __KERNEL__
/*
 * IOCTL structs and macros
 */
struct msp_gpio_ioctl_io_data {
	u32 data;
	u32 mask;
};

struct msp_gpio_ioctl_blink_data {
	u32 mask;
	u32 period;
	u32 dcycle;
};

#define MSP_GPIO_IOCTL_BASE	'Z'

/* Reads the current data bits */
#define MSP_GPIO_IN	_IOWR(MSP_GPIO_IOCTL_BASE, 0, \
			      struct msp_gpio_ioctl_io_data)

/* Writes data bits */
#define MSP_GPIO_OUT	_IOW(MSP_GPIO_IOCTL_BASE, 1, \
			      struct msp_gpio_ioctl_io_data)

/* Sets all masked pins to the msp_gpio_mode given in the data field */
#define MSP_GPIO_MODE	_IOW(MSP_GPIO_IOCTL_BASE, 2, \
			      struct msp_gpio_ioctl_io_data)

/*
 * Starts any masked LEDs blinking with parameters as follows:
 *   - period - The time in 100ths of a second for a single period
 *              (set to '0' to stop blinking)
 *   - dcycle - The 'duty cycle' - what percentage of the period should
 *              the gpio be on?
 */
#define MSP_GPIO_BLINK	_IOW(MSP_GPIO_IOCTL_BASE, 3, \
			      struct msp_gpio_ioctl_blink_data)

/*
 * Public driver functions available to other modules.
 */

/* Reads the bits specified by the mask and puts the values in data */
extern int msp_gpio_in(u32 *data, u32 mask);

/* Sets the specified data on the masked pins */
extern int msp_gpio_out(u32 data, u32 mask);

/* Sets masked pins to the specified msp_gpio_mode */
extern int msp_gpio_mode(enum msp_gpio_mode mode, u32 mask);

/* Stops the specified GPIOs from blinking */
extern int msp_gpio_noblink(u32 mask);

/* Configures GPIO(s) to blink */
extern int msp_gpio_blink(u32 mask, u32 period, u32 dcycle);
#endif /* __KERNEL__ */

#endif /* !__MSP_GPIO_H__ */
